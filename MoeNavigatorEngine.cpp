/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "MoeNavigatorEngine.h"


MoeNavigatorEngine::MoeNavigatorEngine()
{
    this->stylesheet_collection = nullptr;

    this->document_ready = false;

    this->document_width = 0;
    this->document_height = 0;

    //Do not initialise the markup parser directly.
    //Only initalise it when it is clear, which kind of
    //markup document shall be parsed.
    this->markup_parser = nullptr;
    this->stylesheet_parser = std::make_unique<CSSParser>();

    this->network = std::make_shared<MNE::Network::NetworkHandler_POSIX>();
    this->tls_network = std::make_shared<MNE::Network::NetworkHandler_GnuTLS>();

    this->network->setBufferSize(65536); //64KiB buffer size

    this->user_agent = "MoeNavigatorEngine";


    this->cookie_jar = std::make_shared<MNE::CookieJar>();
}


std::shared_ptr<MNE::CookieJar> MoeNavigatorEngine::getCookieJar()
{
    return this->cookie_jar;
}


void MoeNavigatorEngine::setCookieJar(std::shared_ptr<MNE::CookieJar> cookie_jar)
{
    if (cookie_jar == nullptr) {
        return;
    }
    this->cookie_jar = cookie_jar;
}


CookiePolicy MoeNavigatorEngine::getGeneralCookiePolicy()
{
    return this->general_cookie_policy;
}


void MoeNavigatorEngine::setGeneralCookiePolicy(CookiePolicy general_policy)
{
    this->general_cookie_policy = general_policy;
}


std::shared_ptr<StylesheetRule> MoeNavigatorEngine::searchStylesheetsFromNode(
    const std::shared_ptr<DocumentNode> Node
    ) const
{
    return nullptr;
}


unsigned long int MoeNavigatorEngine::decodeColourValues(const std::string colour)
{
    //Determine length:

    int length = colour.length();

    char *r = new char[3];
    memset(r, 0, 3);
    char *g = new char[3];
    memset(g, 0, 3);
    char *b = new char[3];
    memset(b, 0, 3);

    if (length == 7) {
        //Form: #000000
        r[0] = colour[1];
        r[1] = colour[2];
        r[2] = '\0';
        g[0] = colour[3];
        g[1] = colour[4];
        g[2] = '\0';
        b[0] = colour[5];
        b[1] = colour[6];
        b[2] = '\0';
    } else if(length == 4) {
        //Form #000
        r[0] = colour[1];
        r[1] = colour[1];
        r[2] = '\0';
        g[0] = colour[2];
        g[1] = colour[2];
        g[2] = '\0';
        b[0] = colour[3];
        b[1] = colour[3];
        b[2] = '\0';
    }

    unsigned int ir = 0;
    sscanf(r,"%x",&ir);
    unsigned int ig = 0;
    sscanf(g,"%x",&ig);
    unsigned int ib = 0;
    sscanf(b,"%x",&ib);

    unsigned long int result = (ir << 24) + (ig << 16) + (ib << 8) + 255;

    delete[] r;
    delete[] g;
    delete[] b;

    return result;
}


bool MoeNavigatorEngine::isEmptyTextNode(std::shared_ptr<DocumentNode> node)
{
    if(node != nullptr) {
        if(node->name.find_first_not_of(" ") == std::string::npos) {
            //only blank spaces were found
            return true;
        } else {
            return false;
        }
    } else {
        //if $Node is equal to nullptr, the value true should be returned
        //so that the calling function can delete this node.
        return true;
    }
}


unsigned long int* MoeNavigatorEngine::searchSelector(std::string selector)
{
    return nullptr; //TODO
}


void MoeNavigatorEngine::setNodeStyleAttribute(
    const std::shared_ptr<DocumentNode> node,
    const std::string attribute_name,
    const std::string attribute_value
    )
{
    std::string selector_name = "#__$NODE";
    selector_name += node->returnNodeID();

    unsigned long int *result_selector_index = this->searchSelector(
        selector_name
        );
    if (result_selector_index == nullptr) {
        //No index for that Selector found.
        //TODO: re-implement with StylesheetCollection

        //this->StylesheetMedia[STYLESHEETMEDIA_SCREEN]->Rules.push_back(new StylesheetRule(SelectorName));
        //ResultSelectorIndex = new unsigned long int;
        //*ResultSelectorIndex = this->StylesheetMedia[STYLESHEETMEDIA_SCREEN]->Rules.size();
    } else {
        //selector found
    }

    delete result_selector_index;
}


void MoeNavigatorEngine::setNodeStyle(std::shared_ptr<DocumentNode> node)
{
    if ((node != nullptr) and (this->stylesheet_collection != nullptr)) {
        //Media type screen is the default for now.
        //This is why it is specified here.
        auto found_rules = this->stylesheet_collection->findRules(
            "screen",
            node->name
        );

        for (auto rule: found_rules) {
            if (rule == nullptr) {
                continue;
            }
            //The rule has to be applied to the node:
            //This is inaccurate, since only the last rule of a list of
            //rules is used, but to make merging of stylesheet attributes
            //easy, the whole data structures have to be rewritten,
            //which is planned for a later release.
            this->setNodeStyle(node, rule);
        }

        //If no rule was found nothing is done since there is no rule
        //that can be applied to the node.
    }
}


void MoeNavigatorEngine::setNodeStyle(
    std::shared_ptr<DocumentNode> node,
    std::shared_ptr<StylesheetRule> rule
    )
{
    if ((node != nullptr) && (rule != nullptr)) {
        node->style = rule->attributes;
    }
}


void MoeNavigatorEngine::applyAttribute(
    std::shared_ptr<DocumentNode> node,
    std::string name,
    std::string value
    )
{
    //cerr << "Read Attribute: Name: [" << Attribute->Name << "] Value: [" << Attribute->Value << "]" << endl;

    if (name.compare("style") == 0) {
        //Invoke CSS parser, set it to be inside a ruleset
        //and let it return the parsed rules.
        try {
            stylesheet_parser->parseData(
                std::vector<uint8_t>(value.begin(), value.end())
                );
        } catch (MNE::Exception& e) {
            //Nothing to do. The style node contains invalid stylesheets.
        }
        std::shared_ptr<StylesheetCollection> rule_style =
            stylesheet_parser->returnStylesheetRules();
        stylesheet_parser->flushStylesheetRules(); //this function might still be a pain in the ass at the moment! (SIGABRT, something with the destructors and vectors)

        //TODO: Get the parsed rule from the stylesheet collection,
        //extract its attributes and set each attribute using
        //the setNodeStyleAttribute method.
    }
}


void MoeNavigatorEngine::setDocument(
    std::shared_ptr<DocumentNode> new_document,
    const std::string& mime_type
    )
{
    this->setDocument_Internal(new_document->cloneNode(), mime_type);
}


void MoeNavigatorEngine::setDocument_Internal(
    std::shared_ptr<DocumentNode> new_document,
    const std::string& mime_type
    )
{
    this->document = new_document;
    this->current_node = this->document;
    if (mime_type.length() > 0) {
        this->document_mime_type = mime_type;
    }

    if (new_document == nullptr) {
        //We have set the new document to a null pointer and cannot do
        //anything else with it here.
        return;
    }

    if ((this->document_mime_type == "text/html")
        || (this->document_mime_type == "application/xml")
        || (this->document_mime_type == "text/xml")) {
        this->document_head_found = false;

        this->document_ready = true;

        try {
            this->stylesheet_parser->parseData(
                std::vector<uint8_t>(DEFAULT_STYLE.begin(), DEFAULT_STYLE.end())
                );
        } catch (MNE::Exception& e) {
            //Nothing to do. The style node contains invalid stylesheets.
        }

        this->stylesheet_collection = this->stylesheet_parser->returnStylesheetRules();

        this->processDocument(); //to get all the stylesheets and other things for the document

        this->determineVisualAttributes(this->document);
    } else if (this->document_mime_type == "text/plain") {
        this->document_head_found = true;
        this->document_ready = true;
        std::string plaintext_style = "PlainText {"
            "font-size: 12;"
            "font-family: \"monospace\";"
            "white-space: pre;"
            "margin: 12;"
            "}";
        try {
            this->stylesheet_parser->parseData(
                std::vector<uint8_t>(plaintext_style.begin(), plaintext_style.end())
                );
        } catch (MNE::Exception& e) {
            //Nothing to do. The style node contains invalid stylesheets.
        }
        this->stylesheet_collection = this->stylesheet_parser->returnStylesheetRules();
        this->determineVisualAttributes(this->document);
    }
}


void MoeNavigatorEngine::determineVisualAttributes(
    std::shared_ptr<DocumentNode> node
    )
{
    if (node == nullptr) {
        //What shall we do with a nullptr?
        return;
    }

    //How to get positions (draft):
    //1. Set the start positions in X and Y. If the node is the root node
    //   these are calculated as 0 + margin-left + border-left-width and
    //   0 + margin-top + border-top-width.
    //   If the node is a child of the root node its start position is
    //   calculated as: parent_x + margin-left + border-width-left and
    //   parent_y + margin-top + border-width-top.
    //2. Descend into each child node and determine its width and height.
    //3. Set the current node's width and height depending on the width and
    //   height of the child nodes. Child nodes displayed as block elements
    //   increase the current node's height in any case. The current node's
    //   width is only increased when the child node's width is greater than
    //   the currently set width of the current node.
    //   Child nodes displayed as inline increase the current node's
    //   width in the case that no line break is necessary to display the
    //   node in its full width. If the node must be displayed in the
    //   next line, the width of the current node is unchanged and the
    //   height of the current node increases by the height of the child node.

    //If the node is invisible, either because the parent node or the
    //node itself is invisible, we don't need to do anything.
    auto node_parent = node->getParentNode();
    if (node->style.display_method == VisualAttributes::Display::NONE) {
        return;
    } else if (node_parent != nullptr) {
        if (node_parent->style.display_method == VisualAttributes::Display::NONE) {
            node->style.display_method = VisualAttributes::Display::NONE;
            //Iterate over the node's children so that they
            //become invisible, too:
            for (std::shared_ptr<DocumentNode> child: node->children) {
                if (child != nullptr) {
                    this->determineVisualAttributes(child);
                }
            }
            //Return here since no calculations are required.
            return;
        }
    }

    //The following code does not only compute the width and height
    //but also things like colours etc.
    switch (node->type) {
        case DocumentNode::Type::TEXT: {
            // Contains the maximum width of the lines in this text node:
            unsigned short max_width = 0;

            if (node_parent != nullptr) {
                //Text nodes have tag nodes as parents
                //and they use their background-color.
                //Their coordinates are those of the parent node with
                //the parent's padding added.
                node->style.coord_x = node_parent->style.coord_x
                    + node_parent->style.padding_left;
                node->style.coord_y = node_parent->style.coord_y
                    + node_parent->style.padding_top;
                node->style.background_color = node_parent->style.background_color;
                node->style.max_width = node_parent->style.max_width;
                node->style.max_height = node_parent->style.max_height;
            } else {
                //Use the default color: white
                //TODO: derive from default style
                node->style.background_color.r = 255;
                node->style.background_color.g = 255;
                node->style.background_color.b = 255;
            }

            //Determine line width by looking at the text:
            //In this early stage it is assumed that a monospace font is used
            //where a glyph's width is 60% of the glyph's height.

            //TODO (draft):
            //When a text node's content needs to be continued
            //in the next line, the text node has to be splitted into two,
            //where the first one represents the first line of text
            //and the second one representing the rest of the text.
            //This continues until the whole text of the original text
            //node is splitted into several nodes.

            // We need to store the width at the last space character:
            unsigned short width_at_last_space = 0;
            size_t last_split_position = 0;
            size_t last_space_position = 0;

            unsigned short line_width = 0;

            for (size_t i = 0; i < node->name.size(); i++) {
                const char glyph = node->name[i];
                if (glyph == ' ') {
                    //A possibility to insert a line break. Check if the
                    //line width is less than the maximum allowed width
                    //or if it will fit into the reserved space.
                    if ((line_width > node->style.max_width)
                        && (node_parent != nullptr)) {
                        //The text line is too long:
                        //Replace the space with a line break.
                        node->name[i] = '\n';

                        //update "indexes":
                        last_split_position = i;
                        width_at_last_space = 0;
                        line_width = 0;
                    }
                    last_space_position = i;
                }
                line_width++;
            }
            break;
        }
        case DocumentNode::Type::NODE: {
            this->setNodeStyle(node);

            VisualAttributes parent_style;
            if (node_parent != nullptr) {
                parent_style = node_parent->style;
            }

            //The coordinates, max width and height are calculatd by the
            //constraints of the parent node, the node's predecessor
            //and the nodes own margin.
            auto node_previous = node->getPreviousNode();
            if ((node_previous != nullptr)) {
                if (node_previous->style.display_method == VisualAttributes::Display::BLOCK) {
                    //Use the predecessor's Y start position and its height
                    //plus border and margin as start position.
                    //The X start position is that of the parent.
                    node->style.coord_x = parent_style.coord_x;
                    node->style.coord_y = node_previous->style.coord_y
                        + node_previous->style.height
                        + node_previous->style.border_bottom_width
                        + node_previous->style.margin_bottom;
                } else if (node_previous->style.display_method == VisualAttributes::Display::INLINE) {
                    //Use the parent's Y starting position and the predecessor's
                    //X starting position and its width plus border and margin.
                    //(The opposite of what is done for block predecessors).
                    node->style.coord_y = parent_style.coord_y;
                    node->style.coord_x = node_previous->style.coord_x
                        + node_previous->style.width
                        + node_previous->style.border_right_width
                        + node_previous->style.margin_right;
                } else if (node_previous->style.display_method == VisualAttributes::Display::NONE) {
                    //Invisible nodes are not regarded.
                    //X and Y coordinates depend only on the parent
                    //and the node itself.
                    node->style.coord_x = parent_style.coord_x
                        + parent_style.padding_left
                        + node->style.margin_left;
                    node->style.coord_y = parent_style.coord_y
                        + parent_style.padding_top
                        + node->style.margin_top;
                }
            } else {
                //X and Y coordinates depend only on the parent
                //and the node itself.
                node->style.coord_x = parent_style.coord_x
                    + parent_style.padding_left
                    + node->style.margin_left;
                node->style.coord_y = parent_style.coord_y
                    + parent_style.padding_top
                    + node->style.margin_top;
            }
            node->style.max_width = parent_style.max_width
                - node->style.margin_right
                - node->style.border_right_width
                - parent_style.padding_right;
            node->style.max_height = parent_style.max_height
                - node->style.margin_bottom
                - node->style.border_bottom_width
                - parent_style.padding_bottom;

            //determine width and height:
            node->style.width = node->style.padding_left;
            node->style.height = node->style.padding_top
                + node->style.line_height;

            //it is assumed that the required space for the child elements is already determined:
            for (std::shared_ptr<DocumentNode> child: node->children) {
                if (child != nullptr) {
                    this->determineVisualAttributes(child);
                    if (child->style.display_method == VisualAttributes::Display::BLOCK) {
                        node->style.height += child->style.height;
                        if (child->style.width > node->style.width) {
                            node->style.width = child->style.width;
                        }
                    } else if (child->style.display_method == VisualAttributes::Display::INLINE) {
                        node->style.width += child->style.width;
                        if (child->style.height > node->style.height) {
                            node->style.height = child->style.height;
                        }
                    }
                }
            }
            node->style.width += node->style.padding_right;
            node->style.height += node->style.padding_bottom;
            break;
        }
    }
}


void MoeNavigatorEngine::processDocument()
{
    uint32_t current_child_position = 0;

    this->current_node = this->document;

    if (this->current_node == nullptr) {
        //Empty document
        return;
    }

    do {
        //Check for special nodes with names like "style" or "script".

        if (this->current_node->name == "style") {
            //Search for an attribute named "type".
            for (auto i = this->current_node->attributes.begin();
                 i != this->current_node->attributes.end();
                 ++i
                ) {
                if (i->first != "type") {
                    //This is not the attribute name we are looking for.
                    continue;
                }
                //At this point we have found an attribute with the name "typ1e".
                //Now the MIME type has to be checked.
                if (i->second == "text/css") {
                    //A CSS stylesheet node has been found.
                    if (this->current_node->children.size() == 0) {
                        //The node is empty (no text).
                        //There is nothing to do then.
                        continue;
                    }
                    //If there are more than zero nodes below the
                    //current node then there has to be only one text
                    //node with the CSS code in it.
                    //We invoke the CSS parser on the first child node.
                    std::shared_ptr<DocumentNode> first_child =
                        this->current_node->children.front();
                    try {
                        stylesheet_parser->parseData(
                            std::vector<uint8_t>(first_child->name.begin(), first_child->name.end())
                            );
                    } catch (MNE::Exception& e) {
                        //Nothing to do. The style node contains invalid stylesheets.
                    }
                    std::shared_ptr<StylesheetCollection> parsed_rules = stylesheet_parser->returnStylesheetRules();
                    //Merge the parsed rules with the already existing rules:
                    this->stylesheet_collection->merge(parsed_rules);

                    //Remove the rules from the parser:
                    stylesheet_parser->flushStylesheetRules();
                }
            }
        } else if (this->current_node->name == "script") {
            //Script node found
            //TODO
        } else {
            //search node for attributes:
            for (auto i = this->current_node->attributes.begin();
                 i != this->current_node->attributes.end();
                 ++i
                ) {
                this->applyAttribute(this->current_node, i->first, i->second);
            }
        }
        //after all attributes were applied the first child node
        //of $this->current_node can be handled

        if (this->current_node->children.size() > current_child_position) {
            //wenn der aktuelle Knoten Kinder hat und der Zähler für das
            //aktuelle Kind noch nicht das letzte Kind erreicht hat:
            //eine Ebene nach unten
            this->current_node = this->current_node->children[current_child_position];
            this->current_child_stack.push(current_child_position);
            current_child_position = 0;
        } else {
            //wenn der aktuelle Knoten keine Kinder hat oder der Zähler für das aktuelle Kind
            //das letzte Kind erreicht hat

            //cerr << "Node [" << this->current_node->name << "] has no children or the last children was reached!" << endl;

            //grafische Attribute anhand von Stylesheets ermitteln:
            //this->determineVisualAttributes(this->current_node);

            //Tag anwenden und eine Ebene weiter nach oben:

            //eine Ebene nach oben:
            this->current_node = this->current_node->getParentNode();
            if (this->current_child_stack.size() > 0) {
                current_child_position = this->current_child_stack.top();
                this->current_child_stack.pop();
                current_child_position++;
            }
        }

        //if(this->current_node == NULL)
        //{
        //    cerr << "processDocument: FATAL: CurrentNode == NULL!! AFTER" << endl;
        //}
    }
    while((this->current_child_stack.size() > 0) ||
          (this->current_node->children.size() > current_child_position));
    //while(this->current_node != this->current_node->parent); //Abbruch mittels Loop-Knoten

    //cerr << "processDocument: Pass1 finished." << endl;
    //cerr << "processDocument: Pass1 finished. Starting Pass2..." << endl;
    //$this->document contains the start node of the document
    //this->DetermineStartPosition(this->document);
    //cerr << "processDocument: Pass2 finished." << endl;
}


void MoeNavigatorEngine::setDocumentDimensions(
    unsigned long int width,
    unsigned long int height
    )
{
    if ((width < 1) || (height < 1)) {
        //Nothing to do.
        return;
    }

    this->document_width = width;
    this->document_height = height;

    //Pass the width and height to the renderers:

    for (std::shared_ptr<MNERenderer> renderer: this->renderers) {
        if (renderer == nullptr) {
            continue;
        }

        renderer->setDrawingAreaDimensions(
            this->document_width,
            this->document_height
            );
    }
}


void MoeNavigatorEngine::drawPage()
{
    if (this->document_ready == true) {
        //The renderer gets the drawing area dimensions and renders the document.
        //The engine is just responsible for getting and parsing the document
        //and associating stylesheet information to it.

        for (auto renderer: this->renderers) {
            renderer->setDrawingAreaDimensions(
                this->document_width,
                this->document_height
                );
            renderer->setDocument(this->document);
            renderer->render();
        }
    }
}


/////////////////
// network and file stuff:
/////////////////


void MoeNavigatorEngine::openURL(std::string url)
{
    MNE::URL splitted_url(url);
    if (splitted_url.protocol == "http" || splitted_url.protocol == "https") {
        //Build the request object:
        std::shared_ptr<MNE::Network::HTTPRequest> request = nullptr;
        if (splitted_url.protocol == "https") {
            //HTTPS
            request = std::make_shared<MNE::Network::HTTPRequest>(
                this->tls_network,
                splitted_url.domain,
                splitted_url.port,
                splitted_url.path,
                0 //TODO: generate request IDs.
                );
        } else {
            //HTTP
            request = std::make_shared<MNE::Network::HTTPRequest>(
                this->network,
                splitted_url.domain,
                splitted_url.port,
                splitted_url.path,
                0 //TODO: generate request IDs.
                );
        }
        //Set the method:
        request->setMethod("GET");
        //Look for cookies for that domain:
        if (this->cookie_jar != nullptr) {
            auto cookie_list = this->cookie_jar->getDomainCookies(splitted_url.domain);
            if (!cookie_list.empty()) {
                for (auto cookie: cookie_list[splitted_url.domain]) {
                    request->addRequestHeader("Cookie", cookie.name + "=" + cookie.value);
                }
            }
        }

        //Set the user agent:
        request->addRequestHeader("User-Agent", this->user_agent);
        //Start the request:
        request->start();

        auto response_header = request->getResponseHeader();
        //Depending on the MIME type we initialise the correct
        //markup parser or we throw an error if the resource cannot
        //be parsed by any parser:
        std::string content_type_str;
        for (auto field: response_header) {
            if (field.first == "Content-Type") {
                content_type_str = field.second;
            } else if (field.first == "Set-Cookie") {
                auto first_equal_sign = field.second.find_first_of('=');
                if (first_equal_sign == std::string::npos) {
                    //Invalid cookie field.
                    continue;
                }
                MNE::Cookie c;
                c.name.append(field.second, 0, first_equal_sign);
                first_equal_sign++;
                c.value.append(field.second, first_equal_sign, std::string::npos);
                this->cookie_jar->setCookie(splitted_url.domain, c);
            }
        }
        //We must split the (optional) parameters from the MIME type:
        std::string mime_type;
        std::vector<std::string> mime_parameters;
        std::string::iterator first_param_start = content_type_str.end();
        std::string current_param;

        for (auto it = content_type_str.begin(); it != content_type_str.end(); it++) {
            if (*it == ';') {
                if (first_param_start == content_type_str.end()) {
                    //Start of first parameter
                    first_param_start = it;
                    continue;
                }
                //End of previous parameter: add current_param to
                //mime_parameters:
                mime_parameters.push_back(current_param);
                current_param.clear();
            }
            if (first_param_start == content_type_str.end()) {
                //Read the charater if it is not whitespace and
                //add it to the mime_type string:
                if (*it != ' ') {
                    mime_type += *it;
                }
            } else {
                //Read the character if it is not a whitespace
                //inside a parameter:
                if (*it == ' ' && current_param.empty()) {
                    continue;
                }
                current_param+= *it;
            }
        }
        //Add the last parameter to mime_parameters:
        if (!current_param.empty()) {
            mime_parameters.push_back(current_param);
        }
        this->document_mime_type = mime_type;

        //Select the parser depending on the mime type:
        if (this->document_mime_type == "application/xml") {
            this->markup_parser = std::make_unique<XMLParser>();
        } else if (this->document_mime_type == "text/html") {
            this->markup_parser = std::make_unique<HTMLParser>();
        } else if (this->document_mime_type == "text/plain") {
            this->markup_parser = std::make_unique<PlaintextParser>();
        } else {
            //Workaround for early development: Default to HTML parser.
            std::cerr << "MNENetwork: Warning: No parser found for mime type "
                      << mime_type
                      << "! Using plaintext parser as default!"
                      << std::endl;
            this->markup_parser = std::make_unique<PlaintextParser>();
        }

        auto result = request->getResponseData();
        while (!result->empty()) {
            try {
                this->markup_parser->parseData(*result);
            } catch (MNE::Exception e) {
                std::cerr << "Exception on reading: "
                          << e.toString() << std::endl;
                request->abort();
                return;
            }
            result = request->getResponseData();
        }
    } else if (splitted_url.protocol == "gopher") {
        auto request = std::make_shared<MNE::Network::GopherRequest>(
            this->network,
            splitted_url.domain,
            splitted_url.port,
            splitted_url.path,
            0 //TODO: generate request IDs
            );

        char resource_type = request->getItemType();
        if (resource_type == '0') {
            //A text file.
            this->markup_parser = std::make_unique<PlaintextParser>();
            this->document_mime_type = "text/plain";
        } else if ((resource_type == '1') || (resource_type == '7')) {
            //A gopher menu or a search service.
            //We need a special parser for that.
            this->markup_parser = std::make_unique<GopherMenuParser>();
        } else if (resource_type == '9') {
            //Do something with the binary data.
            std::cerr << "ERROR: Downloading binary data isn't implemented yet!" << std::endl;
            return;
        }
        if (this->markup_parser != nullptr) {
            //We have found a parser that can be used.
            request->start();
            auto result = request->getResponseData();
            while (!result->empty()) {
                try {
                    this->markup_parser->parseData(*result);
                } catch (MNE::Exception e) {
                    std::cerr << "Exception on reading: "
                              << e.toString() << std::endl;
                    request->abort();
                    return;
                }
                result = request->getResponseData();
            }
        }
    } else if (splitted_url.protocol == "qotd") {
        auto request = std::make_shared<MNE::Network::QOTDRequest>(
            this->network,
            splitted_url.domain,
            splitted_url.port,
            splitted_url.path,
            0 //TODO: generate request IDs
            );
         this->markup_parser = std::make_unique<PlaintextParser>();
         this->document_mime_type = "text/plain";
         auto result = request->getResponseData();
         while (!result->empty()) {
             try {
                 this->markup_parser->parseData(*result);
             } catch (MNE::Exception e) {
                 std::cerr << "Exception on reading: "
                           << e.toString() << std::endl;
                 request->abort();
                 return;
             }
             result = request->getResponseData();
         }
    } else if (splitted_url.protocol == "file") {
        std::cerr << "file protocol selected " << std::endl;

        std::string file_name = splitted_url.path;
        std::ifstream file(file_name, std::ios::binary);

        //Depending on the file type we must select an appropriate parser
        //or we must stop loading the file.
        //The quick and dirty document type detection: Look at the file name.
        size_t last_dot_position = file_name.find_last_of('.');
        std::string file_extension = file_name.substr(
            last_dot_position+1,
            std::string::npos
            );

        if (file_extension == "xml") {
            this->markup_parser = std::make_unique<XMLParser>();
            this->document_mime_type = "application/xml";
        } else if (file_extension == "html") {
            this->markup_parser = std::make_unique<HTMLParser>();
            this->document_mime_type = "text/html";
        } else {
            throw MNE::Exception(
                "MoeNavigatorEngine",
                "No parser found for file extension " + file_extension,
                1
                );
        }

        //Read the file:
        std::vector<uint8_t> data(
            std::istreambuf_iterator<char>{file},
            std::istreambuf_iterator<char>{}
            );
        markup_parser->parseData(data); //give the whole file content to the markup parser
    } else {
        //Unsupported protocol: do nothing
        return;
    }

    //It doesn't matter what protocol was used.
    //The markup parser will always return the node tree
    //it has parsed (if it could parse something).
    std::shared_ptr<DocumentNode> t = this->markup_parser->returnNodeTree();
    if (t == nullptr) {
        //if $t == NULL then no nodes have been parsed
        std::cerr << "FATAL: No document received! Nothing to do!" << std::endl;
        return;
    }
    this->setDocument_Internal(t);
}


void MoeNavigatorEngine::setUserAgent(std::string user_agent)
{
    this->user_agent = user_agent;
}


//Renderer methods:


void MoeNavigatorEngine::addRenderer(std::shared_ptr<MNERenderer> renderer)
{
    this->renderers.push_back(renderer);
    //Give the renderer the current document:
    if (this->document != nullptr) {
        renderer->setDocument(this->document);
    }
}


std::vector<std::shared_ptr<MNERenderer>> MoeNavigatorEngine::getRenderers()
{
    return this->renderers;
}


void MoeNavigatorEngine::removeRenderer(std::shared_ptr<MNERenderer> renderer)
{
    for (auto it = this->renderers.begin(); it != this->renderers.end(); it++) {
        if (*it == renderer) {
            this->renderers.erase(it);
            return;
        }
    }
    //TODO: replace the for-loop with a call to removeElementFromContainer,
    //when the latter is ready.
}


//ResponseHandler interface implementation


void MoeNavigatorEngine::processResponseData(
    std::shared_ptr<std::vector<uint8_t>> data,
    bool last_chunk
    )
{
    
}



// event handling methods:


void MoeNavigatorEngine::handleEvent(MNEEvent* e)
{
    std::cerr << "MoeNavigatorEngine: Handling event..." << std::endl;
}
