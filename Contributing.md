# Contributing

The contribution guidelines can be found in the wiki of this project:

https://codeberg.org/moenavigator/moenavigatorengine/wiki/Contribution_guidelines
