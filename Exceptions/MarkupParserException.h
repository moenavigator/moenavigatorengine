/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNE__MARKUPPARSEREXCEPTION_H
#define MNE__MARKUPPARSEREXCEPTION_H


#include "../Exceptions/Exception.h"


#include <string>


namespace MNE
{
    class MarkupParserException: public Exception
    {
        protected:


        /**
         * The line number in the source code where the
         * exception occurred.
         */
        unsigned short line_number = 0;


        /**
         * The column number in the source code where the
         * exception occurred.
         */
        unsigned short column_number = 0;


        public:


        MarkupParserException(
            std::string component,
            std::string reason,
            unsigned long int error_code,
            unsigned short line_number = 0,
            unsigned short column_number = 0)
            : Exception(component, reason, error_code),
              line_number(line_number),
              column_number(column_number)
        {
        }


        unsigned short getLineNumber() const
        {
            return this->line_number;
        }


        unsigned short getColumnNumber() const
        {
            return this->column_number;
        }


        std::string toString() const
        {
            return this->component + ": E" + std::to_string(this->error_code) + "@"
                + std::to_string(this->line_number) + "," + std::to_string(this->column_number)
                + ": " + this->reason;
        }
    };
}


#endif
