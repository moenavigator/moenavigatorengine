/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNE__TLSEXCEPTION_H
#define MNE__TLSEXCEPTION_H


#include <map>


#include "Exception.h"


namespace MNE
{
    /**
     * TLS error codes.
     */
    enum TLSError: unsigned long int {

        /**
         * An error occurred that is not represented in the other
         * error codes.
         */
        GENERAL = 0,

        /**
         * The connection could not be established.
         */
        CONNECTION_FAILED = 1,

        /**
         * The connection broke up after being established.
         */
        CONNECTION_HANGUP = 2,

        /**
         * An error occurred while transferring data over a
         * secure connection.
         */
        DATA_TRANSFER = 3,

        /**
         * An error occurred while closing a secure connection.
         */
        CONNECTION_CLOSE = 4,

        /**
         * The server certificate is invalid.
         */
        INVALID_SERVER_CERT = 100,

        /**
         * The client certificate is invalid.
         */
        INVALID_CLIENT_CERT = 101,

        /**
         * The server certificate has expired.
         */
        SERVER_CERT_EXPIRED = 110,

        /**
         * The client certificate has expired.
         */
        CLIENT_CERT_EXPIRED = 111,

        /**
         * The server certificate isn't trusted.
         */
        SERVER_CERT_UNTRUSTED = 120,

        /**
         * The client certificate isn't trusted.
         */
        CLIENT_CERT_UNTRUSTED = 121,

        /**
         * The server certificate does not match the domain
         * of the server.
         */
        SERVER_CERT_BAD_DOMAIN = 130,

        /**
         * The cryptography that is used for the "secure" connection
         * is considered weak or insecure or both.
         */
        WEAK_CRYPTOGRAPHY = 200,
    };


    /**
     * The TLSException class represents exceptions that occur
     * when trying to establish a secure connection using a TLS
     * network handler.
     */
    class TLSException: public Exception
    {
        public:


        TLSException(
            std::string component,
            std::string reason,
            unsigned long int error_code
            )
            : Exception(component, reason, error_code)
        {
        }
    };
}


#endif
