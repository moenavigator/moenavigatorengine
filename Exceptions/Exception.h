/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNE__EXCEPTION_H
#define MNE__EXCEPTION_H


#include <string>


namespace MNE
{
    class Exception
    {
        protected:


        /**
         * The component, where the error occurred (e.g. the class name).
         */
        std::string component;


        /**
         * The reason (error string) why the exception is thrown.
         * This should be a human-readable text.
         */
        std::string reason;


        /**
         * The error code for the exception. The error code can help
         * code catching the exception to make a finer distinction
         * about what has gone wrong.
         *
         * @see MNEErrorCodes.h for the error codes.
         */
        unsigned long int error_code;


        public:


        /**
         * The constructor needs the component, a reason and an error code.
         */
        Exception(
            std::string component,
            std::string reason,
            unsigned long int error_code
            )
            : component(component),
              reason(reason),
              error_code(error_code)
        {
        }


        /**
         * @returns std::string The component that threw the exception.
         */
        std::string getComponent() const
        {
            return this->component;
        }


        /**
         * @returns std::string The reason why the exception has been thrown.
         */
        std::string getReason() const
        {
            return this->reason;
        }


        /**
         * @returns unsigned long int The error code for the exception.
         */
        unsigned long int getErrorCode() const
        {
            return this->error_code;
        }


        std::string toString() const
        {
            return this->component + ": E" + std::to_string(this->error_code) + ": "
                + this->reason;
        }


        /**
         * The default virtual destructor.
         */
        virtual ~Exception() = default;
    };
}


#endif
