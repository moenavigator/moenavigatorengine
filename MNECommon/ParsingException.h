/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNE__PARSINGEXCEPTION_H
#define MNE__PARSINGEXCEPTION_H


#include "../Exceptions/Exception.h"


namespace MNE
{
    /**
     * The ParserException class represents errors that occurred
     * in a parser during parsing.
     */
    class ParsingException: public Exception
    {
        protected:


        /**
         * The text line where the error occurred.
         */
        uint32_t line = 0;


        /**
         * The text column where the error occurred.
         */
        uint32_t column = 0;


        public:


        ParsingException(
            uint32_t line,
            uint32_t column,
            std::string component,
            std::string reason,
            unsigned long int error_code
            ) : line(line),
                column(column),
                Exception(
                    component,
                    reason,
                    error_code
                    )
        {
        }


        std::string toString()
        {
            return this->component + " @ line " + std::to_string(this->line)
                + ", column " + std::to_string(this->column)
                + ": E" + std::to_string(this->error_code)
                + ": " + this->reason;
        }
    };
}


#endif
