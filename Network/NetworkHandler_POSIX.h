/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef NETWORKHANDLER_POSIX_H
#define NETWORKHANDLER_POSIX_H


#include "NetworkHandler.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <netinet/in.h>

#include <iostream>

#include <string.h> //for memset() and memcpy()

#include <cstdint>
#include <iterator>
#include <vector>


namespace MNE::Network
{
    class NetworkHandler_POSIX : public NetworkHandler
    {
        public:


        /**
         * @see NetworkHandler::request
         */
        virtual void request(
            std::string host,
            uint16_t port,
            std::weak_ptr<ResponseHandler> response_handler,
            std::vector<uint8_t> request_header = {},
            std::shared_ptr<DataSource> request_body = nullptr
            ) override;


        /**
         * @see NetworkHandler::setBufferSize
         */
        virtual void setBufferSize(size_t buffer_size) override;


        protected:


        /**
         * This helper method does the address resolving before
         * opening a POSIX socket.
         */
        virtual struct addrinfo* resolveAddress(
            const std::string& host,
            const uint16_t port,
            struct addrinfo* socket_config
            );


        /**
         * This is a helper method for opening a POSIX socket.
         *
         * @returns int An int representing the opened socket.
         *     On error, -1 is returned.
         */
        virtual int openSocket(
            const std::string& host,
            const uint16_t port,
            struct addrinfo* socket_config
            );


        /**
         * Closes an open POSIX socket.
         */
        virtual void closeSocket(long int socket);


        /**
         * The maximum amount of bytes to be buffered when reading
         * or writing data. Defaults to 64 KiB.
         */
        size_t buffer_size = 65536;
    };
}


#endif
