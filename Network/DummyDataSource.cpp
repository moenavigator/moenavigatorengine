/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "DummyDataSource.h"


using namespace MNE::Network;



void DummyDataSource::setMimeType(const std::string& mime_type)
{
    this->mime_type = mime_type;
}

void DummyDataSource::setData(const std::vector<uint8_t>& data)
{
    this->data = data;
}


void DummyDataSource::setData(std::vector<uint8_t>&& data)
{
    this->data = data;
    data.clear();
}


std::vector<uint8_t> DummyDataSource::getData(const size_t chunk_size)
{
    if (this->data.size() == 0) {
        return {};
    }
    //Check if chunk_size is greater than zero and if there are more than
    //$chunk_size bytes in the data vector.
    if ((chunk_size > 0) && (this->data.size() > chunk_size)) {
        //Slice $chunk_size bytes from the beginning of the data vector
        //and return them.
        auto begin = this->data.begin();
        if (begin == this->data.end()) {
            //We cannot get data (something is terribly wrong).
            return {};
        }
        auto end = begin;
        std::advance(end, chunk_size);
        if (end == this->data.end()) {
            //We cannot get data either (and something is terribly wrong).
            return {};
        }
        std::vector<uint8_t> result(begin, end);
        auto remainder_begin = end;
        if (remainder_begin != this->data.end()) {
            //At least one byte is left in the data vector, so the content
            //that has been sliced from it, has to be removed.
            this->data = std::vector<uint8_t>(
                remainder_begin,
                this->data.end()
                );
        }
        return result;
    } else {
        //Return the whole content of the data vector.
        auto result = data;
        this->data.clear();
        return result;
    }
}


std::string DummyDataSource::getMimeType()
{
    return this->mime_type;
}
