/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "DummyServer.h"


using namespace MNE::Network;


void DummyServer::setExpectedRequestData(const std::vector<uint8_t>& data)
{
    this->expected_request_data = data;
}


void DummyServer::setResponseData(const std::vector<uint8_t>& data)
{
    this->response_data = data;
}


void DummyServer::readRequestData(
    const std::vector<uint8_t>& data,
    bool last_chunk
    )
{
    this->request_data.insert(this->request_data.end(), data.begin(), data.end());

    if (last_chunk) {
        //Test whether request_data has the same content
        //as expected_request_data:
        if (this->request_data != this->expected_request_data) {
            throw MNE::Exception(
                "DummyServer",
                "Request data do not match the expected data!",
                1
                );
        }
    }
}


std::vector<uint8_t> DummyServer::getResponseData() const
{
    return this->response_data;
}



void DummyServer::reset()
{
    this->request_data.clear();
    this->expected_request_data.clear();
    this->response_data.clear();
}
