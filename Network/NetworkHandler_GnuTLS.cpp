/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "NetworkHandler_GnuTLS.h"


using namespace MNE::Network;


NetworkHandler_GnuTLS::NetworkHandler_GnuTLS()
{
    gnutls_global_init();
}


NetworkHandler_GnuTLS::~NetworkHandler_GnuTLS()
{
    gnutls_global_deinit();
}


void NetworkHandler_GnuTLS::request(
    std::string host,
    uint16_t port,
    std::weak_ptr<ResponseHandler> response_handler,
    std::vector<uint8_t> request_header,
    std::shared_ptr<DataSource> request_body
    )
{
    if (response_handler.expired()) {
        //No need to do a request.
        return;
    }
    auto handler = response_handler.lock();
    if (!handler) {
        //No handler available.
        return;
    }
    struct addrinfo socket_config;
    memset(&socket_config, 0, sizeof(addrinfo));
    socket_config.ai_family = AF_UNSPEC;
    socket_config.ai_socktype = SOCK_STREAM;
    socket_config.ai_flags = AI_PASSIVE;

    gnutls_certificate_credentials_t credentials;
    if (gnutls_certificate_allocate_credentials(&credentials) < 0) {
        //Memory allocation error.
        return;
    }

    if (gnutls_certificate_set_x509_system_trust(credentials) < 0) {
        //Error setting system certificates.
        gnutls_certificate_free_credentials(credentials);
        return;
    }

    gnutls_session_t session;
    if (gnutls_init(&session, GNUTLS_CLIENT) < 0) {
        //Could not init session.
        gnutls_certificate_free_credentials(credentials);
        return;
    }
    int gnutls_result = gnutls_server_name_set(session, GNUTLS_NAME_DNS, host.c_str(), host.length());
    if (gnutls_result < 0) {
        //Could not set the server name.
        gnutls_deinit(session);
        gnutls_certificate_free_credentials(credentials);
        return;
    }

    if (gnutls_set_default_priority(session) < 0) {
        //Could not set priority.
        gnutls_deinit(session);
        gnutls_certificate_free_credentials(credentials);
        return;
    }

    //Add the crededentials to the session:
    gnutls_result = gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, credentials);
    if (gnutls_result < 0) {
        //Error setting credentials.
        gnutls_deinit(session);
        gnutls_certificate_free_credentials(credentials);
        return;
    }

    int connection = this->openSocket(host, port, &socket_config);
    if (connection == -1) {
        gnutls_deinit(session);
        gnutls_certificate_free_credentials(credentials);
        return;
    }

    gnutls_transport_set_int(session, connection);
    gnutls_handshake_set_timeout(session, GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);

    //Do the handshake:
    int handshake_result = 0;
    do {
        handshake_result = gnutls_handshake(session);
    } while ((handshake_result < 0) && (gnutls_error_is_fatal(handshake_result) == 0));
    //Check for handshake errors:
    if (handshake_result < 0) {
        //TODO: Throw handshake failed exception.
        this->closeSocket(connection);
        gnutls_deinit(session);
        gnutls_certificate_free_credentials(credentials);
        throw MNE::TLSException(
            "NetworkHandler_GnuTLS",
            "Handshake failed: " + std::string(gnutls_strerror(handshake_result)),
            MNE::TLSError::CONNECTION_FAILED
            );
    }

    if (!request_header.empty()) {
        //Send data:
        int send_result = 0;
        do {
            send_result = gnutls_record_send(session, request_header.data(), request_header.size());
        } while (send_result == GNUTLS_E_AGAIN || send_result == GNUTLS_E_INTERRUPTED);
        if (send_result < 0) {
            //Error while sending data.
            this->closeSocket(connection);
            gnutls_deinit(session);
            gnutls_certificate_free_credentials(credentials);
            throw MNE::TLSException(
                "NetworkHandler_GnuTLS",
                "Sending data failed!",
                MNE::TLSError::DATA_TRANSFER
                );
        }
    }
    if (request_body != nullptr) {
        //Send all the data from the body:
        size_t chunk_size = 65536;
        auto body_data = request_body->getData(chunk_size);
        while (!body_data.empty()) {
            int send_result = 0;
            do {
                send_result = gnutls_record_send(session, body_data.data(), body_data.size());
            } while (send_result == GNUTLS_E_AGAIN || send_result == GNUTLS_E_INTERRUPTED);
            if (send_result < 0) {
                //Error while sending data.
                this->closeSocket(connection);
                gnutls_deinit(session);
                gnutls_certificate_free_credentials(credentials);
                throw MNE::TLSException(
                    "NetworkHandler_GnuTLS",
                    "Sending data failed!",
                    MNE::TLSError::DATA_TRANSFER
                    );
            }
            body_data = request_body->getData(chunk_size);
        }
    }

    //Receive the response:

    //fixed_buffer_size is used to prevent effects when
    //another thread is using this network handler and modifies the
    //buffer size. While reading data, it can be a... delicate situation
    //when this->buffer_size is greater than the size of the buffer.
    size_t fixed_buffer_size = this->buffer_size;
    auto buffer = std::make_shared<std::vector<uint8_t>>();
    buffer->reserve(fixed_buffer_size);
    int receive_status = 0;
    do {
        receive_status = gnutls_record_recv(session, (char*)buffer->data(), fixed_buffer_size);
        if (receive_status > 0) {
            //Create a chunk with the size of the received data which is stored
            //in receive_status.
            std::vector<uint8_t>::iterator data_end = buffer->begin();
            std::advance(data_end, receive_status);
            auto chunk = std::make_shared<std::vector<uint8_t>>(buffer->begin(), data_end);
            handler->processResponseData(chunk, (receive_status < fixed_buffer_size));
        }
    } while (receive_status == GNUTLS_E_AGAIN || receive_status == GNUTLS_E_INTERRUPTED);
    bool error = gnutls_bye(session, GNUTLS_SHUT_RDWR) < 0;
    this->closeSocket(connection);
    gnutls_deinit(session);
    gnutls_certificate_free_credentials(credentials);
    if (error) {
        throw MNE::TLSException(
            "NetworkHandler_GnuTLS",
            "Error while closing the session!",
            MNE::TLSError::CONNECTION_CLOSE
            );
    }
}
