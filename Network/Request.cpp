/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Request.h"


using namespace MNE::Network;


//Constructors and destructor:


Request::Request(std::string resource)
    : resource(resource)
{
}


Request::Request(
    std::shared_ptr<NetworkHandler> network_handler,
    std::string host,
    uint16_t port,
    std::string resource,
    uint64_t id
    )
    : network_handler(network_handler),
      host(host),
      port(port),
      resource(resource),
      id(id)
{
}


void Request::processResponseData(
            std::shared_ptr<std::vector<uint8_t>> data,
            bool last_chunk
            )
{
    //Since raw requests don't have any header fields, we can simply release the
    //response_header_ready mutex:
    this->response_header_ready.unlock();
    //lock the buffer mutex, append the data to the buffer
    //and release the mutex again.
    //If the response has been read completely,
    //release the response finished semaphore.
    this->response_buffer_mutex.lock();
    this->response_buffer.insert(this->response_buffer.end(), data->begin(), data->end());
    this->response_buffer_mutex.unlock();
    if (last_chunk) {
        this->request_running_semaphore.unlock();
    }
    //Response data are available:
    this->response_data_available.unlock();
}


Request::~Request()
{
}


void Request::start()
{
    if (this->network_handler == nullptr) {
        //TODO: more detailed handling of this case (exception, message, ...).
        return;
    }

    //Clear the response buffer:
    this->response_buffer.clear();

    this->network_handler->request(
        this->host,
        this->port,
        this->shared_from_this(),
        this->request_buffer,
        this->data_source
        );
}


void Request::setStarted()
{
    this->request_running_semaphore.lock();
    this->response_header_ready.lock();
    this->response_data_available.lock();
}


bool Request::hasFinished()
{
    if (this->request_running_semaphore.try_lock()) {
        this->request_running_semaphore.unlock();
        return true;
    }
    return false;
}


void Request::waitUntilFinished()
{
    this->request_running_semaphore.lock();
    this->request_running_semaphore.unlock();
}


void Request::waitUntilHeaderReady()
{
    this->response_header_ready.lock();
    this->response_header_ready.unlock();
}


void Request::waitUntilResponseDataAvailable()
{
    this->response_data_available.lock();
    this->response_data_available.unlock();
}


//Getters and setters:


void Request::setNetworkHandler(std::shared_ptr<NetworkHandler> network_handler)
{
    this->network_handler = network_handler;
}


std::shared_ptr<NetworkHandler> Request::getNetworkHandler() const
{
    return this->network_handler;
}


void Request::setResource(const std::string& resource)
{
    this->resource = resource;
}


std::string Request::getResource() const
{
    return this->resource;
}


void Request::setId(const uint64_t& id)
{
    this->id = id;
}


uint64_t Request::getId() const
{
    return this->id;
}


std::string Request::getHost() const
{
    return this->host;
}


uint16_t Request::getPort() const
{
    return this->port;
}


size_t Request::getBodySize() const
{
    //The response body size cannot be determined for raw requests
    //since there is no header that can tell it.
    return 0;
}


std::shared_ptr<DataSource> Request::getDataSource()
{
    return this->data_source;
}


void Request::setDataSource(std::shared_ptr<DataSource> data_source)
{
    this->data_source = data_source;
}


std::shared_ptr<std::vector<uint8_t>> Request::getResponseData()
{
    auto data = std::make_shared<std::vector<uint8_t>>();
    this->response_buffer_mutex.lock();
    //TODO: use std::move, if possible:
    data->assign(this->response_buffer.begin(), this->response_buffer.end());
    this->response_buffer.clear();
    this->response_buffer_mutex.unlock();
    return data;
}


std::string Request::getResponseString()
{
    auto data = this->getResponseData();
    if (data == nullptr) {
        return "";
    }
    return std::string(data->begin(), data->end());
}


void Request::abort()
{
    this->aborted = true;
}


bool Request::isAborted() const
{
    return this->aborted;
}
