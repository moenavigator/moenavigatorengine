/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#ifndef REQUEST_H
#define REQUEST_H


#include <cstdint>
#include <map>
#include <memory>
#include <mutex>
#include <vector>


#include "NetworkHandler.h"
#include "ResponseHandler.h"
#include "DataSource.h"



namespace MNE::Network
{
    class Request: public ResponseHandler, public std::enable_shared_from_this<Request>
    {
        protected:

        /**
         * The network handler where the request instance
         * will send and receive data from.
         */
        std::shared_ptr<NetworkHandler> network_handler = nullptr;


        /**
         * The data source for sending data.
         */
        std::shared_ptr<DataSource> data_source = nullptr;


        /**
         * The name of the resource that shall be requested. Depending on the
         * network protocol, this can be a path in the webroot (HTTP),
         * a mailbox folder (IMAP) or something completely different.
         */
        std::string resource;


        /**
         * This is an unique identifier for the request itself
         * so that the "client" that created the request can distinguish
         * between different requests.
         */
        uint64_t id;


        /**
         * The host from where the resource shall be read.
         * This can be an IP address or a domain name.
         * This is a read-only attribute that is derived from the resource
         * attribute.
         */
        std::string host = "";


        /**
         * The port number from where the resource shall be read.
         * This is a read-only attribute that is either derived from the
         * resource attribute or set to the default of the used protocol
         * (defined in a protocol-specific specialisation of this class).
         */
        uint16_t port = 0;


        /**
         * The buffer for the request data.
         */
        std::vector<uint8_t> request_buffer = {};


        /**
         * The buffer for the response data.
         */
        std::vector<uint8_t> response_buffer = {};


        /**
         * This mutex controls exclusive access to the response buffer.
         */
        std::mutex response_buffer_mutex;


        /**
         * This semaphore indicates whether the request
         * is "running" or not: It gets locked when the request starts
         * and it is released when the response has been
         * completely received.
         */
        std::mutex request_running_semaphore;


        /**
         * This mutex acts as a semaphore. It gets locked when the request
         * starts and gets released when the request header has been read
         * completely.
         */
        std::mutex response_header_ready;


        /**
         * This mutex acts as a semaphore. It gets locked when the request
         * starts and gets released after a data packet has been appenden
         * to the (empty) data buffer. It gets locked again after the buffer
         * data is transferred via getResponseData or getResponseString until
         * another block of data is available.
         */
        std::mutex response_data_available;


        /**
         * Indicates whether the request has been aborted (true) or not (false).
         */
        bool aborted = false;


        public:


        //Constructors and destructor:


        Request(std::string resource = "");


        Request(
            std::shared_ptr<NetworkHandler> network_handler,
            std::string host,
            uint16_t port,
            std::string resource,
            uint64_t id = 0
            );


        virtual ~Request();


        /**
         * Starts the request by invoking the network handler
         * and sending data to it.
         */
        virtual void start();


        /**
         * Locks the request_started_semaphore.
         */
        virtual void setStarted();


        /**
         * Returns the state of the request by checking the state
         * of the request_started_semaphore.
         *
         * @returns bool True, if the request has finished, false otherwise.
         */
        virtual bool hasFinished();


        /**
         * Waits until the request_started_semaphore gets unlocked.
         */
        virtual void waitUntilFinished();


        /**
         * Waits until the response header has been completely read.
         */
        virtual void waitUntilHeaderReady();


        /**
         * Waits until response data are available for reading.
         */
        virtual void waitUntilResponseDataAvailable();


        /**
         * @see ResponseHandler::processResponseData
         */
        virtual void processResponseData(
            std::shared_ptr<std::vector<uint8_t>> data,
            bool last_chunk = false
            );


        //Getters and setters:


        virtual void setNetworkHandler(std::shared_ptr<NetworkHandler> network_handler = nullptr);


        virtual std::shared_ptr<NetworkHandler> getNetworkHandler() const;


        virtual void setResource(const std::string& resource = "");


        virtual std::string getResource() const;


        virtual void setId(const uint64_t& id = 0);


        virtual uint64_t getId() const;


        virtual std::string getHost() const;


        virtual uint16_t getPort() const;


        virtual size_t getBodySize() const;


        /**
         * Returns the data source for the request.
         *
         * @returns std::shared_ptr<DataSource> A data source instance
         *     or a nullptr in case no data source is attached to the request.
         */
        virtual std::shared_ptr<DataSource> getDataSource();


        /**
         * Sets a data source for the request.
         *
         * @param std::shared_ptr<DataSource> data source The data source
         *     to be set.
         */
        virtual void setDataSource(std::shared_ptr<DataSource> data_source);


        /**
         * Transfers the read response data from the response buffer
         * into a temporary buffer and returns that buffer.
         *
         * @returns std::shared_ptr<std::vector<uint8_t>> The data from the
         *     response buffer.
         */
        virtual std::shared_ptr<std::vector<uint8_t>> getResponseData();


        /**
         * Transfers the read response data from the response buffer
         * into a string and returns that string.
         *
         * @return std::string The data from the response buffer as string.
         */
        virtual std::string getResponseString();


        /**
         * Aborts the request by setting the aborted flag.
         */
        virtual void abort();


        /**
         * Returns the status of the aborted flag which indicates, if the
         * request has been aborted or not.
         *
         * @returns bool True, if the aborted flag is set, false otherwise.
         */
        virtual bool isAborted() const;
    };
}


#endif
