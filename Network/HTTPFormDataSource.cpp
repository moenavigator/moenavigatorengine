/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "HTTPFormDataSource.h"
#include "../CommonClasses/URL.h"
#include "../CommonClasses/Hash.h"


using namespace MNE::Network;


std::vector<uint8_t> HTTPFormDataSource::getData(const size_t chunk_size)
{
    std::vector<uint8_t> output;
    if (items.empty()) {
        //There is no need to output any data.
        return {};
    }

    size_t current_chunk_size = 0;
    if (this->chunk_overflow.size() > 0) {
        //There are encoded data from the last chunk.
        if (this->chunk_overflow.size() > chunk_size) {
            //The encoded data is bigger than one chunk. It has to be split.
            auto part_end = this->chunk_overflow.begin();
            std::advance(part_end, chunk_size);
            output.insert(
                output.begin(),
                this->chunk_overflow.begin(),
                part_end
                );
            this->chunk_overflow.erase(this->chunk_overflow.begin(), part_end);
            //We can return here since we have a full chunk.
            return output;
        } else {
            output.insert(
                output.begin(),
                this->chunk_overflow.begin(),
                this->chunk_overflow.end()
                );
            current_chunk_size = this->chunk_overflow.size();
            this->chunk_overflow.clear();
        }
    }
    if (this->current_item_pos == -1) {
        this->current_item_pos = 0;
    }
    while ((current_chunk_size < chunk_size) && this->current_item_pos < this->items.size()) {
        if (this->url_encode) {
            //Output data as urlencoded (percent encoded) data.
            std::string encoded_item = "";
            if (this->current_item_pos > 0) {
                //It is not the first item. Add an ampersand before
                //the next item.
                encoded_item += '&';
            }
            auto current_item = this->items.at(this->current_item_pos);
            std::vector<uint8_t> name_vector(current_item.first.begin(), current_item.first.end());
            encoded_item += MNE::URL::percentEncode(name_vector, true);
            encoded_item += '=';
            encoded_item += MNE::URL::percentEncode(current_item.second, true);
            current_chunk_size += encoded_item.size();
            if (current_chunk_size > chunk_size) {
                //Only add a part of the encoded item to the output and put the
                //rest into the chunk_overflow vector.
                auto available_space = chunk_size - output.size();
                if (available_space > 0) {
                    //Make the chunk full.
                    auto encoded_item_part = encoded_item.substr(0, available_space);
                    output.insert(output.end(), encoded_item_part.begin(), encoded_item_part.end());
                    current_chunk_size += available_space;
                    auto leftover_part = encoded_item.substr(available_space, std::string::npos);
                    this->chunk_overflow.assign(leftover_part.begin(), leftover_part.end());
                } else {
                    //No space left in the current chunk.
                    //Put everything in the chunk_overflow buffer.
                    this->chunk_overflow.assign(encoded_item.begin(), encoded_item.end());
                }
            } else {
                //Add everything to the output:
                output.insert(output.end(), encoded_item.begin(), encoded_item.end());
            }
        } else {
            //Output data as multipart form data.
            std::vector<uint8_t> encoded_item;
            std::string item_header;
            if (this->current_item_pos == 0) {
                //It is the first item. We must add a boundary
                //to the item header.
                item_header = "--" + this->boundary + "\r\n";
            }
            auto current_item = this->items.at(this->current_item_pos);
            item_header += "Content-Disposition: form-data; name=\""
                + current_item.first + "\"\r\n";
            std::string item_footer = "\r\n--" + this->boundary + "\r\n";
            //Check if the field value has to be base64 encoded or not:
            if (HTTPFormDataSource::valueNeedsBase64Encoding(current_item.second)) {
                item_header += "Content-Type: application/octet-stream\r\n"
                    "Content-Transfer-Encoding: base64\r\n\r\n";
                auto base64_item = MNE::Functions::base64Encode(current_item.second);
                encoded_item.assign(item_header.begin(), item_header.end());
                encoded_item.insert(
                    encoded_item.end(),
                    base64_item.begin(),
                    base64_item.end()
                    );
                encoded_item.insert(
                    encoded_item.end(),
                    item_footer.begin(),
                    item_footer.end()
                    );
            } else {
                item_header += "\r\n"; //CR + LF
                encoded_item.assign(item_header.begin(), item_header.end());
                encoded_item.insert(
                    encoded_item.end(),
                    current_item.second.begin(),
                    current_item.second.end()
                    );
                encoded_item.insert(
                    encoded_item.end(),
                    item_footer.begin(),
                    item_footer.end()
                    );
            }
            if (current_chunk_size + encoded_item.size() > chunk_size) {
                //The current item will not fully fit into the current chunk.
                auto available_space = chunk_size - output.size();
                if (available_space > 0) {
                    //Make the chunk full.
                    auto encoded_item_split = encoded_item.begin();
                    std::advance(encoded_item_split, available_space);
                    output.insert(output.end(), encoded_item.begin(), encoded_item_split);
                    current_chunk_size += available_space;
                    this->chunk_overflow.assign(encoded_item_split, encoded_item.end());
                }
            } else {
                //The current item will fit into the chunk.
                output.insert(output.end(), encoded_item.begin(), encoded_item.end());
            }
        }
        this->current_item_pos++;
    }
    return output;
}


std::string HTTPFormDataSource::getMimeType()
{
    if (this->url_encode) {
        return "application/x-www-form-urlencoded";
    } else {
        return "multipart/formdata";
    }
}


void HTTPFormDataSource::addItem(const std::string& name, const std::string& value)
{
    this->addItem(name, std::vector<uint8_t>(value.begin(), value.end()));
}


void HTTPFormDataSource::addItem(const std::string& name, std::vector<uint8_t> value)
{
    this->items.push_back(std::pair<std::string, std::vector<uint8_t>>(name, value));
}


std::vector<uint8_t> HTTPFormDataSource::getItem(const std::string& name)
{
    for (auto item: this->items) {
        if (item.first == name) {
            return item.second;
        }
    }
    //Item not found.
    return {};
}


std::vector<std::vector<uint8_t>> HTTPFormDataSource::getAllItems(const std::string& name)
{
    std::vector<std::vector<uint8_t>> result;
    for (auto item: this->items) {
        if (item.first == name) {
            result.push_back(item.second);
        }
    }
    return result;
}


void HTTPFormDataSource::removeItems(const std::string& name)
{
    std::vector<std::pair<std::string, std::vector<uint8_t>>> new_items;
    for (auto item: this->items) {
        if (item.first != name) {
            new_items.push_back(item);
        }
    }

    this->items = new_items;
    //We must reset the current_item_pos counter, since it is invalid now.
    this->current_item_pos = -1;
}


bool HTTPFormDataSource::hasItem(const std::string& name)
{
    for (auto item: this->items) {
        if (item.first == name) {
            return true;
        }
    }
    //Item not found.
    return false;
}


void HTTPFormDataSource::enableURLEncode()
{
    this->url_encode = true;
}


void HTTPFormDataSource::disableURLEncode()
{
    this->url_encode = false;
}


bool HTTPFormDataSource::urlEncodeEnabled()
{
    return this->url_encode;
}


void HTTPFormDataSource::setBoundary(const std::string& boundary)
{
    this->boundary = boundary;
}


std::string HTTPFormDataSource::getBoundary()
{
    return this->boundary;
}


void HTTPFormDataSource::reset()
{
    this->current_item_pos = -1;
    this->chunk_overflow.clear();
    this->boundary.clear();
}


bool HTTPFormDataSource::valueNeedsBase64Encoding(const std::vector<uint8_t>& value)
{
    for (auto byte: value) {
        if (byte > 0x7f) {
            //We have found a byte with a value greater than 127.
            //Base64 encoding is required.
            return true;
        }
    }
    //No byte has a value greater than 127. Base64 encoding is not required.
    return false;
}
