/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "GopherRequest.h"


using namespace MNE::Network;


//Request class overrides:


GopherRequest::GopherRequest(
    std::shared_ptr<NetworkHandler> network_handler,
    std::string host,
    uint16_t port,
    std::string resource,
    uint64_t id
    )
    : Request(network_handler, host, port, resource, id)
{
    //Check if the resource has a tab character in it.
    //If so, split the resource (path) on the first occurrence of
    //that character and treat everything after that character
    //as search string.
    size_t search_string_pos = this->resource.find('\t');
    if (search_string_pos != std::string::npos) {
        //The resource contains a search string.
        size_t length = this->resource.length();
        if (search_string_pos + 1 < length) {
            std::string search_string = this->resource.substr(search_string_pos + 1);
            if (!search_string.empty()) {
                //Set the search string and cut the resource string:
                this->setSearchString(search_string);
                this->resource = resource.substr(0, search_string_pos);
            }
        }
    }
}


void GopherRequest::start()
{
    //Build the query string:

    std::string query_string = "";
    if (!this->resource.empty()) {
        //In gopher, the resource is optional.
        query_string += this->resource;
    }
    if (!this->search_string.empty()) {
        query_string += "\t" + this->search_string;
    }

    query_string += "\r\n";

    this->request_buffer = std::vector<uint8_t>(query_string.begin(), query_string.end());

    Request::start();
}


//Own methods:


void GopherRequest::setSearchString(std::string& search_string)
{
    this->search_string = search_string;
}


std::string GopherRequest::getSearchString()
{
    return this->search_string;
}


char GopherRequest::getItemType()
{
    if (this->resource.empty()) {
        //We request a menu.
        return '1';
    }
    auto first_char = this->resource.begin();
    if (*first_char == '/') {
        //move one character forward:
        first_char++;
    }
    if (first_char == this->resource.end()) {
        //The resource is "/". We are still requesting a menu.
        return '1';
    }
    //We might have found the item type. If a slash follows,
    //we definetly found it.
    char item_type = *first_char;
    first_char++;
    if (first_char == this->resource.end()) {
        //We have requested a resource that has one single character
        //as name. Assume item type text:
        return '0';
    }
    if (*first_char == '/') {
        //We found the item type.
        return item_type;
    }
    //At this point, we can only assume that text is requested.
    return '0';
}
