/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNE__NETWORK__MIDDLEWARE_H
#define MNE__NETWORK__MIDDLEWARE_H


#include "NetworkHandler.h"


namespace MNE::Network
{
    /**
     * The Middleware class allows components to place themselves
     * between the network handler and a request, so that all data
     * exchanged between the two must pass through the Middleware instance.
     * The Middleware instance can manipulate or encapsulate the network data
     * for different purposes, depending on the Middleware implementation.
     */
    class Middleware: public NetworkHandler
    {
        protected:


        /**
         * A pointer to the network handler instance that gets data to be sent
         * from the Middleware and that passes received data to this Middleware.
         */
        std::shared_ptr<NetworkHandler> handler;


        public:


        /**
         * The constructor for the Middleware class.
         * It requires a network handler instance.
         */
        Middleware(std::shared_ptr<NetworkHandler> handler);


        /**
         * The default virtual destructor.
         */
        virtual ~Middleware() = default;


        /**
         * Sets the network handler instance to be used in the Middleware.
         */
        virtual void setHandler(std::shared_ptr<NetworkHandler> handler);


        /**
         * Returns the network handler that is used by the Middleware.
         *
         * @returns std::shared_ptr<NetworkHandler> The network handler instance
         *     that is used in the Middleware instance. This pointer might be
         *     a nullptr in case no network handler instance is set.
         */
        virtual std::shared_ptr<NetworkHandler> getHandler();
    };
}


#endif
