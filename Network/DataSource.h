/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#ifndef MNE__NETWORK__DATASOURCE_H
#define MNE__NETWORK__DATASOURCE_H


#include <cstdint>
#include <string>
#include <vector>


namespace MNE::Network
{
    /**
     * The DataSource interface defines the methods for implementations
     * that provide data that shall be sent in a network request using a
     * Request object.
     */
    class DataSource
    {
        public:


        /**
         * Returns the next chunk of request data.
         *
         * @param size_t chunk_size The maximum amount of bytes to return.
         *
         * @returns std::vector<uint8_t> The next chunk of request data.
         *     If all of the request data have been read, the vector is empty.
         */
        virtual std::vector<uint8_t> getData(const size_t chunk_size = 65536) = 0;


        /**
         * Returns the mime type of the data that the DataSource implementation
         * will deliver via the getData method.
         *
         * @returns std::string The mime type for the provided data.
         */
        virtual std::string getMimeType() = 0;


        /**
         * The default virtual destructor.
         */
        virtual ~DataSource() = default;
    };
}


#endif
