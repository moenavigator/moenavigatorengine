/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "NetworkHandler_Dummy.h"


using namespace MNE::Network;


void NetworkHandler_Dummy::request(
    std::string host,
    uint16_t port,
    std::weak_ptr<ResponseHandler> response_handler,
    std::vector<uint8_t> request_header,
    std::shared_ptr<DataSource> request_body
    )
{
    if (this->dummy_server != nullptr) {
        this->dummy_server->readRequestData(request_header, (request_body == nullptr));
        if (request_body != nullptr) {
            auto body = request_body->getData(this->chunk_size);
            while (!body.empty()) {
                this->dummy_server->readRequestData(body, (body.size() < this->chunk_size));
                body = request_body->getData(this->chunk_size);
            }
        }
        if (!response_handler.expired()) {
            auto handler = response_handler.lock();
            if (handler) {
                auto ptr_data = std::make_shared<std::vector<uint8_t>>(
                    this->dummy_server->getResponseData()
                    );
                handler->processResponseData(ptr_data, true);
            }
        }
    }
}


void NetworkHandler_Dummy::setBufferSize(size_t buffer_size)
{
    if (buffer_size > 0) {
        this->chunk_size = buffer_size;
    } else {
        throw MNE::Exception(
            "NetworkHandler_Dummy",
            "setBufferSize: Invalid buffer size (0 or negative)!",
            1
            );
    }
}


void NetworkHandler_Dummy::setDummyServer(
    std::shared_ptr<DummyServer> dummy_server
    )
{
    this->dummy_server = dummy_server;
}
