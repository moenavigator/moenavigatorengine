/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNE__NETWORK__DUMMYSERVER_H
#define MNE__NETWORK__DUMMYSERVER_H


#include <vector>


#include "../Exceptions/Exception.h"


namespace MNE::Network
{
    /**
     * The purpose of the DummyServer class is to emulate a server
     * so that Request classes can be tested without having to interact
     * with a real server. This avoids network problems during testing.
     */
    class DummyServer
    {
        protected:


        /**
         * The data that are expected to be sent by a request.
         */
        std::vector<uint8_t> expected_request_data = {};


        /**
         * The data that are sent by a request.
         */
        std::vector<uint8_t> request_data = {};


        /**
         * The data that shall be sent as a repsonse to a request.
         */
        std::vector<uint8_t> response_data = {};


        public:


        /**
         * Sets the data that shall be received by this server from a
         * request.
         *
         * @param const std::vector<uint8_t>& data The expected data to be
         *     received.
         */
        virtual void setExpectedRequestData(const std::vector<uint8_t>& data);


        /**
         * Sets the response data for a request.
         *
         * @param const std::vector<uint8_t>& data The data to be sent
         *     as response.
         */
        virtual void setResponseData(const std::vector<uint8_t>& data);


        /**
         * Reads a chunk of data from a request.
         *
         * @param const std::vector<uint8_t>& data The data from the request.
         *
         * @param bool last_chunk Whether the data chunk is the last one (true)
         *     or not (false). Defaults to false.
         *
         * @throws MNE::Exception An exception is thrown if the last chunk is
         *     read and the request data is not the same as the expected
         *     request data.
         */
        virtual void readRequestData(
            const std::vector<uint8_t>& data,
            bool last_chunk = false
            );


        /**
         * @returns std::vector<uint8_t> Returns the response data.
         */
        virtual std::vector<uint8_t> getResponseData() const;


        /**
         * Resets the dummy server to its inital state.
         */
        void reset();
    };
}


#endif
