/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#ifndef MNE__NETWORK__HTTPFORMDATASOURCE_H
#define MNE__NETWORK__HTTPFORMDATASOURCE_H


#include <string>
#include <utility>
#include <vector>


#include "DataSource.h"
#include "../CommonClasses/Functions.h"


namespace MNE::Network
{
    /**
     * The HTTPDataSource class generates data for a HTTP Request body.
     */
    class HTTPFormDataSource: public DataSource
    {
        protected:


        /**
         * This flag specifies whether to use URL encoding (true)
         * or encode items as multipart form data (false).
         * Defaults to false.
         */
        bool url_encode = false;


        /**
         * The list of items in the form data. It is a vector instead of a
         * map to keep the items in the order they have been inserted.
         */
        std::vector<std::pair<std::string, std::vector<uint8_t>>> items;


        /**
         * The current item position that is being output.
         * It is necessary to keep track of the current position
         * in the item list when preparing data for getData in chunks.
         */
        size_t current_item_pos = -1;


        /**
         * The boundary for items when the data shall be encoded as
         * multipart form data.
         */
        std::string boundary;


        /**
         * A buffer for the "overflow" that can be produced when data
         * for a chunk is encoded and the encoded data would exceed the
         * chunk size. In that case, the overflow is stored in here and
         * included in the beginning of the next chunk.
         */
        std::vector<uint8_t> chunk_overflow;


        public:


        /**
         * @see DataSource::getData
         */
        virtual std::vector<uint8_t> getData(const size_t chunk_size = 65536) override;


        /**
         * @see DataSource::getMimeType
         */
        virtual std::string getMimeType() override;


        /**
         * Adds an item to the set of data.
         * This method is useful for setting form data which are ususally
         * key-value pairs where both the key and the value are strings.
         *
         * @param const std::string& name The name of the item (the key).
         *
         * @param const std::string& value The value of the item.
         */
        virtual void addItem(const std::string& name, const std::string& value = "");


        /**
         * Another variant of the addItem method that allows addings items
         * containing binary data.
         */
        virtual void addItem(const std::string& name, std::vector<uint8_t> value = {});


        /**
         * Returns the first item with the specified name, if such items exists.
         *
         * @returns std::vector<uint8_t> The value of the item, if it exists.
         *     If no such item exists, the returned vector is empty.
         *
         * @throws MNE::Exception if the item cannot be found.
         */
        virtual std::vector<uint8_t> getItem(const std::string& name);


        /**
         * Returns all item values with the specified name, if such items exist.
         *
         * @returns std::vector<std::vector<uint8_t>> The found item values.
         *     If no items with the name can be found, the returned vector
         *     is empty.
         *
         * @throws MNE::Exception if the item cannot be found.
         *
         */
        virtual std::vector<std::vector<uint8_t>> getAllItems(const std::string& name);


        /**
         * Removes all items from the set of data that have the specified name.
         *
         * @param const std::string& name The item to be removed.
         */
        virtual void removeItems(const std::string& name);


        /**
         * Determines whether the specified item is in the set of data.
         *
         * @param const std::string& name The item that shall be found.
         *
         * @returns bool True, if the item is in the set of data,
         *     false otherwise.
         */
        virtual bool hasItem(const std::string& name);


        /**
         * Enables the encoding of items as URL encoded form data.
         */
        virtual void enableURLEncode();


        /**
         * Disables the encoding of items as URL encoded form data.
         */
        virtual void disableURLEncode();


        /**
         * Returns whether URL encoding of items is enabled or not.
         *
         * @returns True, if URL encoding is enabled, false otherwise.
         */
        virtual bool urlEncodeEnabled();


        /**
         * Sets the boundary for multipart form data.
         *
         * @param const std::string& boundary The boundary to be used.
         */
        virtual void setBoundary(const std::string& boundary);


        /**
         * Returns the boundary that is currently set for multipart form data.
         *
         * @returns std::string The currently set boundary.
         */
        virtual std::string getBoundary();


        /**
         * Resets the output counters so that all the form data can be
         * re-generated again from the beginning.
         */
        virtual void reset();


        /**
         * Checks whether the specified value must be base64 encoded.
         * This means that the value contains bytes outside the 7-bit ASCII
         * range.
         *
         * @param const std::vector<uint8_t>& value The value to check.
         *
         * @returns True if the value must be base64 encoded, false otherwise.
         */
        static bool valueNeedsBase64Encoding(const std::vector<uint8_t>& value);
    };
}


#endif
