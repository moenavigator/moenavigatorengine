/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "HTTPRequest.h"


using namespace MNE::Network;


HTTPRequest::HTTPRequest(
    std::shared_ptr<NetworkHandler> network_handler,
    std::string host,
    uint16_t port,
    std::string resource,
    uint64_t id
    )
    : Request(network_handler, host, port, resource, id)
{
    //nothing else here
}


void HTTPRequest::start()
{
    if (this->network_handler == nullptr) {
        return;
    }

    if (this->method.empty()) {
        return;
    }

    //Put the main header fields in the request string:

    std::string request_string = this->method + " " + this->resource + " HTTP/1.0\r\n"
        + "Host: " + this->host + "\r\n";

    //Append all other header fields:
    for (auto field: this->request_header_fields) {
        request_string += field.first + ": " + field.second + "\r\n";
    }

    //Add an epmty line to mark the end of the header:
    request_string += "\r\n";

    //Put the request string into the request buffer:
    this->request_buffer = std::vector<uint8_t>(request_string.begin(), request_string.end());

    Request::start();
}



void HTTPRequest::parseHeader()
{
    //Parse the header... how?
    //Well, a good first try is to split lines by
    //seeking the sequence CR+LF. After that, each line
    //can be split by the first colon to get the key and the value
    //of the header field.
    std::vector<uint8_t> line;
    //Since we are dealing with uint8-vectors, we define the line ending
    //(CR + LF) as vector, too:
    const std::vector<uint8_t> line_end = {13, 10};

    //Read the first header line since it has a special syntax.
    auto line_start_pos = this->response_header_buffer.begin();
    auto line_end_pos = std::search(
        this->response_header_buffer.begin(),
        this->response_header_buffer.end(),
        line_end.begin(),
        line_end.end()
        );
    std::string first_line(line_start_pos, line_end_pos);
    std::stringstream first_line_items(first_line);
    std::string protocol_and_version;
    first_line_items >> protocol_and_version >> this->response_status_code >> this->response_status_message;

    auto protocol_start_pos = protocol_and_version.find('/', 0);
    if (protocol_start_pos == std::string::npos) {
        //Not an HTTP header.
        //TODO: throw something instead than just returning
        return;
    }
    this->response_http_version = protocol_and_version.substr(protocol_start_pos + 1, std::string::npos);

    auto remaining_elements = std::distance(line_start_pos, this->response_header_buffer.end());
    if (remaining_elements < 2) {
        //Nothing left to read. That's odd...
        return;
    }
    line_start_pos = std::next(line_end_pos, 2);

    //Now we can read the next line:
    line_end_pos = std::search(
        line_start_pos,
        this->response_header_buffer.end(),
        line_end.begin(),
        line_end.end()
        );
    while (line_end_pos != this->response_header_buffer.end()) {
        line.assign(line_start_pos, line_end_pos);
        //Add a size limitation to avoid very large header fields:
        if (line.size() <= 8192) {
            //The colon character has the integer value 58.
            auto first_colon = std::find(line.begin(), line.end(), 58);
            if (first_colon != line.end()) {
                std::string key(line.begin(), first_colon);
                if (std::distance(first_colon, line.end()) > 2) {
                    //For the value, the first_colon iterator has to be moved
                    //two characters forward to avoid having the space character
                    //after the colon inside the value.
                    std::advance(first_colon, 2);
                    std::string value(first_colon, line.end());
                    this->response_header_fields.insert(std::make_pair(key, value));
                } else {
                    //A header field with an empty value.
                    this->response_header_fields.insert(std::make_pair(key, ""));
                }
            }
        }
        line_start_pos = std::next(line_end_pos, 2);
        line_end_pos = std::search(
            line_start_pos,
            this->response_header_buffer.end(),
            line_end.begin(),
            line_end.end()
            );
    }
    //At this point, the header has been read completely.
    this->response_header_ready.unlock();
}


void HTTPRequest::setStarted()
{
    Request::setStarted();
    this->response_header_ready.lock();
}


void HTTPRequest::processResponseData(
    std::shared_ptr<std::vector<uint8_t>> data,
    bool last_chunk
    )
{
    if (data == nullptr) {
        //Nothing to do.
        return;
    }
    if (this->response_header_read) {
        //Let the base class handle the response body.
        Request::processResponseData(data, last_chunk);
    } else {
        std::vector<uint8_t> header_end = {13, 10, 13, 10};

        //Append all of data to the response header buffer:
        this->response_header_buffer.insert(this->response_header_buffer.end(), data->begin(), data->end());

        //Find the header fields and store them somewhere else.
        //The header ends with a blank line (sequence CR,LF,CR,LF).
        //The data after that line belong to the response body.
        auto header_end_pos = std::search(
            this->response_header_buffer.begin(),
            this->response_header_buffer.end(),
            header_end.begin(),
            header_end.end()
            );

        if (header_end_pos != this->response_header_buffer.end()) {
            //The end of the header has been found. Remove the body part from
            //the response header buffer and pass it to the base class
            //processResponseData method.
            auto elements_until_end = std::distance(header_end_pos, this->response_header_buffer.end());
            if (elements_until_end > 4) {
                //There is at least one element (byte) that belongs to the body.
                auto body_start_pos = std::next(header_end_pos, 4);
                auto new_body = std::make_shared<std::vector<uint8_t>>(body_start_pos, this->response_header_buffer.end());
                //Remove the body part from the response header buffer:
                this->response_header_buffer.erase(body_start_pos, this->response_header_buffer.end());
                //Pass the body to the request class:
                Request::processResponseData(new_body, last_chunk);
            }
            //Parse the header buffer and set the response_header_read flag.
            this->parseHeader();
            this->response_header_read = true;
        }
    }
}


size_t HTTPRequest::getBodySize() const
{
    return Request::getBodySize();
}


std::shared_ptr<std::vector<uint8_t>> HTTPRequest::getResponseData()
{
    return Request::getResponseData();
}


void HTTPRequest::addRequestHeader(std::string key, std::string value)
{
    this->request_header_fields.insert(
        std::pair<std::string, std::string>(key, value)
        );
}


std::unordered_map<std::string, std::string> HTTPRequest::getResponseHeader()
{
    return this->response_header_fields;
}


void HTTPRequest::setMethod(const std::string& method)
{
    this->method = method;
}


std::string HTTPRequest::getMethod()
{
    return this->method;
}


void HTTPRequest::setRequestHTTPVersion(std::string version)
{
    this->request_http_version = version;
}


std::string HTTPRequest::getRequestHTTPVersion()
{
    return this->request_http_version;
}


uint16_t HTTPRequest::getResponseStatusCode()
{
    return this->response_status_code;
}


std::string HTTPRequest::getResponseStatusMessage()
{
    return this->response_status_message;
}
