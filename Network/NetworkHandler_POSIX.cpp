/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "NetworkHandler_POSIX.h"


using namespace MNE::Network;




void NetworkHandler_POSIX::request(
    std::string host,
    uint16_t port,
    std::weak_ptr<ResponseHandler> response_handler,
    std::vector<uint8_t> request_header,
    std::shared_ptr<DataSource> request_body
    )
{
    if (host.empty()) {
        //No host? No data.
        return;
    }
    if (port < 1) {
        //No port? No data either.
        return;
    }
    if (response_handler.expired()) {
        //Why bother to request data when there is no object to handle
        //the response?
        return;
    }

    auto handler = response_handler.lock();
    if (!handler) {
        //The pointer to the response handler has expired.
        return;
    }

    struct addrinfo socket_config;
    memset(&socket_config, 0, sizeof(addrinfo));
    socket_config.ai_family = AF_UNSPEC;
    socket_config.ai_socktype = SOCK_STREAM;
    socket_config.ai_flags = AI_PASSIVE;

    int connection = this->openSocket(host, port, &socket_config);
    //At this point we should have a connection:
    if (connection == -1) {
        //No, we don't have a connection.
        return;
    }

    if (!request_header.empty()) {
        int sent_bytes = 0;
        uint8_t* header_ptr = request_header.data();
        if ((header_ptr != nullptr) && (request_header.size() > 0)) {
            sent_bytes = send(
                connection,
                header_ptr, //That's a lot of data ;)
                request_header.size(),
                0
                );
        }
    }
    if (request_body != nullptr) {
        //Send all the data from the body:
        size_t chunk_size = 65536;
        int sent_bytes = 0;
        auto body_data = request_body->getData(chunk_size);
        while (!body_data.empty()) {
            sent_bytes = send(
                connection,
                body_data.data(),
                body_data.size(),
                0
                );
            body_data = request_body->getData(chunk_size);
        }
    }

    //Receive the response:

    size_t max_bytes = this->buffer_size;

    auto buffer = std::make_shared<std::vector<uint8_t>>();
    buffer->reserve(max_bytes);
    int received_bytes = 0;

    do {
        received_bytes = recv(connection, (char*)buffer->data(), max_bytes, 0);
        if(received_bytes > 0) {
            //Create a chunk with the size of the received data.
            std::vector<uint8_t>::iterator data_end = buffer->begin();
            std::advance(data_end, received_bytes);
            auto chunk = std::make_shared<std::vector<uint8_t>>(buffer->begin(), data_end);
            handler->processResponseData(chunk, (received_bytes < max_bytes));
        }
    } while (received_bytes > 0);

    this->closeSocket(connection);
}


void NetworkHandler_POSIX::setBufferSize(size_t buffer_size)
{
    if (buffer_size > 0) {
        this->buffer_size = buffer_size;
    }
}


struct addrinfo* NetworkHandler_POSIX::resolveAddress(
    const std::string& host,
    const uint16_t port,
    struct addrinfo* socket_config
    )
{
    std::string port_str = std::to_string(port);
    struct addrinfo* resolved_address = nullptr;
    int status = getaddrinfo(host.c_str(), port_str.c_str(), socket_config, &resolved_address);
    if (status != 0) {
        if (resolved_address != nullptr) {
            freeaddrinfo(resolved_address);
        }
        return nullptr;
    }

    return resolved_address;
}



int NetworkHandler_POSIX::openSocket(
    const std::string& host,
    const uint16_t port,
    struct addrinfo* socket_config
    )
{
    if (socket_config == nullptr) {
        return -1;
    }

    struct addrinfo* addr_info = this->resolveAddress(host, port, socket_config);
    if (addr_info == nullptr) {
        return -1;
    }

    int connection = -1;

    for (struct addrinfo* result_ptr = addr_info;
        result_ptr != nullptr;
        result_ptr = result_ptr->ai_next) {
        connection = socket(
            result_ptr->ai_family,
            result_ptr->ai_socktype,
            result_ptr->ai_protocol
            );

        if (connection == -1) {
            freeaddrinfo(addr_info);
            return -1;
        }

        //Call the POSIX connect command.
        //The scope resolution operator is used
        //to avoid compilers trying to call
        //MNENetworkHandler_POSIX::connect here.
        if (::connect(
                connection,
                result_ptr->ai_addr,
                result_ptr->ai_addrlen)
            != -1) {
            break;
        } else {
            close(connection);
        }
    }
    freeaddrinfo(addr_info);
    return connection;
}


void NetworkHandler_POSIX::closeSocket(long int socket)
{
    if (socket > -1) {
        close(socket);
    }
}
