/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef NETWORKHANDLER_H
#define NETWORKHANDLER_H


#include <cstdint>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "../MNECommon/MNECommon.h"
#include "../CommonClasses/URL.h"
#include "DataSource.h"
#include "ResponseHandler.h"


namespace MNE::Network
{


    class Connection
    {
        public:


        MNE::URL URL;
    };




    /**
     * An interface to a network library which
     * enables MoeNavigatorEngine to input and output
     * data via network interfaces.
     */
    class NetworkHandler
    {
        public:


        /**
         * Initiates a (raw) connection to a specified host and port.
         * When the connection is made, the specified data is sent.
         * Received data is passed to the response handler (if any).
         *
         * @param std::string host The host to connect to. This can be
         *     an IP address or a domain.
         *
         * @param uint16_t port The port on the host to connect to.
         *
         * @param std::weak_ptr<ResponseHandler> response_handler
         *    The handler for the response data. If it is not set,
         *    all the response data is discarded.
         *
         * @param std::vector<uint8_t> request_header The header data to be sent
         *     when the connection is established.
         *
         * @param std::shared_ptr<DataSource> request_body
         *    An optional request body that shall be sent after the
         *    data from the request_header parameter has been sent.
         */
        virtual void request(
            std::string host,
            uint16_t port,
            std::weak_ptr<ResponseHandler> response_handler,
            std::vector<uint8_t> request_header = {},
            std::shared_ptr<DataSource> request_body = nullptr
            ) = 0;


        /**
         * Sets the buffer size when reading or writing data.
         *
         * @param size_t buffer_size The buffer size to be set.
         */
        virtual void setBufferSize(size_t buffer_size) = 0;


        /**
         * Default virtual destructor.
         */
        virtual ~NetworkHandler() = default;
    };
}


#endif
