/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#ifndef HTTPREQUEST_H
#define HTTPREQUEST_H


#include <algorithm>
#include <cstdint>
#include <iterator>
#include <sstream>
#include <unordered_map>

#include "Request.h"



namespace MNE::Network
{
    /**
     * The Request_HTTP class is a specialisation of the raw request class
     * for sending and receiving data via the Hypertext Transfer Protocol.
     */
    class HTTPRequest: public Request
    {
        protected:


        /**
         * This string stores the HTTP method that shall be used,
         * e.g. "GET", "POST", "PUT", etc.
         */
        std::string method = "GET";


        /**
         * All request header fields are placed in here.
         */
        std::unordered_map<std::string, std::string> request_header_fields;


        /**
         * All repsonse header fields are placed in here.
         */
        std::unordered_map<std::string, std::string> response_header_fields;


        /**
         * The buffer for the response header in the phase when the
         * response header hasn't been read completely yet.
         */
        std::vector<uint8_t> response_header_buffer = {};


        /**
         * The HTTP version used in the request.
         */
        std::string request_http_version = "1.0";


        /**
         * The HTTP version used in the response.
         */
        std::string response_http_version = "1.0";


        uint16_t response_status_code = 0;


        std::string response_status_message = "";


        /**
         * Indicates whether the response header has been extracted
         * from the response data.
         */
        bool response_header_read = false;


        /**
         * A semaphore indicating if the response header is ready
         * and can be returned via the getResponseHeader method.
         * This semaphore gets unlocked by the parseHeader method
         * when it finished parsing the header buffer.
         */
        std::mutex response_header_ready;


        virtual void parseHeader();


        public:


        HTTPRequest(
            std::shared_ptr<NetworkHandler> network_handler,
            std::string host,
            uint16_t port,
            std::string resource,
            uint64_t id = 0
            );


        //Request class overrides:


        /**
         * @see Request::start
         */
        virtual void start() override;


        /**
         * @see Request::setStarted
         */
        virtual void setStarted() override;


        /**
         * @see ResponseHandler::processResponseData
         */
        virtual void processResponseData(
            std::shared_ptr<std::vector<uint8_t>> data,
            bool last_chunk = false
            ) override;


        /**
         * @see Request::getBodySize
         */
        virtual size_t getBodySize() const override;


        /**
         * @see Request::getResponseData
         */
        virtual std::shared_ptr<std::vector<uint8_t>> getResponseData() override;


        //Methods that are special to this class:


        /**
         * Adds a request header field to the request.
         */
        virtual void addRequestHeader(std::string key, std::string value);


        /**
         * Returns all fields of the response header.
         * This method will block until the response header is read.
         */
        virtual std::unordered_map<std::string, std::string> getResponseHeader();


        virtual void setMethod(const std::string& method);


        virtual std::string getMethod();


        /**
         * Sets the HTTP version to be used in the request.
         */
        virtual void setRequestHTTPVersion(std::string version = "1.0");


        /**
         * Returns the HTTP version to be used in the request.
         */
        virtual std::string getRequestHTTPVersion();


        /**
         * Returns the HTTP status code of the response.
         */
        virtual uint16_t getResponseStatusCode();


        /**
         * Returns the status message of the response.
         */
        virtual std::string getResponseStatusMessage();
    };
}


#endif
