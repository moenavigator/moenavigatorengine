/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNE__NETWORK__NETWORKHANDLER_GNUTLS_H
#define MNE__NETWORK__NETWORKHANDLER_GNUTLS_H


#include <gnutls/gnutls.h>
#include <gnutls/x509.h>


#include "NetworkHandler_POSIX.h"
#include "../Exceptions/TLSException.h"


namespace MNE::Network
{
    class NetworkHandler_GnuTLS: public NetworkHandler_POSIX
    {
        protected:


        public:


        /**
         * The default constructor.
         */
        NetworkHandler_GnuTLS();


        /**
         * The default destructor.
         */
        ~NetworkHandler_GnuTLS();


        //NetworkHandler_POSIX overrides:


        /**
         * @see NetworkHandler_POSIX::request
         */
        virtual void request(
            std::string host,
            uint16_t port,
            std::weak_ptr<ResponseHandler> response_handler,
            std::vector<uint8_t> request_header = {},
            std::shared_ptr<DataSource> request_body = nullptr
            ) override;
    };
}


#endif
