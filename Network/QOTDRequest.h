/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#ifndef QOTDREQUEST_H
#define QOTDREQUEST_H


#include "Request.h"


namespace MNE::Network
{
    class QOTDRequest: public Request
    {
        protected:


        /**
         * The amount of bytes read.
         * QOTD responses should not be greater than 512 bytes.
         */
        size_t read_bytes = 0;


        public:


        /**
         * @see Request class constructor.
         */
        QOTDRequest(
            std::shared_ptr<NetworkHandler> network_handler,
            std::string host,
            uint16_t port,
            std::string resource,
            uint64_t id = 0
            );

        /**
         * @see Request::start
         */
        virtual void start() override;


        /**
         * @see ResponseHandler::processResponseData
         */
        virtual void processResponseData(
            std::shared_ptr<std::vector<uint8_t>> data,
            bool last_chunk = false
            ) override;
    };
}


#endif
