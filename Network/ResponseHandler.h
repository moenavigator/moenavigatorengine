/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2019  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



#ifndef RESPONSEHANDLER_H
#define RESPONSEHANDLER_H


#include <cstdint>
#include <memory>
#include <vector>


namespace MNE::Network
{
    /**
     * A ResponseHandler is a class whose instances process the response
     * data of specific request where each instance is responsible for one
     * request response.
     */
    class ResponseHandler
    {
        public:


        /**
         * ResponseHandler implementations get all the response data via
         * this method. Data delivery instances (such as NetworkHandler
         * instances) must identify the last chunk of data that can be read
         * and set the last_chunk parameter of this method correctly.
         *
         * @param std::vector<uint8_t> data The response data.
         *
         * @param bool last_chunk Whether the data that are passed in the
         *      data parameter contain the last chunk of data that has been read
         *      (true) or not (false). Defaults to false.
         */
        virtual void processResponseData(
            std::shared_ptr<std::vector<uint8_t>> data,
            bool last_chunk = false
            ) = 0;


        /**
         * Abstract default destructor.
         */
        virtual ~ResponseHandler() = default;
    };
}


#endif
