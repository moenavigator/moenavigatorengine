/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "QOTDRequest.h"


using namespace MNE::Network;


QOTDRequest::QOTDRequest(
    std::shared_ptr<NetworkHandler> network_handler,
    std::string host,
    uint16_t port,
    std::string resource,
    uint64_t id
    )
    : Request(network_handler, host, port, resource, id)
{
    //nothing else here
}


void QOTDRequest::start()
{
    //Reset the read_bytes counter and call the parent method:
    this->read_bytes = 0;
    Request::start();
}


void QOTDRequest::processResponseData(
    std::shared_ptr<std::vector<uint8_t>> data,
    bool last_chunk
    )
{
    if (this->read_bytes >= 512) {
        //Discard the response data and abort the request.
        if (!this->isAborted()) {
            this->abort();
            return;
        }
    }
    size_t current_chunk_size = data->size();
    if (current_chunk_size > 512) {
        //"Shrink" the response to 512 bytes and abort the request:
        data->resize(512);
        this->read_bytes = 512;
        this->abort();
    } else {
        //Count the bytes in the response and those that have already
        //been read:
        if ((current_chunk_size + this->read_bytes) > 512) {
            //Cut the current data chunk:
            auto cut_size = 512 - this->read_bytes;
            if (cut_size > 0) {
                data->resize(cut_size);
                this->abort();
                this->read_bytes += cut_size;
            }
        } else {
            this->read_bytes += current_chunk_size;
        }
    }
    Request::processResponseData(data, last_chunk);
}
