/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNE__NETWORK__DUMMYDATASOURCE_H
#define MNE__NETWORK__DUMMYDATASOURCE_H


#include "DataSource.h"


namespace MNE::Network
{
    /**
     * The DummyDataSource is the simplest form of data source possible.
     * The data to be sent can be set by just passing them to an instance
     * of this class.
     *
     * Note that this class assumes that all data is passed at once and it
     * isn't thread-safe because of this. Calling setData multiple times
     * while the data are also processed by a Request instance may result
     * in data loss or too few data being sent, depending on which thread
     * will first access the setData or getData method.
     */
    class DummyDataSource: public DataSource
    {
        protected:


        /**
         * The mime type to be returned via getMimeType.
         * Defaults to the mime type for general binary data.
         */
        std::string mime_type = "application/octet-stream";


        /**
         * The data that shall be sent.
         */
        std::vector<uint8_t> data;


        public:


        /**
         * Sets the mime type to be returned via the getMimeType method.
         *
         * @param const std::string& mime_type The mime type to be set.
         */
        virtual void setMimeType(const std::string& mime_type);


        /**
         * Sets the data to be sent by copying them.
         *
         * @param const std::vector<uint8_t>& data The data to be sent.
         */
        virtual void setData(const std::vector<uint8_t>& data);


        /**
         * Sets the data to be sent by moving them.
         *
         * @param std::vector<uint8_t>&& data The data to be sent.
         */
        virtual void setData(std::vector<uint8_t>&& data);


        /**
         * @see DataSource::getData
         */
        virtual std::vector<uint8_t> getData(const size_t chunk_size = 65536) override;


        /**
         * @see DataSource::getMimeType
         */
        virtual std::string getMimeType() override;
    };
}


#endif
