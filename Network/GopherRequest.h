/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GOPHERREQUEST_H
#define GOPHERREQUEST_H


#include "Request.h"


namespace MNE::Network
{
    class GopherRequest: public Request
    {
        public:


        //Request class overrides:


        GopherRequest(
            std::shared_ptr<NetworkHandler> network_handler,
            std::string host,
            uint16_t port,
            std::string resource,
            uint64_t id = 0
            );


        /**
         * @see Request::start
         */
        virtual void start() override;


        //Own methods:


        /**
         * Sets the search string for seachable resources
         * (Gopher resource type 7).
         */
        virtual void setSearchString(std::string& search_string);


        /**
         * @returns std::string The currently set query string.
         */
        virtual std::string getSearchString();


        /**
         * Determines the gopher item type from the resource path
         * that is requested.
         *
         * @returns char The gopher item type.
         */
        virtual char getItemType();


        protected:


        std::string search_string = "";
    };
}


#endif
