/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNE__NETWORK__NETWORKHANDLER_DUMMY_H
#define MNE__NETWORK__NETWORKHANDLER_DUMMY_H


#include "NetworkHandler.h"
#include "DummyServer.h"
#include "../Exceptions/Exception.h"


namespace MNE::Network
{
    class NetworkHandler_Dummy: public NetworkHandler
    {
        protected:


        /**
         * The dummy server to be used for handling requests.
         */
        std::shared_ptr<DummyServer> dummy_server = nullptr;


        /**
         * The size for data chunks.
         */
        size_t chunk_size = 65536;


        public:


        /**
         * @see NetworkHandler::request
         */
        virtual void request(
            std::string host,
            uint16_t port,
            std::weak_ptr<ResponseHandler> response_handler,
            std::vector<uint8_t> request_header = {},
            std::shared_ptr<DataSource> request_body = nullptr
            );


        /**
         * This method from the NetworkHandler interface is not implemented
         * since the network traffic that is generated from this network handler
         * is purely virtual.
         */
        virtual void setBufferSize(size_t buffer_size);


        /**
         * Sets a dummy server that checks if the request has send out
         * the correct data and that sends a pre-defined response for the
         * request.
         */
        virtual void setDummyServer(std::shared_ptr<DummyServer> dummy_server);
    };
}


#endif
