/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "MNENodeTreeTextRenderer.h"


/**
 * Renders the content of a node as text string
 * and calls itself recursively to render the children
 * of the node.
 */
std::string MNENodeTreeTextRenderer::nodeToText(
    std::shared_ptr<DocumentNode> node,
    unsigned short layer
    )
{
    std::string output = "";
    //add one space for each layer:
    for (unsigned short i = 0; i < layer; i++) {
        output += ' ';
    }
    //add the node name:
    output += node->name;

    //print attributes:
    output += '[';
    for (std::map<std::string, std::string>::iterator i = node->attributes.begin();
         i != node->attributes.end();
         ++i
        ) {
        if (i != node->attributes.begin()) {
            output += ", ";
        }
        output += i->first + "=" + i->second;
    }
    output += ']';

    //now render the children by calling this function recursively:

    for (auto child: node->children) {
        //DocumentNode::Children is a vector of DocumentNode*.
        //Until the DocumentNode class is refactored to use smart pointers
        //we must wrap the children into a new smart pointer:
        output += this->nodeToText(
            child,
            layer + this->indentation
            );
    }

    return output;
}


MNENodeTreeTextRenderer::MNENodeTreeTextRenderer()
{
    this->document = nullptr;
    this->rendered_text = "";
    this->indentation = 1;
}


void MNENodeTreeTextRenderer::setIndentationWidth(uint8_t indentation)
{
    this->indentation = indentation;
}


uint8_t MNENodeTreeTextRenderer::getIndentationWidth()
{
    return this->indentation;
}


//MNERenderer interface impementation:


uint32_t MNENodeTreeTextRenderer::getSupportedOutputModes()
{
    return MNERenderer::OutputMode::SIMPLETEXT;
}


void MNENodeTreeTextRenderer::setOutputMode(MNERenderer::OutputMode mode)
{
    if (mode != MNERenderer::OutputMode::SIMPLETEXT) {
        //throw new MNERendererException;
    }
}


/**
 * This renderer has no drawing area so setting this has no effect on
 * the rendered output.
 */
void MNENodeTreeTextRenderer::setDrawingAreaDimensions(
    uint32_t width,
    uint32_t height
    )
{
    //empty on purpose
}


void MNENodeTreeTextRenderer::setDocument(
    std::shared_ptr<DocumentNode> document
    )
{
    this->document = document;
}


std::shared_ptr<MNERenderedData> MNENodeTreeTextRenderer::getRenderedData()
{
    auto output = std::make_shared<MNERenderedData>();
    if (output != nullptr) {
        output->data = this->rendered_text;
    }
    return output;
}


void MNENodeTreeTextRenderer::render()
{
    if(this->document != nullptr)
    {
        //Ok, document is set: we can render the document to a text string:
        this->rendered_text = this->nodeToText(this->document, 0);
    }
}
