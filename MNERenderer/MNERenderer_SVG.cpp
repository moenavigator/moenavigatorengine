/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "MNERenderer_SVG.h"


#include "../CommonClasses/VisualAttributes.h"


std::string MNERenderer_SVG::renderNode(std::shared_ptr<DocumentNode> node)
{
    if (node == nullptr) {
        return "";
    }

    if (node->style.display_method == VisualAttributes::Display::NONE) {
        //Invisible nodes are not drawn.
        return "";
    }

    std::stringstream svg;

    if (node->type == DocumentNode::Type::NODE) {
        bool full_border = (
            (node->style.border_left_width > 0)
            &&
            (node->style.border_top_width > 0)
            &&
            (node->style.border_right_width > 0)
            &&
            (node->style.border_bottom_width > 0)
            );
        if ((node->style.border_style == VisualAttributes::BorderStyle::SOLID)
            && (full_border == true)) {
            //Draw a rectangle around the node:
            uint32_t rect_top_pos = 0;
            uint32_t rect_left_pos = 0;
            uint32_t rect_width = 0;
            uint32_t rect_height = 0;

            switch (node->style.display_method) {
                case VisualAttributes::Display::BLOCK: {
                    if (this->output_width > 0) {
                        rect_width = this->output_width
                            - node->style.margin_left
                            - node->style.margin_right;
                    } else {
                        rect_width = node->style.width
                            + node->style.padding_left
                            + node->style.padding_right;
                    }
                }
                case VisualAttributes::Display::INLINE: {
                    rect_width = node->style.width
                        + node->style.padding_left
                        + node->style.padding_right;
                }
            }

            //Draw a rectangle element:
            rect_height = node->style.height
                + node->style.border_top_width //TODO: draw four lines instead to allow different border sizes for each border!
                + node->style.padding_top
                + node->style.padding_bottom;
            //Note that SVG X and Y coordinates are interchanged compared
            //to X and Y coordinaties in MNE.
            svg << "<rect y=\""
                << rect_top_pos << "\" "
                << "x=\"" << rect_left_pos << "\" "
                << "width=\"" << rect_width << "\" "
                << "height=\"" << rect_height << "\" "
                << "stroke=\"" << node->style.border_color.toHexString() << "\" "
                << "fill=\"" << node->style.background_color.toHexString()
                << "\"/>";
        }
    } else if (node->type == DocumentNode::Type::TEXT) {
        //The "y" attribute needs the height, too, since the text is drawn
        //above the point marked by x and y.
        //Note that SVG X and Y coordinates are interchanged compared
        //to X and Y coordinaties in MNE.
        svg << "<text y=\"" << node->style.coord_x << "\" "
            << "x=\"" << node->style.coord_y + node->style.height << "\" "
            << "stroke=\"" << node->style.foreground_color.toHexString()
            << "\">" << node->name << "</text>";
    }

    for (std::shared_ptr<DocumentNode> child: node->children) {
        if (child != nullptr) {
            svg << this->renderNode(child);
        }
    }

    return svg.str();
}


MNERenderer_SVG::MNERenderer_SVG()
{
    this->document = nullptr;
    this->output = nullptr;
    this->output_width = 0;
    this->output_height = 0;
    this->current_x_pos = 0;
    this->current_y_pos = 0;
}


uint32_t MNERenderer_SVG::getSupportedOutputModes()
{
    return MNERenderer::OutputMode::GRAPHICS;
}


void MNERenderer_SVG::setOutputMode(MNERenderer::OutputMode mode)
{
    if (mode != MNERenderer::OutputMode::GRAPHICS) {
        //throw new MNERendererException;
    }
}


void MNERenderer_SVG::setDrawingAreaDimensions(uint32_t width, uint32_t height)
{
    if (width >= 0) {
        this->output_width = width;
    }
    if (height >= 0) {
        this->output_height = height;
    }
}


void MNERenderer_SVG::setDocument(std::shared_ptr<DocumentNode> document)
{
    if (document != nullptr) {
        this->document = document;
    }
}


std::shared_ptr<MNERenderedData> MNERenderer_SVG::getRenderedData()
{
    return this->output;
}


void MNERenderer_SVG::render()
{
    std::stringstream output;
    output << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
           << "<svg xmlns=\"http://www.w3.org/2000/svg\" "
           << "width=\"" << this->output_width << "\" "
           << "height=\"" << this->output_height << "\" "
           << "version=\"1.1\">\n";

    if (this->document != nullptr) {
        output << this->renderNode(this->document);
    }

    output << "</svg>";

    this->output = std::make_shared<MNERenderedData>();
    if (this->output != nullptr) {
        this->output->data = output.str();
    }
}
