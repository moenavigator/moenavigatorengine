/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNERENDERER_SVG_H
#define MNERENDERER_SVG_H


#include <memory>
#include <inttypes.h>


#include "MNERenderer.h"
#include "../CommonClasses/VisualAttributes.h"


class MNERenderer_SVG: public MNERenderer
{
  protected:

    std::shared_ptr<DocumentNode> document;
    std::shared_ptr<MNERenderedData> output;
    uint32_t output_width;
    uint32_t output_height;
    uint32_t current_x_pos;
    uint32_t current_y_pos;

    std::string renderNode(std::shared_ptr<DocumentNode> node);

  public:

    MNERenderer_SVG();

    virtual uint32_t getSupportedOutputModes();

    virtual void setOutputMode(MNERenderer::OutputMode mode);

    virtual void setDrawingAreaDimensions(uint32_t width, uint32_t height);

    virtual void setDocument(std::shared_ptr<DocumentNode> document);

    virtual std::shared_ptr<MNERenderedData> getRenderedData();

    virtual void render();
};


#endif
