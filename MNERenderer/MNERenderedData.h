/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNERENDEREDDATA_H
#define MNERENDEREDDATA_H

#include <string>

/**
 * This data class contains a list of "rendered data". "Rendered data"
 * means data whose graphical coordinates, size, colors and other
 * style attributes have been determined. They can be output
 * by an appropriate output module to generate GUI elements,
 * PDF files or SVG images or another graphical representation
 * of a document that can be parsed and processed by MoeNavigatorEngine.
 *
 */
class MNERenderedData
{
    public:
        std::string data; //STUB
};

#endif
