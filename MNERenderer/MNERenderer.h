/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNERENDERER_H
#define MNERENDERER_H

#include <memory>
#include <inttypes.h>

#include "../CommonClasses/DocumentNode.h"
#include "../CommonClasses/VisualAttributes.h"
#include "./MNERenderedData.h"


/**
 * The MNERenderer class outputs the content of documents which have been
 * parsed and processed by MoeNavigatorEngine in a binary intermediate format
 * that is described in (or by) the MNERenderedData class.
 * That format contains the pixel coordinates of the document content
 * for the specified dimensions of the drawing area.
 */
class MNERenderer
{
  public:

    enum OutputMode
    {
        DOM = 1, ///The renderer outputs the document as a DOM tree.
        SIMPLETEXT = 2, ///The renderer outputs the document as simple text (e.g. on a unix shell).
        RICHTEXT = 4, ///The renderer outputs the document as markup or rich text.
        GRAPHICS = 8 ///The renderer outputs the document as a rendered graphic.
    };

    /**
     * This method returns the supportes output modes of the renderer
     * as a bit field. Depending on the supported output modes
     * the engine may skip work which cannot be used by any
     * output mode the renderer supports.
     */
    virtual uint32_t getSupportedOutputModes() = 0;

    /**
     * Sets the preferred output mode in case a renderer supports
     * more than one output mode.
     */
    virtual void setOutputMode(MNERenderer::OutputMode mode) = 0;

    /**
     * Sets the drawing area dimensions the renderer may use.
     *
     * @param uint16_t width The width of the drawing area.
     *     Must be greater than -1. In case that the
     *     width is zero it is interpreted as unlimited.
     *
     * @param uint16_t height The height of the drawing area.
     *     Must be greater than -1. In case that the
     *     height is zero it is interpreted as unlimited.
     */
    virtual void setDrawingAreaDimensions(uint32_t width, uint32_t height) = 0;

    /**
     * Gives the renderer access to the document the engine
     * has processed.
     */
    virtual void setDocument(std::shared_ptr<DocumentNode> document) = 0;

    /**
     * Returns the rendered data.
     */
    virtual std::shared_ptr<MNERenderedData> getRenderedData() = 0;

    /**
     * Starts rendering the document.
     */
    virtual void render() = 0;


    /**
     * The default virtual destructor.
     */
    virtual ~MNERenderer() = default;
};

#endif
