/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNENODETREETEXTRENDERER_H
#define MNENODETREETEXTRENDERER_H


#include <memory>
#include <string>
#include <inttypes.h>

#include "../CommonClasses/DocumentNode.h"
#include "../CommonClasses/VisualAttributes.h"
#include "./MNERenderedData.h"
#include "./MNERenderer.h"


/**
 * This renderer outputs plain text containing the node tree of the processed document.
 */
class MNENodeTreeTextRenderer: public MNERenderer
{
  protected:
    virtual std::string nodeToText(
        std::shared_ptr<DocumentNode> node = nullptr,
        unsigned short layer = 0
        );

    std::shared_ptr<DocumentNode> document;
    std::string rendered_text;
    uint8_t indentation;

  public:

    //Special methods:

    MNENodeTreeTextRenderer();
    virtual void setIndentationWidth(uint8_t indentation = 1);
    uint8_t getIndentationWidth();

    //MNERenderer interface implementation:

    virtual uint32_t getSupportedOutputModes();
    virtual void setOutputMode(MNERenderer::OutputMode mode);
    virtual void setDrawingAreaDimensions(uint32_t width, uint32_t height);
    virtual void setDocument(std::shared_ptr<DocumentNode> document);
    virtual std::shared_ptr<MNERenderedData> getRenderedData();
    virtual void render();
};


#endif
