/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <string>
#include <vector>


#include "../Network/NetworkHandler_Dummy.h"
#include "../Network/DummyServer.h"
#include "../Network/GopherRequest.h"


int main()
{
    auto handler = std::make_shared<MNE::Network::NetworkHandler_Dummy>();
    auto server = std::make_shared<MNE::Network::DummyServer>();
    handler->setDummyServer(server);

    //Test resource string splitting:

    std::string resource = "/0/TestText";
    auto request = std::make_shared<MNE::Network::GopherRequest>(
        handler,
        "127.0.0.1",
        70,
        resource,
        0
        );

    if (request->getResource() != resource) {
        std::cerr << "The stored resource attribute does not match the resource specified in the constructor!" << std::endl;
        return 1;
    }

    resource = "/7/search\thello world!";

    request = std::make_shared<MNE::Network::GopherRequest>(
        handler,
        "127.0.0.1",
        70,
        resource,
        1
        );

    if (request->getResource() != "/7/search") {
        std::cerr << "The stored resource attribute does not match the resource part specified in the constructor!" << std::endl;
        return 1;
    }

    if (request->getSearchString() != "hello world!") {
        std::cerr << "The stored search string does not match the search string part specified in the constructor!" << std::endl;
        return 1;
    }

    //Test network request and response handling:
    std::string expected_request_data = "/7/search\thello world!\r\n";
    std::string response = "iNo search results!\r\n";
    server->setExpectedRequestData(
        std::vector<uint8_t>(
            expected_request_data.begin(),
            expected_request_data.end()
            )
        );
    server->setResponseData(
        std::vector<uint8_t>(
            response.begin(),
            response.end()
            )
        );
    try {
        request->start();
    } catch (MNE::Exception e) {
        std::cerr << e.toString() << std::endl;
        return 1;
    }

    auto received_response = request->getResponseString();
    if (received_response != response) {
        std::cerr << "The received response does not match the sent response!" << std::endl;
        return 1;
    }

    return 0;
}
