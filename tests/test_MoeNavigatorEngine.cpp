/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <memory>
#include <string>


#include "../MarkupParsers/HTMLParser.h"
#include "../MNERenderer/MNENodeTreeTextRenderer.h"
#include "../MoeNavigatorEngine.h"
#include "../MNERenderer/MNENodeTreeTextRenderer.h"
#include "../MNERenderer/MNERenderer_SVG.h"


/**
 * This is a test document.
 */
const std::string html = "<html>"
    "<head>"
        "<title>Hello World!</title>"
        "<meta name=\"test\" content=\"something\">"
        "<style type=\"text/css\">"
         "* { background-color: #000; color: #fff;}"
         ".intro { color: #00f; background-color: #444;}"
         "h1 {text-decoration: underline;}"
        "</style>"
    "</head>"
    "<body>"
    "<h1>Hello World!</h1>"
    "<p class=\"intro\">This is just an intro text\
    that spans across multiple lines.</p>"
    "<ul id=\"list1\">"
        "<li>Section1"
            "<ol>"
                "<!-- We add some HTML weirdness (li tags without closing tags) here. -->"
                "<li>Something else"
                "<li>Another item"
            "</ol>"
        "</li>"
    "</ul>"
    "</body>"
    "</html>";



int main()
{
    auto parser = std::make_unique<HTMLParser>();

    try {
        parser->parseData(std::vector<uint8_t>(html.begin(), html.end()));
    } catch (MNE::MarkupParserException e) {
        //The document should be parseable.
        std::cerr << "The HTML parser threw an exception!" << std::endl;
        return 1;
    }

    //Check if the engine processes the document correctly
    //and without memory leaks:

    auto document = parser->returnNodeTree();

    auto engine = std::make_shared<MoeNavigatorEngine>();

    //Check if adding and removing renderers work:

    auto renderer1 = std::make_shared<MNENodeTreeTextRenderer>();
    auto renderer2 = std::make_shared<MNERenderer_SVG>();

    engine->addRenderer(renderer1);
    engine->addRenderer(renderer2);
    engine->removeRenderer(renderer1);
    auto renderers = engine->getRenderers();
    if (renderers.size() != 1) {
        //The list of renderers has the wrong amount of renderers.
        return 1;
    }
    if (renderers.at(0) != renderer2) {
        //The wrong renderer is in the list.
        return 1;
    }

    //Re-attach the first renderer:
    engine->addRenderer(renderer1);
    engine->setDocument(document, "text/html");
    engine->setDocumentDimensions(640, 480);
    engine->drawPage();

    auto output = renderer1->getRenderedData();

    if (output == nullptr) {
        return 1;
    }
    std::cout << output->data << std::endl;

    //Detach the second renderer and re-render the document:
    engine->removeRenderer(renderer2);
    engine->setDocument(document);
    engine->drawPage();
    auto output2 = renderer1->getRenderedData();
    if (output2 == nullptr) {
        return 1;
    }
    if (output2->data != output->data) {
        //The data should be identical.
        return 1;
    }

    return 0;
}
