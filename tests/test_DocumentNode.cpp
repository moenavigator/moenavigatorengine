/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <memory>

#include "../CommonClasses/DocumentNode.h"


int main()
{
    auto node1 = std::make_shared<DocumentNode>("node1", DocumentNode::Type::NODE);

    //Make sure the basic data are stored correctly:
    if ((node1->name != "node1") || (node1->type != DocumentNode::Type::NODE)) {
        std::cerr << "DocumentNode: name or type not stored properly!" << std::endl;
        return 1;
    }

    //Add a child node:

    auto node2 = std::make_shared<DocumentNode>("node2", DocumentNode::Type::NODE);
    node1->appendChild(node2);

    if (!node1->hasChildNodes()) {
        std::cerr << "DocumentNode::hasChildNodes returns false after adding a child node!" << std::endl;
        return 1;
    }

    //Check the hierarchy:

    if (node2->getParentNode() != node1) {
        std::cerr << "Child node has the wrong parent after being appended via appendChild method!" << std::endl;
        return 1;
    }

    if (node1->getFirstChild() != node2) {
        std::cerr << "DocumentNode::getFirstChild returns the wrong child!" << std::endl;
        return 1;
    }

    if (node1->hasAttribute("nonexistant")) {
        std::cerr << "A node has an attribute that doesn\'t exist!" << std::endl;
        return 1;
    }

    //Set an attribute:

    node1->setAttribute("test", "success");
    if (!node1->hasAttribute("test")) {
        std::cerr << "Adding an attribute to a node did not work!" << std::endl;
        return 1;
    }

    //Update the attribute:
    node1->setAttribute("test", "still success?");
    auto value = node1->getAttribute("test");
    if (value != "still success?") {
        std::cerr << "Updating a node\'s attribute did not work!" << std::endl;
        return 1;
    }

    //Check if node1 is different from node2:
    if (node1->isSameNode(node2)) {
        std::cerr << "Different nodes are reported to be identical!" << std::endl;
        return 1;
    }

    //Add some text nodes and other nodes to node2 and then call normalize
    //to see if adjacent text nodes have been merged into one text node:
    auto new_text = std::make_shared<DocumentNode>(
        "Hello World! ",
        DocumentNode::Type::TEXT
        );
    node2->appendChild(new_text);
    new_text = std::make_shared<DocumentNode>(
        "How are you? ",
        DocumentNode::Type::TEXT
        );
    node2->appendChild(new_text);
    new_text = std::make_shared<DocumentNode>(
        "Have you eaten?",
        DocumentNode::Type::TEXT
        );

    auto node3 = std::make_shared<DocumentNode>(
        "test3",
        DocumentNode::Type::NODE
        );
    node2->appendChild(node3);

    new_text = std::make_shared<DocumentNode>(
        "Some other text.",
        DocumentNode::Type::TEXT
        );
    node2->appendChild(new_text);

    //reset the shared_ptr to node2 and node3. After that, call the insertBefore
    //method and check the hierachy of the new child node of node1:
    node2.reset();
    node3.reset();

    auto node4 = std::make_shared<DocumentNode>(
        "test4",
        DocumentNode::Type::NODE
        );
    node1->insertBefore(node4, node1->getFirstChild());
    auto first_child = node1->getFirstChild();
        if (first_child != node4) {
        std::cerr << "DocumentNode::insertBefore did not insert a new node in the right position!" << std::endl;
        return 1;
    }
    auto first_child_next = first_child->getNextNode();
    if (first_child_next == nullptr) {
        std::cerr << "DocumentNode::insertBefore did not insert a new node in the right position!" << std::endl;
        return 1;
    }
    if (first_child_next->name != "node2") {
        std::cerr << "DocumentNode::insertBefore did not insert a new node in the right position!" << std::endl;
        return 1;
    }
    node2 = first_child_next;
    node3 = node2->getFirstChild();
    if (node3 == nullptr) {
        std::cerr << "Memory management error!" << std::endl;
        return 1;
    }
    if (node3->name != "Hello World! ") {
        std::cerr << "Hierarchy error!" << std::endl;
        return 1;
    }

    //Test the removeChild method with node2:
    node2 = node1->removeChild(node2);
    auto node2_previous = node2->getPreviousNode();
    if ((first_child->getNextNode() == node2) || (first_child == node2->getPreviousNode())) {
        std::cerr << "DocumentNode::removeChild did not clean up the pointers to siblings!" << std::endl;
        return 1;
    }
    auto node2_parent = node2->getParentNode();
    if (node2_parent != nullptr) {
        std::cerr << "DocumentNode::removeChild did not reset the parent_node pointer!" << std::endl;
        return 1;
    }
    if (node1->removeChild(nullptr) != nullptr) {
        std::cerr << "DocumentNode::removeChild did not return a nullptr in case a nonexistant node shall be removed!" << std::endl;
        return 1;
    }


    //Test the normalize/normalise method with node2, that should
    //still exist at this point (as standalone node):

    node2->normalize();
    auto first_node2_child = node2->getFirstChild();
    if (first_node2_child == nullptr) {
        std::cerr << "DocumentNode::normalize did remove all previous child text nodes!" << std::endl;
        return 1;
    }
    if (first_node2_child->type != DocumentNode::Type::TEXT) {
        std::cerr << "DocumentNode::normalize did move the text nodes that were the first child nodes before!" << std::endl;
        return 1;
    }
    if (first_node2_child->name != "Hello World! How are you? Have you eaten?") {
        std::cerr << "DocumentNode::normalize did not normalise the text nodes correctly!" << std::endl;
        return 1;
    }


    //Test the replaceChild method:

    auto node5 = std::make_shared<DocumentNode>(
        "test5",
        DocumentNode::Type::NODE
        );
    node1->replaceChild(node4, node5);
    //The first child should now be node5:
    first_child = node1->getFirstChild();
    if (first_child != node5) {
        std::cerr << "DocumentNode::replaceChild did not replace an existing node with a new one!" << std::endl;
        return 1;
    }

    if (node1->replaceChild(nullptr, nullptr) != nullptr) {
        std::cerr << "DocumentNode::replaceChild did not return a nullptr in case nullptrs are passed as parameters!" << std::endl;
        return 1;
    }

    return 0;
}
