/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <memory>
#include <vector>

#include "../Exceptions/Exception.h"
#include "../Network/HTTPFormDataSource.h"


int main()
{
    auto form_data = std::make_unique<MNE::Network::HTTPFormDataSource>();

    std::vector<uint8_t> binary = {17, 76, 19, 05, 19, 17, 255, 0, 192, 224};
    form_data->addItem("test", "Hello World!");
    form_data->addItem("binary", binary);
    form_data->addItem("test2", "Something");
    form_data->addItem("test3", "More");
    //It should be possible to have several items with the same name
    //in a form:
    form_data->addItem("test2", "A bit more");
    form_data->addItem("test3", "stuff");

    form_data->removeItems("test2");
    if (form_data->hasItem("test2")) {
        //test2 items should have been deleted.
        return 1;
    }
    if (!form_data->hasItem("test")) {
        //test should be there.
        return 1;
    }
    if (!form_data->hasItem("test3")) {
        //test3 should be in the form.
        return 1;
    }
    try {
        auto bin_val = form_data->getItem("binary");
        if (bin_val != binary) {
            //The data that come out of the form doesn't match the data
            //that went into it.
            return 1;
        }
    } catch (MNE::Exception& e) {
        return 1;
    }

    //Test URL encoding (percent encoding):

    form_data->enableURLEncode();

    auto data = form_data->getData();
    if (data.size() < 1) {
        return 1;
    }
    std::string url_encoded_data(data.begin(), data.end());
    std::string expected_data = "test=Hello%20World%21&binary=%11L%13%05%13%11%ff%00%c0%e0&test3=More&test3=stuff";

    if (url_encoded_data != expected_data) {
        return 1;
    }

    //Test multipart form encoding:

    form_data->reset();
    form_data->disableURLEncode();

    form_data->setBoundary("will_not_be_used_anywhere_else");


    data = form_data->getData();

    //Parse the multipart form data and compare it to an expected result.

    std::string data_str(data.begin(), data.end());


    std::string expected_multipart_form_data = "--will_not_be_used_anywhere_else\r\n"
        "Content-Disposition: form-data; name=\"test\"\r\n"
        "\r\n"
        "Hello World!\r\n"
        "--will_not_be_used_anywhere_else\r\n"
        "Content-Disposition: form-data; name=\"binary\"\r\n"
        "Content-Type: application/octet-stream\r\n"
        "Content-Transfer-Encoding: base64\r\n"
        "\r\n"
        "EUwTBRMR/wDA4A==\r\n"
        "--will_not_be_used_anywhere_else\r\n"
        "Content-Disposition: form-data; name=\"test3\"\r\n"
        "\r\n"
        "More\r\n"
        "--will_not_be_used_anywhere_else\r\n"
        "Content-Disposition: form-data; name=\"test3\"\r\n"
        "\r\n"
        "stuff\r\n"
        "--will_not_be_used_anywhere_else\r\n";
    if (data_str != expected_multipart_form_data) {
        return 1;
    }

    return 0;
}
