/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <memory>
#include <string>
#include <vector>


#include "../StylesheetParsers/CSSSelectorParser.h"


void exitOnSelectorError(std::shared_ptr<CSSSelector> expected_selector, std::shared_ptr<CSSSelector> result)
{
    if (expected_selector == nullptr) {
        //Nothing to do.
        return;
    }
    if (result == nullptr) {
        std::cerr << "CSSSelectorParser failed to parse the selector \""
                  << expected_selector->toString() << "\"! The result is a null pointer!" << std::endl;
        exit(1);
    } else {
        //Check if the parsed selector has the same content
        //as the expected selector:
        if (*expected_selector == *result) {
            return;
        } else {
            std::cerr << "CSSSelectorParser failed to parse the selector \""
                      << expected_selector->toString() << "\"! The parsed result is: \""
                      << result->toString() << "\"!" << std::endl;
            exit(1);
        }
    }
}


int main()
{
    auto parser = std::make_shared<CSSSelectorParser>();

    //Start with basic selectors:

    auto expected_selector = std::make_shared<CSSSelector>();
    expected_selector->element = "a";
    parser->parseString("a");
    auto result = parser->getSelector();
    exitOnSelectorError(expected_selector, result);
    expected_selector->element = "";
    parser->reset();

    expected_selector->id = "main";
    parser->parseString("#main");
    result = parser->getSelector();
    exitOnSelectorError(expected_selector, result);
    expected_selector->id = "";
    parser->reset();

    //Now a more advanced selector:

    expected_selector->element = "a";
    expected_selector->classes.push_back("blue");
    expected_selector->attributes.insert(
        std::pair<std::string, std::string>("target", "_blank")
        );
    parser->parseString("a.blue[target=\"_blank\"]");
    result = parser->getSelector();
    exitOnSelectorError(expected_selector, result);
    expected_selector->element = "";
    expected_selector->classes = {};
    expected_selector->attributes = {};
    parser->reset();


    //And even more advanced:

    expected_selector->element = "section";
    expected_selector->classes.push_back("content");
    auto child = std::make_shared<CSSSelector>();
    child->element = "div";
    child->classes.push_back("warning");
    expected_selector->child = child;
    expected_selector->direct_child = true;
    parser->parseString("section.content > div.warning");
    result = parser->getSelector();
    exitOnSelectorError(expected_selector, result);
    parser->reset();

    //Now we change the seletor a little bit:
    expected_selector->direct_child = false;
    parser->parseString("section.content div.warning");
    result = parser->getSelector();
    exitOnSelectorError(expected_selector, result);
    parser->reset();

    //That's enough for now.
    return 0;
}
