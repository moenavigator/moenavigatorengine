/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <string>
#include <vector>


#include "../Network/NetworkHandler_Dummy.h"
#include "../Network/DummyServer.h"
#include "../Network/HTTPRequest.h"
#include "../Network/HTTPFormDataSource.h"


int main()
{
    auto handler = std::make_shared<MNE::Network::NetworkHandler_Dummy>();
    auto server = std::make_shared<MNE::Network::DummyServer>();
    handler->setDummyServer(server);

    //Test HTTP GET:

    std::string resource = "/Test.txt";
    auto request = std::make_shared<MNE::Network::HTTPRequest>(
        handler,
        "127.0.0.1",
        80,
        resource,
        0
        );

    if (request->getResource() != resource) {
        std::cerr << "The stored resource does not match the resource specified in the constructor!" << std::endl;
        return 1;
    }

    request->addRequestHeader("X-Extra-Header", "1337");

    std::string expected_request_data = "GET /Test.txt HTTP/1.0\r\n"
        "Host: 127.0.0.1\r\n"
        "X-Extra-Header: 1337\r\n\r\n";

    std::string response = "HTTP/1.0 200 OK\r\n"
        "Content-Type: text/plain;encoding=UTF-8\r\n"
        "X-Extra-Header: 1337\r\n\r\n"
        "Hello World!";

    server->setExpectedRequestData(
        std::vector<uint8_t>(
            expected_request_data.begin(),
            expected_request_data.end()
            )
        );
    server->setResponseData(
        std::vector<uint8_t>(
            response.begin(),
            response.end()
            )
        );

    try {
        request->start();
    } catch (MNE::Exception e) {
        std::cerr << e.toString() << std::endl;
        return 1;
    }

    auto response_header = request->getResponseHeader();
    auto content_type = response_header.find("Content-Type");
    if (content_type == response_header.end()) {
        std::cerr << "The Content-Type field is not present in the received response!" << std::endl;
        return 1;
    }
    if (content_type->second != "text/plain;encoding=UTF-8") {
        std::cerr << "The Content-Type value does not match the value sent from the server!" << std::endl;
        return 1;
    }
    auto extra_header = response_header.find("X-Extra-Header");
    if (extra_header == response_header.end()) {
        std::cerr << "The X-Extra-Header field is not present in the received response!" << std::endl;
        return 1;
    }
    if (extra_header->second != "1337") {
        std::cerr << "The X-Extra-Header value does not match the value sent from the server!" << std::endl;
        return 1;
    }

    auto request_response = request->getResponseString();
    if (request_response != "Hello World!") {
        std::cerr << "The response body does not match the body sent from the server!" << std::endl;
        return 1;
    }

    //Test HTTP POST:
    server->reset();

    request = std::make_shared<MNE::Network::HTTPRequest>(
        handler,
        "127.0.0.1",
        80,
        "/submit",
        0
        );
    request->setMethod("POST");

    auto body = std::make_shared<MNE::Network::HTTPFormDataSource>();
    body->addItem("Hello", "World");
    body->addItem("Post", "works too!");
    body->enableURLEncode();

    request->setDataSource(body);

    expected_request_data = "POST /submit HTTP/1.0\r\n"
        "Host: 127.0.0.1\r\n\r\n"
        "Hello=World&Post=works%20too%21";

    response = "HTTP/1.0 200 OK\r\n"
        "Content-Type: text/plain;encoding=UTF-8\r\n\r\n"
        "Submitted";

    server->setExpectedRequestData(
        std::vector<uint8_t>(
            expected_request_data.begin(),
            expected_request_data.end()
            )
        );
    server->setResponseData(
        std::vector<uint8_t>(
            response.begin(),
            response.end()
            )
        );

    try {
        request->start();
    } catch (MNE::Exception e) {
        std::cerr << e.toString() << std::endl;
        return 1;
    }

    request_response = request->getResponseString();
    if (request_response != "Submitted") {
        return 1;
    }

    return 0;
}
