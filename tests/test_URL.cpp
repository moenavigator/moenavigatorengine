/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "../CommonClasses/URL.h"


using namespace MNE;


int main()
{
    //Test a common URL:

    URL url("gopher://example.example/1/main");

    if (url.protocol != "gopher") {
        return 1;
    }
    if (!url.user_info.empty()) {
        return 1;
    }
    if (url.domain != "example.example") {
        return 1;
    }
    if (url.port != 70) {
        //The port is set automatically to the default of the protocol,
        //if it isn't explicitly specified.
        return 1;
    }
    if (url.path != "/1/main") {
        return 1;
    }

    //Test a "fully featured" URL:
    url = URL("https://user:password@example.org:3700/path/to/resource.txt");

    if (url.protocol != "https") {
        return 1;
    }
    if (url.user_info != "user:password") {
        return 1;
    }
    if (url.domain != "example.org") {
        return 1;
    }
    if (url.port != 3700) {
        return 1;
    }
    if (url.path != "/path/to/resource.txt") {
        return 1;
    }

    //Test percent decoding and encoding:

    //Test percent decode:
    std::string encoded_str = "Hello+World%21";
    auto decoded_result = URL::percentDecode(encoded_str);
    std::string decoded_result_str(decoded_result.begin(), decoded_result.end());
    if (decoded_result_str != "Hello World!") {
        return 1;
    }

    //Test percent encode:
    auto encoded_result = URL::percentEncode(decoded_result);
    if (encoded_result != "Hello%20World%21") {
        return 1;
    }

    //Test an URL that has an "invalid argument" as port number.
    try {
        url = URL("https://example.org:NaN/test.txt");
        //The constructor should have thrown an exception!
        return 1;
    } catch (std::invalid_argument& e) {
        //This is an expected exception.
        return 0;
    }

    //Test an URL with a port number that is out of range for a port.
    try {
        url = URL("https://example.org:131072/cool-port.txt");
        //The constructor should have thrown an exception!
        return 1;
    } catch (MNE::Exception& e) {
        //Nothing. Everything is fine.
    }

    //Test an URL with a port number that is out of range for a string.
    try {
        url = URL("http://example.org:1917819250210/numbers.txt");
        //stoul should have thrown an exception.
        return 1;
    } catch (std::out_of_range& e) {
        //Nothing. That exception is expected.
    }

    return 0;
}
