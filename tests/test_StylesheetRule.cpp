/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <memory>
#include <vector>


#include "../CommonClasses/StylesheetRule.h"


int main()
{
    std::vector<std::string> selectors = {"*", "div"};
    auto rule1 = std::make_shared<StylesheetRule>(
        selectors
        );
    if (rule1->getSelectors() != selectors) {
        return 1;
    }


    auto rule2 = rule1->copy();
    if (rule2 == nullptr) {
        return 1;
    }
    if (rule2->getSelectors() != selectors) {
        return 1;
    }

    auto css = rule2->toCSS();
    if (css.empty()) {
        return 1;
    }

    return 0;
}
