/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <string>


#include "../CommonClasses/Colour.h"


int main()
{
    unsigned long int colour_value = 0xffeedd44;
    Colour c1(colour_value);

    if ((c1.r != 0xff) || (c1.g != 0xee) || (c1.b != 0xdd) || (c1.alpha != 0x44)) {
        std::cerr << "Error: assignInt32 doesn't assign values correctly!" << std::endl;
        return 1;
    }

    auto hex_string = c1.toHexString();

    if (hex_string != "#ffeedd") {
        return 1;
    }

    Colour c2(0x08070605);
    auto hex_string2 = c2.toHexString();
    if (hex_string2 != "#080706") {
        return 1;
    }

    return 0;
}
