/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <string>
#include <vector>


#include "../Network/NetworkHandler_Dummy.h"
#include "../Network/DummyServer.h"
#include "../Network/Request.h"
#include "../Network/DummyDataSource.h"


int main()
{
    auto handler = std::make_shared<MNE::Network::NetworkHandler_Dummy>();
    auto server = std::make_shared<MNE::Network::DummyServer>();
    handler->setDummyServer(server);

    //Read/write only one byte at a time. Inefficient of course, but we
    //want to test the appending of chunks, too.
    handler->setBufferSize(1);

    auto request = std::make_shared<MNE::Network::Request>(
        handler,
        "127.0.0.1",
        4000,
        "test"
        );
    std::string request_data = "Hello, world!";
    auto request_data_source = std::make_shared<MNE::Network::DummyDataSource>();
    request_data_source->setData(std::vector<uint8_t>(request_data.begin(), request_data.end()));
    request->setDataSource(request_data_source);

    std::string response = "Hello, request!";
    std::vector<uint8_t> response_data(
        response.begin(),
        response.end()
        );

    server->setExpectedRequestData(
        std::vector<uint8_t>(
            request_data.begin(),
            request_data.end()
            )
        );
    server->setResponseData(response_data);

    try {
        request->start();
    } catch (MNE::Exception e) {
        std::cerr << e.toString() << std::endl;
        return 1;
    }

    auto received_response = request->getResponseData();
    if (received_response == nullptr) {
        std::cerr << "The response has not been received!" << std::endl;
        return 1;
    }

    if (*received_response != response_data) {
        std::cerr << "The response data are not identical to those sent from the server!" << std::endl;
        return 1;
    }

    return 0;
}
