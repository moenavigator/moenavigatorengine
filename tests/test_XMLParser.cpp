/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <memory>
#include <string>


#include "../MarkupParsers/XMLParser.h"


/**
 * This bad document has syntax errors and nodes without closing tags.
 */
const std::string bad_xml_doc1 = "<test with=\"bad things\">\
<hello who = \"World\"></hello>\
<answer>now\
</test2>";


/**
 * The XML parser should throw an exception on this document, since it has
 * two root nodes (test and test2).
 */
const std::string bad_xml_doc2 = "<?xml version=\"1.0\" encoding=\"utf-8\">\
<test document=\"still good\">\
    <text>Just some random text in a child node.</text>\
</test>\
<test2 document=\"invalid\" reason=\"two top nodes\">Hehehe</test2>";


/**
 * The XML parser should be able to parse this simple document, including the
 * unicode part.
 */
const std::string simple_xml_doc = "<?xml version=\"1.0\" encoding=\"utf-8\">\
<test for=\"XMLParser\" using=\"a-test-string\">\
    <text>Some text</text>\
    <quote by=\"Someone Inthewild\">Hello World!</quote>\
    <unicode name=\"Geschichte vom Bär\">\
        <bär>Honig ist süß!</bär>\
        <ferkel>Unterdrückung auch?</ferkel>\
        <bär>Nur wenn meine Partei und mein Militär das macht!</bär>\
    </unicode>\
</test>";


/**
 * This is a more complex XML document with two namespaces.
 */
const std::string complex_xml_doc = "<?xml version=\"1.0\" encoding=\"utf-8\">\
<t:test xmlns:t=\"https://example.org/ns/t\">\
    <t:message type=\"chat\">\
        <html:div xmlns:html=\"http://www.w3.org/1999/xhtml\">\
            <html:h1>Test message</html:h1>\
            <html:p>This is a test message.</html:p>\
        </html:div>\
    </t:message>\
</t:test>";




int main()
{
    auto parser = std::make_unique<XMLParser>();

    try {
        parser->parseData(std::vector<uint8_t>(bad_xml_doc1.begin(), bad_xml_doc1.end()));
        //If the following line of code is reached, the parser has parsed
        //the document to the end.
        return 1;
    } catch (MNE::MarkupParserException e) {
        //Nothing here. The parser has done the right thing.
    }

    parser->reset();

    try {
        parser->parseData(std::vector<uint8_t>(bad_xml_doc2.begin(), bad_xml_doc2.end()));
        //The same as above.
        return 1;
    } catch (MNE::MarkupParserException e) {
        //Also the same as above.
    }

    parser->reset();

    try {
        parser->parseData(std::vector<uint8_t>(simple_xml_doc.begin(), simple_xml_doc.end()));
    } catch (MNE::MarkupParserException e) {
        return 1;
    }
}
