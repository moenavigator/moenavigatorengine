/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <memory>
#include <vector>


#include "../Network/DummyDataSource.h"


int main()
{
    auto ds = std::make_unique<MNE::Network::DummyDataSource>();

    std::vector<uint8_t> data = {13, 37, 20, 21, 19, 17, 19, 5, 17, 91, 17, 76, 255, 255, 255, 0};

    //Copy data:
    ds->setData(data);

    //Read result and compare:
    auto result1 = ds->getData();

    if (result1 != data) {
        //Data went missing!
        return 1;
    }

    //Move data:
    ds->setData(std::move(data));
    //Confirm move:
    if (data.size() > 0) {
        //Data were not moved!
        return 1;
    }
    auto result2 = ds->getData();
    if (result2 != result1) {
        //Data went missing!
        return 1;
    }

    //Read data in chunks:
    ds->setData(result2);

    auto result3 = ds->getData(10);
    if (result3.size() != 10) {
        //Not enough bytes have been returned.
        return 1;
    }
    auto result3_2 = ds->getData(10);
    result3.insert(result3.end(), result3_2.begin(), result3_2.end());
    if (result3 != result2) {
        //Data went missing.
        return 1;
    }

    return 0;
}
