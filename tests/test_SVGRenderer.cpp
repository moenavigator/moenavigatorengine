/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <memory>
#include <string>


#include "../Exceptions/MarkupParserException.h"
#include "../MarkupParsers/XMLParser.h"
#include "../MNERenderer/MNERenderer_SVG.h"


int main()
{
    auto document = std::make_shared<DocumentNode>("test", DocumentNode::Type::NODE);
    if (document == nullptr) {
        //Initialisation error!
        return 1;
    }
    document->style.coord_x = 0;
    document->style.coord_y = 0;
    document->style.display_method = VisualAttributes::Display::BLOCK;
    document->style.background_color = Colour(0x00ff00ff);
    document->style.foreground_color = Colour(0xffffffff);
    document->style.font_size = 14;
    document->style.line_height = 18;
    document->style.border_left_width = 1;
    document->style.border_top_width = 2;

    document->style.margin_left = 4;
    document->style.margin_top = 4;
    document->style.padding_left = 10;
    document->style.padding_top = 10;

    auto text = std::make_shared<DocumentNode>("Hello World!", DocumentNode::Type::TEXT);
    if (text == nullptr) {
        //Initialisation error!
        return 1;
    }
    document->appendChild(text);

    auto renderer = std::make_unique<MNERenderer_SVG>();
    if (renderer == nullptr) {
        //Initialisation error!
        return 1;
    }

    renderer->setDrawingAreaDimensions(320, 240);
    renderer->setDocument(document);
    renderer->render();
    auto result = renderer->getRenderedData();
    if (result == nullptr) {
        //The renderer did not produce any output!
        return 1;
    }

    auto xml_parser = std::make_unique<XMLParser>();
    try {
        xml_parser->parseData(std::vector<uint8_t>(result->data.begin(), result->data.end()));
    } catch (MNE::MarkupParserException e) {
        //Error while parsing the XML document.
        return 1;
    }

    auto result_doc = xml_parser->returnNodeTree();
    if (result_doc == nullptr) {
        //No SVG document could be parsed!
        return 1;
    }

    if (result_doc->name != "svg") {
        //The SVG document has the wrong root node!
        return 1;
    }

    if (!result_doc->hasAttribute("xmlns")) {
        //The svg node doesn't have a namespace declaration!
        return 1;
    }
    if (result_doc->attributes["xmlns"] != "http://www.w3.org/2000/svg") {
        //The namespace of the svg node is wrong!
        return 1;
    }
    if (result_doc->children.size() != 1) {
        //The svg node has the wrong amount of children!
        return 1;
    }

    auto first_child = result_doc->getFirstChild();
    if (first_child == nullptr) {
        //There is something wrong since we checked that the amount of children
        //is correct.
        return 1;
    }
    if (first_child->children.size() != 1) {
        //The text is missing.
        return 1;
    }
    auto first_text = first_child->getFirstChild();
    if (first_text == nullptr) {
        //Something is wrong since children->size reported one child.
        return 1;
    }
    if (first_text->name != "Hello World!") {
        //This is not the text we had in the DOM document!
        return 1;
    }

    //Nothing more to test for the moment.

    return 0;
}
