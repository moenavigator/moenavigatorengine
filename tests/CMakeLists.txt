set(
  all_tests

  #The engine:
  MoeNavigatorEngine

  #Common classes:
  DocumentNode Colour URL StylesheetRule

  #Network classes:
  Request GopherRequest HTTPRequest DummyDataSource HTTPFormDataSource

  #Markup parsers:
  HTMLParser GopherMenuParser XMLParser

  #Stylesheet parsers:
  CSSParser CSSSelectorParser

  #Renderers:
  SVGRenderer
  )

foreach(test IN LISTS all_tests)
  add_executable(test_${test} test_${test}.cpp)
  target_link_libraries(test_${test} moenavigatorengine)
  add_test(NAME test_${test} COMMAND test_${test})
endforeach()
