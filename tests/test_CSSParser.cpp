/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <memory>
#include <string>
#include <vector>


#include "../CommonClasses/VisualAttributes.h"
#include "../CommonClasses/Colour.h"
#include "../StylesheetParsers/CSSParser.h"




const std::string css_code = "\
* {\
    background-color: #ffffff;\
    color: #00cc00;\
}\
\
div {\
    font-size: 16;\
    margin-top: 10;\
    margin-bottom: 10;\
    something-bad: \"yes\";\
}\
\
@media print {\
    a {\
        color: blue;\
    }\
\
    div.warning {\
        display: none;\
    }\
}\
";




int main()
{
    std::cerr << "CSS code to parse: \n" << css_code << std::endl;

    const std::vector<uint8_t> css_data(css_code.begin(), css_code.end());

    auto parser = std::make_unique<CSSParser>();

    try {
        parser->parseData(css_data);
    } catch (MNE::ParsingException e) {
        std::cerr << "ERROR: Parser throwed an exception: " << e.toString() << std::endl;
    }

    auto stylesheet_collection = parser->returnStylesheetRules();
    if (stylesheet_collection == nullptr) {
        return 1;
    }

    if (stylesheet_collection->groups.size() != 2) {
        //There should only be two groups: media all and media print.
        std::cerr << "ERROR: The amount of stylesheet groups should be 2, not "
                  << stylesheet_collection->groups.size() << "!" << std::endl;
        return 1;
    }

    //The first of the two media groups should be for media "all".
    auto first_group = stylesheet_collection->groups[0];
    if (first_group == nullptr) {
        return 1;
    }
    if (first_group->name != "all") {
        //Name mismatch.
        std::cerr << "ERROR: First stylesheet rule group name should be \"all\", not \""
                  << first_group->name << "\"!" << std::endl;
        return 1;
    }

    //The second group should be the media "print" group:
    auto second_group = stylesheet_collection->groups[1];
    if (second_group == nullptr) {
        return 1;
    }
    if (second_group->name != "print") {
        //Name mismatch.
        std::cerr << "ERROR: Second stylesheet rule group name should be \"print\", not \""
                  << second_group->name << "\"!" << std::endl;
        return 1;
    }

    //Now check the rules of the first group:

    auto rules = first_group->rules;

    if (rules.size() != 2) {
        std::cerr << "ERROR: The first stylesheet group does not contain the expected amount of rules!" << std::endl;
        return 1;
    }

    //Check each rule, their selector and their attributes:

    auto rule = rules[0];
    if (rule == nullptr) {
        std::cerr << "ERROR: The rule is a nullptr!" << std::endl;
        return 1;
    }
    auto selectors = rule->getSelectors();
    if (selectors.size() != 1) {
        std::cerr << "ERROR: The rule has the wrong amount of selectors!" << std::endl;
        return 1;
    }
    if (selectors[0] != "*") {
        std::cerr << "ERROR: The rule has the wrong selector!" << std::endl;
        return 1;
    }

    if (rule->attributes.background_color != Colour(0xffffffff)) {
        std::cerr << "ERROR: Attribute has the wrong value!" << std::endl;
        return 1;
    }
    if (rule->attributes.background_color != Colour(0x00cc00ff)) {
        std::cerr << "ERROR: Attribute has the wrong value!" << std::endl;
        return 1;
    }

    //Ok, check the next rule:

    rule = rules[1];
    if (rule == nullptr) {
        std::cerr << "ERROR: The rule is a nullptr!" << std::endl;
        return 1;
    }
    selectors = rule->getSelectors();
    if (selectors.size() != 1) {
        std::cerr << "ERROR: The rule has the wrong amount of selectors!" << std::endl;
        return 1;
    }
    if (selectors[0] != "div") {
        std::cerr << "ERROR: The rule has the wrong selector!" << std::endl;
        return 1;
    }

    if (rule->attributes.font_size != 10) {
        std::cerr << "ERROR: Attribute has the wrong value!" << std::endl;
        return 1;
    }
    if (rule->attributes.margin_top != 10) {
        std::cerr << "ERROR: Attribute has the wrong value!" << std::endl;
        return 1;
    }
    if (rule->attributes.margin_bottom != 10) {
        std::cerr << "ERROR: Attribute has the wrong value!" << std::endl;
        return 1;
    }

    //Check the rules of the second group:
    rules = second_group->rules;

    if (rules.size() != 2) {
        std::cerr << "ERROR: The first stylesheet group does not contain the expected amount of rules!" << std::endl;
        return 1;
    }

    rule = rules[0];
    if (rule == nullptr) {
        std::cerr << "ERROR: The rule is a nullptr!" << std::endl;
        return 1;
    }
    selectors = rule->getSelectors();
    if (selectors.size() != 1) {
        std::cerr << "ERROR: The rule has the wrong amount of selectors!" << std::endl;
        return 1;
    }
    if (selectors[0] != "a") {
        std::cerr << "ERROR: The rule has the wrong selector!" << std::endl;
        return 1;
    }
    if (rule->attributes.foreground_color != Colour(0x0000ffff)) {
        std::cerr << "ERROR: Attribute has the wrong value!" << std::endl;
        return 1;
    }

    rule = rules[1];
    if (rule == nullptr) {
        std::cerr << "ERROR: The rule is a nullptr!" << std::endl;
        return 1;
    }
    selectors = rule->getSelectors();
    if (selectors.size() != 1) {
        std::cerr << "ERROR: The rule has the wrong amount of selectors!" << std::endl;
        return 1;
    }
    if (selectors[0] != "div.warning") {
        std::cerr << "ERROR: The rule has the wrong selector!" << std::endl;
        return 1;
    }
    if (rule->attributes.display_method != VisualAttributes::Display::NONE) {
        std::cerr << "ERROR: Attribute has the wrong value!" << std::endl;
        return 1;
    }

    return 0;
}
