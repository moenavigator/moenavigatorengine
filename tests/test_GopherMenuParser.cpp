/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <array>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "../MarkupParsers/GopherMenuParser.h"
#include "../CommonClasses/DocumentNode.h"


bool checkInfoNode(const std::shared_ptr<DocumentNode> node, const std::string& text)
{
    if (node->name != "i") {
        std::cerr << "Wrong node hierarchy!" << std::endl;
        return false;
    }

    auto node_text = node->getFirstChild();
    if (node_text == nullptr) {
        std::cerr << "Missing node text!" << std::endl;
        return false;
    }
    if (node_text->name != text) {
        std::cerr << "Wrong node text!" << std::endl;
        return false;
    }

    return true;
}


bool checkLinkNode(
    const std::shared_ptr<DocumentNode> node,
    const std::string& url,
    const std::string& link_text
    )
{
    if (node->name != "link") {
        std::cerr << "Wrong node hierarchy!" << std::endl;
        return false;
    }
    if (!node->hasAttribute("href")) {
        std::cerr << "Missing href attribute for link!" << std::endl;
        return false;
    }
    if (node->getAttribute("href") != url) {
        std::cerr << "Wrong href attribute value!" << std::endl;
        return false;
    }
    auto node_text = node->getFirstChild();
    if (node_text == nullptr) {
        std::cerr << "Missing link text!" << std::endl;
        return false;
    }

    if (node_text->name != link_text) {
        std::cerr << "Wrong link text!" << std::endl;
        return false;
    }

    return true;
}


int main()
{
    const std::string test_menu = "iTest gopher menu\tfake\t(NULL)\t0\r\n"
        "iMultiple information text lines shall become one paragraph.\tfake\t(NULL)\t0\r\n"
        "0About moenavigatorengine\t/about.txt\tgopher.example.org\t70\t+\r\n"
        "iSome more text...\tfake\t(NULL)\t0\r\n"
        "1Files\tfiles\tgopher.example.org\t70\t+\r\n"
        "iAnother text line...\tfake\t(NULL)\t0\r\n";

    auto data = std::vector<uint8_t>(test_menu.begin(), test_menu.end());

    auto parser = std::make_shared<GopherMenuParser>();
    parser->parseData(data);
    auto document = parser->returnNodeTree();

    if (document == nullptr) {
        std::cerr << "Parser returned no document!" << std::endl;
        return 1;
    }

    if (document->name != "menu") {
        std::cerr << "Root node is not named \"menu\"!" << std::endl;
        return 1;
    }

    if (!document->hasChildNodes()) {
        std::cerr << "Root node has no child nodes!" << std::endl;
        return 1;
    }

    std::cerr << "DEBUG:\ntest menu:\n\n" << test_menu << "\n\nDocument as XML:\n\n" << document->toXML() << std::endl;

    auto child = document->children.at(0);
    //The first child should be an "i" node:
    if (!checkInfoNode(child, "Test gopher menu\r\nMultiple information text lines shall become one paragraph.\r\n")) {
        return 1;
    }

    child = child->getNextNode();
    if (child == nullptr) {
        std::cerr << "Wrong node hierarchy!" << std::endl;
        return 1;
    }

    //The second child should be a link with a href attribute.
    if (!checkLinkNode(child, "gopher://gopher.example.org:70/0/about.txt", "About moenavigatorengine")) {
        return 1;
    }

    child = child->getNextNode();
    if (child == nullptr) {
        std::cerr << "Wrong node hierarchy!" << std::endl;
        return 1;
    }

    //The third child should be a text node.
    if (!checkInfoNode(child, "Some more text...\r\n")) {
        return 1;
    }

    child = child->getNextNode();
    if (child == nullptr) {
        std::cerr << "Wrong node hierarchy!" << std::endl;
        return 1;
    }

    //The fourth node should be a link again.
    if (!checkLinkNode(child, "gopher://gopher.example.org:70/1/files", "Files")) {
        return 1;
    }

    child = child->getNextNode();
    if (child == nullptr) {
        std::cerr << "Wrong node hierarchy!" << std::endl;
        return 1;
    }

    //And the fifth and final node should be a text node.
    if (!checkInfoNode(child, "Another text line...\r\n")) {
        return 1;
    }

    return 0;
}
