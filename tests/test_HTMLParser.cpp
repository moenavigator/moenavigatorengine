/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <memory>
#include <string>


#include "../MarkupParsers/HTMLParser.h"


/**
 * This document should be parsed correctly.
 */
const std::string html_doc1 = "<html>"
    "<head>"
        "<title>Hello World!</title>"
        "<meta name=\"test\" content=\"something\">"
    "</head>"
    "<body>"
    "<h1>Hello World!</h1>"
    "<p class=\"intro\">This is just an intro text\
    that spans across multiple lines.</p>"
    "<ul id=\"list1\">"
        "<li>Section1"
            "<ol>"
                "<!-- We add some HTML weirdness (li tags without closing tags) here. -->"
                "<li>Something else"
                "<li>Another item"
            "</ol>"
        "</li>"
    "</ul>"
    "</body>"
    "</html>";




int main()
{
    auto parser = std::make_unique<HTMLParser>();

    try {
        parser->parseData(std::vector<uint8_t>(html_doc1.begin(), html_doc1.end()));
    } catch (MNE::MarkupParserException e) {
        //The document should be parseable.
        std::cerr << "The parser threw an exception while parsing a valid document!" << std::endl;
        return 1;
    }

    //Check the node structure:

    auto document = parser->returnNodeTree();
    if (document == nullptr) {
        std::cerr << "The parser did not build a DOM from the document!" << std::endl;
        return 1;
    }

    if (document->name != "html") {
        std::cerr << "The root node doesn\'t have the name \"html\"!" << std::endl;
        return 1;
    }

    if (document->children.size() != 2) {
        std::cerr << "The root node has the wrong amount of child nodes!" << std::endl;
        return 1;
    }

    //Check the head node:
    auto head = document->children[0];
    if (head == nullptr) {
        std::cerr << "The head node does not exist!" << std::endl;
        return 1;
    }
    if (head->children.size() != 2) {
        std::cerr << "The head node has the wrong amount of child nodes!" << std::endl;
        return 1;
    }
    auto title = head->children[0];
    if (title == nullptr) {
        std::cerr << "The title node does not exist!" << std::endl;
        return 1;
    }
    if (title->name != "title") {
        std::cerr << "The title node has the wrong name!" << std::endl;
        return 1;
    }
    auto title_text = title->getFirstChild();
    if (title_text == nullptr) {
        std::cerr << "The title node has no text!" << std::endl;
        return 1;
    }
    if (title_text->name != "Hello World!") {
        std::cerr << "The title node contains unexpected text!" << std::endl;
        return 1;
    }
    auto meta = head->children[1];
    if (meta == nullptr) {
        std::cerr << "The meta node does not exist!" << std::endl;
        return 1;
    }
    if (!meta->hasAttribute("name")) {
        std::cerr << "The meta node doesn\'t have the name attribute!" << std::endl;
        return 1;
    }
    if (meta->attributes["name"] != "test") {
        std::cerr << "The name attribute of the meta node has the wrong value!" << std::endl;
        return 1;
    }
    if (!meta->hasAttribute("content")) {
        std::cerr << "The meta node doesn\'t have the content attribute!" << std::endl;
        return 1;
    }
    if (meta->attributes["content"] != "something") {
        std::cerr << "The content attribute of the meta node has the wrong value!" << std::endl;
        return 1;
    }

    //Check the body node:
    auto body = document->children[1];
    if (body == nullptr) {
        std::cerr << "The body node doesn\'t exist!" << std::endl;
        return 1;
    }
    if (body->name != "body") {
        std::cerr << "The body node has the wrong name!" << std::endl;
        return 1;
    }
    if (body->children.size() != 3) {
        std::cerr << "The body node has the wrong amount of children!" << std::endl;
        return 1;
    }
    auto h1 = body->children[0];
    if (h1 == nullptr) {
        std::cerr << "The h1 node doesn\'t exist!" << std::endl;
        return 1;
    }
    if (h1->name != "h1") {
        std::cerr << "The h1 node has the wrong name!" << std::endl;
        return 1;
    }
    auto h1_text = h1->getFirstChild();
    if (h1_text == nullptr) {
        std::cerr << "The h1 node has no text!" << std::endl;
        return 1;
    }
    if (h1_text->name != "Hello World!") {
        std::cerr << "The h1 node contains the wrong text!" << std::endl;
        return 1;
    }
    auto p = body->children[1];
    if (p == nullptr) {
        std::cerr << "The p node doesn\'t exist!" << std::endl;
        return 1;
    }
    if (p->name != "p") {
        std::cerr << "The p node has the wrong name!" << std::endl;
        return 1;
    }
    auto p_text = p->getFirstChild();
    if (p_text == nullptr) {
        std::cerr << "The p node has no text!" << std::endl;
        return 1;
    }
    std::string expected_p_text = "This is just an intro text\
    that spans across multiple lines.";
    if (p->name != expected_p_text) {
        std::cerr << "The p node has the wrong text!" << std::endl;
        return 1;
    }
    auto ul = body->children[2];
    if (ul == nullptr) {
        std::cerr << "The ul node doesn\'t exist!" << std::endl;
        return 1;
    }
    if (ul->name != "ul") {
        std::cerr << "The ul node has the wrong name!" << std::endl;
        return 1;
    }
    if (!ul->hasAttribute("id")) {
        std::cerr << "The ul node has no id attribute!" << std::endl;
        return 1;
    }
    if (ul->attributes["id"] != "list1") {
        std::cerr << "The ul node\'s id attribute has the wrong value!" << std::endl;
        return 1;
    }
    if (ul->children.size() != 1) {
        std::cerr << "The ul node has the wrong amount of children!" << std::endl;
        return 1;
    }
    auto ul_li = ul->children[0];
    if (ul_li == nullptr) {
        std::cerr << "The ul\'s li node doesn't exist!" << std::endl;
        return 1;
    }
    if (ul_li->name != "li") {
        std::cerr << "The ul\'s li node has the wrong name!" << std::endl;
        return 1;
    }
    if (ul_li->children.size() != 2) {
        std::cerr << "The ul\'s li node has the wrong amount of children!" << std::endl;
        return 1;
    }
    auto ul_li_text = ul_li->children[0];
    if (ul_li_text == nullptr) {
        std::cerr << "The ul\'s li node has an empty first child!" << std::endl;
        return 1;
    }
    if (ul_li_text->name != "Section1") {
        std::cerr << "The ul\'s li node has the wrong text content!" << std::endl;
        return 1;
    }
    auto ol = ul_li->children[1];
    if (ol == nullptr) {
        std::cerr << "The ol node does not exist!" << std::endl;
        return 1;
    }
    if (ol->name != "ol") {
        std::cerr << "The ol node has the wrong name!" << std::endl;
        return 1;
    }
    if (ol->children.size() != 2) {
        std::cerr << "The ol node has the wrong amount of children!" << std::endl;
        return 1;
    }
    auto ol_li1 = ol->children[0];
    if (ol_li1 == nullptr) {
        std::cerr << "The ol node\'s first li node does not exist!" << std::endl;
        return 1;
    }
    if (ol_li1->name != "li") {
        std::cerr << "The ol node\'s first li node has the wrong name!" << std::endl;
        return 1;
    }
    auto ol_li1_text = ol_li1->getFirstChild();
    if (ol_li1_text == nullptr) {
        std::cerr << "The ol node\'s first li node has no child node!" << std::endl;
        return 1;
    }
    if (ol_li1_text->name != "Something else") {
        std::cerr << "The ol node\'s first li node contains the wrong text!" << std::endl;
        return 1;
    }
    auto ol_li2 = ol->children[1];
    if (ol_li2 == nullptr) {
        std::cerr << "The ol node\'s second li node does not exist!" << std::endl;
        return 1;
    }
    if (ol_li2->name != "li") {
        std::cerr << "The ol node\'s second li node has the wrong name!" << std::endl;
        return 1;
    }
    auto ol_li2_text = ol_li2->getFirstChild();
    if (ol_li2_text == nullptr) {
        std::cerr << "The ol node\'s second li node has no child node!" << std::endl;
        return 1;
    }
    if (ol_li2_text->name != "Another item") {
        std::cerr << "The ol node\'s second li node contains the wrong text!" << std::endl;
        return 1;
    }

    return 0;
}
