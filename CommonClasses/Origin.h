/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2019  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ORIGIN_H
#define ORIGIN_H


#include <cstdint>
#include "URL.h"


namespace MNE
{
    /**
     * This struct contains origin information of parsed code, like the
     * URL (or path) and the position in the source code.
     */
    struct Origin
    {
        /**
         * The line number in the source.
         */
        uint32_t line;

        /**
         * The column number in the source.
         */
        uint32_t column;

        /**
         * The URL to the source.
         */
        URL url;


        Origin()
        {
            this->line = 0;
            this->column = 0;
        }
    };
}


#endif
