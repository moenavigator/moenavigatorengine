/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNE__HASH
#define MNE__HASH


#include <cstdint>
#include <string>
#include <vector>


#ifdef TLS_USE_GNUTLS
#include <gnutls/gnutls.h>
#include <gnutls/crypto.h>
#endif


namespace MNE
{
    class Hash
    {
        public:


        /**
         * Generates an MD5 sum from the specified data.
         * If additional data is not empty, that data is appended to the MD5 sum
         * and another MD5 sum is generated which is then being returned instead
         * of the MD5 sum for the data alone.
         *
         * @param const std::vector<uint8_t>& data The data for which to build
         *     an MD5 sum.
         *
         * @param uint64_t additional_data Additional data to generate
         *     a second MD5 sum from the first and the additional data.
         *
         * @returns std::vector<uint8_t> The MD5 checksum for the data or the
         *     checksum of the checksum of the data in combination with the
         *     additional_data number.
         */
        static std::vector<uint8_t> MD5(
            const std::vector<uint8_t>& data,
            const std::vector<uint8_t>& additional_data = {}
            );


        /**
         * Generates a nonce of a specified size.
         *
         * @param const size_t bytes The amount of bytes to generate.
         *
         * @returns std::vector<uint8_t> The generated nonce.
         */
        static std::vector<uint8_t> generateNonce(const size_t bytes);
    };
}


#endif
