/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNE__FUNCTIONS
#define MNE__FUNCTIONS


#include <cstdint>
#include <sstream>
#include <string>
#include <vector>


#ifdef TLS_USE_GNUTLS
#include <cstring>
#include <gnutls/gnutls.h>
#endif


namespace MNE
{
    class Functions
    {
        public:


        /**
         * Converts data to a string of hexadecimal numbers. Each byte
         * is represented by two digits.
         *
         * @param const std::vector& data The data to be converted.
         *
         * @returns std::string A hexadecimal representation of the data.
         */
        static std::string toHexString(const std::vector<uint8_t>& data);


        /**
         * Converts data to a base64 representation.
         *
         * @param const std::vector<uint8_t>& bytes The data to be encoded.
         *
         * @returns std::vector<uint8_t> The base64 encoded data.
         */
        static std::vector<uint8_t> base64Encode(const std::vector<uint8_t>& data);
    };
}


#endif
