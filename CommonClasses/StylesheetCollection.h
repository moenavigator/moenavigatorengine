/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef STYLESHEETCOLLECTION_H
#define STYLESHEETCOLLECTION_H


#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "StylesheetGroup.h"
#include "StylesheetRule.h"


class StylesheetCollection
{
    public:


    std::vector<std::shared_ptr<StylesheetGroup>> groups;


    StylesheetCollection();


    ~StylesheetCollection();


    std::shared_ptr<StylesheetCollection> copy();


    void addGroup(unsigned char group_type, std::string group_name);


    std::shared_ptr<StylesheetGroup> getLastGroupRef();


    std::string toCSS();


    std::vector<std::shared_ptr<StylesheetRule>> findRules(
        const std::string& group_name,
        const std::vector<std::string>& selectors
        );


    std::vector<std::shared_ptr<StylesheetRule>> findRules(
        const std::string& group_name,
        const std::string& selector_str
        );


    void merge(std::shared_ptr<StylesheetCollection> other);
};


#endif
