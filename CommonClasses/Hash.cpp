/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Hash.h"


using namespace MNE;


std::vector<uint8_t> Hash::MD5(
    const std::vector<uint8_t>& data,
    const std::vector<uint8_t>& additional_data
    )
{
    #ifdef TLS_USE_GNUTLS
    gnutls_hash_hd_t handle;
    if (gnutls_hash_init(&handle, GNUTLS_DIG_MD5) < 0) {
        //We cannot init the handle.
        return {};
    }
    if (gnutls_hash(handle, data.data(), data.size()) < 0) {
        //We cannot hash the data.
        return {};
    }
    size_t hash_length = gnutls_hash_get_len(GNUTLS_DIG_MD5);

    std::vector<uint8_t> output1(hash_length);
    gnutls_hash_output(handle, output1.data());
    if (additional_data.empty()) {
        return output1;
    } else {
        //Do a second round of MD5 sum generation using the
        //first MD5 sum and the additional data.
        output1.insert(output1.end(), additional_data.begin(), additional_data.end());
        if (gnutls_hash(handle, output1.data(), output1.size()) < 0) {
            //We cannot hash the data.
            return {};
        }

        std::vector<uint8_t> output2(hash_length);
        gnutls_hash_output(handle, output2.data());
        return output2;
    }
    #else
    //Without a TLS library, we cannot generate an MD5 sum.
    return {};
    #endif
}


std::vector<uint8_t> Hash::generateNonce(const size_t bytes)
{
    #ifdef TLS_USE_GNUTLS
    std::vector<uint8_t> output(bytes);
    if (gnutls_rnd(GNUTLS_RND_NONCE, output.data(), bytes) < 0) {
        //We cannot generate a nonce.
        return {};
    }
    return output;
    #else
    //We need a TLS library to generate an MD5 sum.
    return {};
    #endif
}
