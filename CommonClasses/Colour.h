/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef COLOUR_H
#define COLOUR_H


#include <string>


#include <fmt/core.h>


class Colour
{
    public:

    /**
     * The red channel.
     */
    unsigned char r = 0;


    /**
     * The green channel.
     */
    unsigned char g = 0;


    /**
     * The blue channel.
     */
    unsigned char b = 0;


    /**
     * The alpha channel.
     */
    unsigned char alpha = 255;


    /**
     * The default constructor.
     */
    Colour()
    {
        this->r = 0;
        this->g = 0;
        this->b = 0;
        //We want to have full alpha on construction:
        this->alpha = 255;
    }


    /**
     * This constructor sets the colour channels by a 32 bit integer.
     * @see Colour::assignInt32 for a description.
     */
    Colour(const unsigned long int& int_colour)
    {
        this->assignInt32(int_colour);
    }


    /**
     * Sets the colour channels by a 32 bit integer value.
     * The integer must have the following format: High[ R G B A ]Low
     * The highest byte is the red channel, the lowest of the four
     * bytes represents the alpha channel.
     */
    void assignInt32(const unsigned long int& int_colour)
    {
        this->r = ((int_colour & 0xFF000000) >> 24);
        this->g = ((int_colour & 0x00FF0000) >> 16);
        this->b = ((int_colour & 0x0000FF00) >> 8);
        this->alpha = ((int_colour & 0x000000FF));
    }


    /**
     * Converts the colour value to a hex string. Note that the
     * alpha channel is omitted since a hex string representation
     * including the alpha channel is not yet widespread.
     *
     * @returns std::string The hex string in the format "#rrggbb".
     *
     */
    std::string toHexString()
    {
        return fmt::format(
            "#{0:02x}{1:02x}{2:02x}",
            this->r,
            this->g,
            this->b
            );
    }


    bool operator==(const Colour& other) const
    {
        return (this->r == other.r)
            && (this->g == other.g)
            && (this->b == other.b)
            && (this->alpha == other.alpha);
    }


    bool operator!=(const Colour& other) const
    {
        return !(*this == other);
    }
};


#endif
