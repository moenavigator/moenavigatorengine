/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DOCUMENTNODE_H
#define DOCUMENTNODE_H


/**
This file is part of the DOM 4 implementation in MoeNavigator
See http://dvcs.w3.org/hg/domcore/raw-file/tip/Overview.html
for specifications.
*/


#include <memory>
#include <iostream>
#include <vector>
#include <map>
#include <string>


#include "../Exceptions/Exception.h"
#include "VisualAttributes.h"
#include "Origin.h"


enum
{
    DOCUMENTNODE_UNDEFINED, //0
    DOCUMENTNODE_TAG,       //1
    DOCUMENTNODE_ATTRIBUTE, //2
    DOCUMENTNODE_ATTRVALUE, //3
    DOCUMENTNODE_TEXT,      //4
    DOCUMENTNODE_COMMENT,   //5
    DOCUMENTNODE_CONTROL    //6
};


/**
 * This class represents a node of a document in the document
 * object model (DOM).
 */
class DocumentNode: public std::enable_shared_from_this<DocumentNode>
{
    private:


    /**
     * This attribute holds an unique ID for the node.
     * It is set on construction and can't be altered.
     */
    unsigned long long int node_id;

    /**
     * This class attribute holds the next ID
     * which is used on the construction of the next node.
     */
    static unsigned long long int next_id;

    /**
     *
     */
    static unsigned long long int getNodeID();


    protected:


    //Tree traversal attributes and methods:

    /**
     * The current position marker when traversing a tree.
     */
    std::weak_ptr<DocumentNode> tree_traversal_position;

    /**
     * Traverses to the next node.
     * This method iterates over all child nodes.
     * The next node is referenced in $tree_traversal_position.
     */
    void traverseNext(bool only_tags = true);


    /**
     * The parent node of this node.
     */
    std::weak_ptr<DocumentNode> parent_node;


    /**
     * The previous node.
     */
    std::weak_ptr<DocumentNode> previous;


    /**
     * The next node.
     */
    std::weak_ptr<DocumentNode> next;



    public:


    /**
     * enum to distinguish the types of document nodes.
     */
    enum class Type
    {
        NONE, //Nothing.
        NODE, //A node.
        TEXT, //A text node.
        CONTROL, //A control node.
    };

    std::string name;
    Type type;

    /**
     * The origin of the node.
     */
    MNE::Origin origin;

    const unsigned long long int returnNodeID() const;

    /**
     * This attribute holds style information for this node.
     */
    VisualAttributes style;


    /**
     * This vector contains all child nodes of this node.
     */
    std::vector<std::shared_ptr<DocumentNode>> children;


    /**
     * This map holds all the attributes of this node.
     */
    std::map<std::string, std::string> attributes;


    /**
     * The standard constructor.
     */
    DocumentNode(const std::string& name, const Type type);


    /**
     * The copy constructor.
     */
    DocumentNode(const DocumentNode& other);


    /**
     * The move constructor.
     */
    DocumentNode(DocumentNode&& other);


    /**
     * The destructor.
     */
    ~DocumentNode();


    /**
     * The overloaded assignment operator.
     */
    DocumentNode& operator=(const DocumentNode& other);


    /**
     * The move assignment operator.
     */
    DocumentNode& operator=(DocumentNode&& other);


    /**
     * This is a debug function which outputs the basic attributes
     * of a DocumentNode instance in the JSON format.
     */
    std::string toJSON();


    /**
     * ToTree is a debug function which outputs the structure
     * of a node as a string.
     */
    std::string toTree(unsigned short layer = 0);


    /**
     * Converts the node, its attributes and children to a string
     * containing an XML representation of the node tree.
     */
    std::string toXML();

    ///////////////
    // DOM methods:
    ///////////////

    //DOM element selection:

    /**
     * Traverses the tree below the node to find an element with the
     * specified ID.
     *
     * @param std::string id The ID to search for.
     */
    std::shared_ptr<DocumentNode> getElementById(std::string id = "");

    std::vector<std::shared_ptr<DocumentNode>> getElementsByTagName(
        std::string name = ""
        );

    std::vector<std::shared_ptr<DocumentNode>> getElementsByClassName(
        std::string name = ""
        );

    std::shared_ptr<DocumentNode> querySelector(
        std::string selector = ""
        );

    std::vector<std::shared_ptr<DocumentNode>> querySelectorAll(
        std::string selector = ""
        );


    /**
     * Returns a pointer to the parent node of this node, if a parent exists.
     *
     * @returns std::shared_ptr<DocumentNode> A pointer to the parent node
     *     or nullptr if no parent node exists.
     */
    std::shared_ptr<DocumentNode> getParentNode();


    /**
     * Sets the parent node for this node. The parent node can be a nullptr
     * to remove a child from a previous parent.
     *
     * @param std::shared_ptr<DocumentNode> new_parent The new parent node.
     */
    void setParentNode(std::shared_ptr<DocumentNode> new_parent = nullptr);


    /**
     * Returns a pointer to the previous node of this node, if a
     * predecessor exists.
     *
     * @returns std::shared_ptr<DocumentNode> A pointer to the previous node
     *     or nullptr if no predecessor exists.
     */
    std::shared_ptr<DocumentNode> getPreviousNode();


    /**
     * Sets the previous node for this node. The previous node can be a nullptr
     * to unlink a node from its predecessor.
     *
     * @param std::shared_ptr<DocumentNode> new_previous The new predecessor.
     */
    void setPreviousNode(std::shared_ptr<DocumentNode> new_previous = nullptr);


    /**
     * Returns a pointer to the next node of this node, if a
     * successor exists.
     *
     * @returns std::shared_ptr<DocumentNode> A pointer to the next node
     *     or nullptr if no successor exists.
     */
    std::shared_ptr<DocumentNode> getNextNode();


    /**
     * Sets the next node for this node. The next node can be a nullptr
     * to unlink a node from its successor.
     *
     * @param std::shared_ptr<DocumentNode> new_next The new successor.
     */
    void setNextNode(std::shared_ptr<DocumentNode> new_next = nullptr);


    //DOM manipulation:


    /**
     * This is a helper method for the appendChild method that creates
     * a new node from a node name and type.
     */
    void appendChild(const std::string& child_name, const Type& type);


    /**
     * @throws MNE::Exception If a circular hierarchy would be created
     *     when inserting the child.
     */
    void appendChild(std::shared_ptr<DocumentNode> child);


    std::shared_ptr<DocumentNode> getFirstChild();

    void addAttribute(
        const std::string& attribute_name,
        const std::string& attribute_value = ""
        );


    /*
     * temporarily disabled.
     void addSibling(std::string siblingname, Type type);
     void addSibling(std::shared_ptr<DocumentNode> new_sibling);
    */


    /**
     * Removes one or more child nodes by using CSS selectors.
     */
    void removeChildsBySelector(const std::string& selector);

    std::shared_ptr<DocumentNode> getLastChild();


    /**
     * Clones this node, either with all child nodes or without them.
     *
     * @param bool deep Whether to include all child nodes into the
     *     cloning process (true) or to just clone this node (false).
     *     Defaults to true.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Node/cloneNode
     */
    std::shared_ptr<DocumentNode> cloneNode(bool deep = true);


    /**
     * Indicates whether this node has child nodes or not.
     *
     * @returns bool True, if this node has at least one child node,
     *     false otherwise.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Node/hasChildNodes
     */
    bool hasChildNodes();


    /**
     * Indicates whether the attribute specified by its name exists
     * for this node.
     *
     * @param std::string name The name of the attribute.
     *
     * @returns bool True, whether an attribute with the specified name
     *     is set, false otherwise.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Element/hasAttribute
     */
    bool hasAttribute(const std::string& name) const;



    /**
     * Sets the value of an attribute that either is created in that process
     * or updated in case it already exists.
     *
     * @param std::string name The name of the attribute.
     *
     * @param std::string value The value of the attribute.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Element/setAttribute
     */
    void setAttribute(const std::string& name, const std::string& value);


    /**
     * Returns the value of the attribute specified by its name.
     * Note that for non-existant attributes, the value is an empty string, too.
     *
     * @param std::string name The name of the attribute.
     *
     * @returns std::string The value of the attribute or an empty string,
     *     if the attribute doesn't exist or the name is an empty string.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Element/getAttribute
     */
    std::string getAttribute(const std::string& name);


    /**
     * Determines whether this node and another node are the same.
     * This means that their raw pointer points to the same object
     * (to the same memory address).
     *
     * @param std::shared_ptr<DocumentNode> other_node A pointer that shall be
     *     checked for equality with this node.
     *
     * @returns bool True, if other_node is the same as this node,
     *     false otherwise.
     */
    bool isSameNode(std::shared_ptr<DocumentNode> other_node);


    /**
     * Merges several text child nodes into one, if they are direct siblings.
     * The text is appended to the first node of a sequence of text nodes and
     * all but the first text node of that sequence are deleted.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Node/normalize
     */
    void normalize();


    /**
     * This method is the same as normalize, but with a British English name.
     *
     * @see DocumentNode::normalize
     */
    void normalise();


    /**
     * Inserts a new node before the reference node which is a child node
     * of this node.
     *
     * @param std::shared_ptr<DocumentNode> new_node The node to be inserted.
     *
     * @param std::shared_ptr<DocumentNode> reference_node The child node that
     *     shall succeed new_node after the latter has been added as child.
     *     If reference_node is a nullptr, new_node will be added as last child
     *     to this node.
     *
     * @returns std::shared_ptr<DocumentNode> A pointer to new_node in case the
     *     insertion of new_node has been successful. nullptr is returned when
     *     reference_node is not a child of this node or new_node itself
     *     is a nullptr.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Node/insertBefore
     */
    std::shared_ptr<DocumentNode> insertBefore(
        std::shared_ptr<DocumentNode> new_node,
        std::shared_ptr<DocumentNode> reference_node
        );


    /**
     * Removes a child node.
     *
     * @param std::shared_ptr<DocumentNode> child The child node to be removed.
     *
     * @returns std::shared_ptr<DocumentNode> The child node after its removal.
     *     In case the specified node is not a child node or a nullptr,
     *     a nullptr is returned.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Node/removeChild
     */
    std::shared_ptr<DocumentNode> removeChild(
        std::shared_ptr<DocumentNode> child
        );


    /**
     * Replaces a child node with another node.
     *
     * @param std::shared_ptr<DocumentNode> new_child The new node that
     *     is replacing the old child node.
     *
     * @param std::shared_ptr<DocumentNode> old_child The old child node
     *     that is being replaced.
     *
     * @returns std::shared_ptr<DocumentNode> The replaced child node.
     *     If old_child or new_child is a nullptr or old_child is not a
     *     child of this node, a nullptr is returned.
     *
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Node/replaceChild
     */
    std::shared_ptr<DocumentNode> replaceChild(
        std::shared_ptr<DocumentNode> new_child,
        std::shared_ptr<DocumentNode> old_child
        );
};


#endif
