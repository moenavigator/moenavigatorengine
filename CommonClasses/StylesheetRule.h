/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2019  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STYLESHEETRULE_H
#define STYLESHEETRULE_H


#include <cstdint>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "VisualAttributes.h"


class StylesheetRule
{
    protected:

    /**
     * The specificity of the rule, calculated by its selectors.
     */
    uint32_t specificity;

    /**
     * The selectors for this rule.
     */
    std::vector<std::string> selectors; //multiple selectors are possible


    void calculateSpecificity();


    public:

    /**
     * Returns the selectors for this rule.
     */
    std::vector<std::string> getSelectors();

    /**
     * Sets the selectors for this rule.
     */
    void setSelectors(const std::vector<std::string>& selectors);


    /**
     * Adds one selector to the list of selectors.
     */
    void addSelector(const std::string& selector);


    VisualAttributes attributes;


    StylesheetRule();


    StylesheetRule(const StylesheetRule& other);


    /**
     * Constructs a stylesheet rule with only one selector.
     */
    StylesheetRule(const std::string& selector);


    /**
     * Constructs a stylesheet rule with multiple selectors.
     */
    StylesheetRule(const std::vector<std::string>& selectors);


    ~StylesheetRule();


    StylesheetRule& operator=(const StylesheetRule& other);


    std::shared_ptr<StylesheetRule> copy();


    StylesheetRule(const std::vector<std::string>& selectors, const VisualAttributes& attributes);


    std::string toCSS();
};


#endif
