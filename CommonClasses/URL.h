/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2018  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef URL_H
#define URL_H


#include "../MNECommon/MNECommon.h"
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <string>
#include <vector>


namespace MNE
{
    class URL
    {
        public:


        URL();


        /**
         * A constructor that accepts an URL string and splits it into
         * its parts.
         *
         * @param std::string url The URL to be used (splitted).
         *
         * @throws std::invalid_argument If the port number in the URL is not
         *     a number that can be parsed by the stoul function.
         *
         * @throws std::out_of_range If the port number in the URL is bigger
         *     than 2^32 -1 (the maximum for uint32_t).
         *
         * @throws MNE::Exception If the parsed port number is
         *     greater than 65535.
         */
        URL(const std::string& url);


        /**
         * Combines the protocol, domain and path into an URL string.
         *
         * @returns string A string representation of the URL.
         */
        std::string combine();

        /**
         * The protocol part of the URL: http, https, ftp, ...
         */
        std::string protocol = "";

        /**
         * The (optional) user info part of the URL.
         */
        std::string user_info = "";

        /**
         * The domain part of the URL: example.org, w3.org, ...
         */
        std::string domain = "";


        /**
         * The port on which to connect to the domain.
         */
        uint16_t port = 0;

        /**
         * The path part of the URL: /, /index.html, /index.php, ...
         */
        std::string path = "";


        /**
         * Percent encodes data and returns it.
         *
         * @param const std::vector<uint8_t>& data The data to be encoded.
         *
         * @param bool strict Whether to strictly use percent encoding (true)
         *    or to allow encoding spaces as "+" characters (false).
         *    Defaults to true.
         *
         * @returns std::string The URL encoded data.
         */
        static std::string percentEncode(const std::vector<uint8_t>& data, bool strict = true);


        /**
         * Decodes data from a percent encoded string and returns it.
         *
         * @param const std::string& data The encoded data to be decoded.
         *
         * @returns std::vector<uint8_t> The decoded data.
         */
        static std::vector<uint8_t> percentDecode(const std::string& data);
    };
}


#endif
