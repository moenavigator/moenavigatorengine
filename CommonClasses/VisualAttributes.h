/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VISUALATTRIBUTES_H
#define VISUALATTRIBUTES_H

#include <sstream>
#include <string>
#include <inttypes.h>

#include "Colour.h"


/**
 * This class holds all visual attributes of a document node.
 * **/
class VisualAttributes
{
  public:


    /**
     * This enum class defines the border styles for element borders.
     */
    enum class BorderStyle
    {
        NONE,
        SOLID
        //More to be added
    };


    /**
     * The Display enum class defines the display types for elements.
     */
    enum class Display
    {
        NONE,
        INLINE,
        BLOCK,
        TABLE,
        TABLE_ROW,
        TABLE_COLUMN,
        TABLE_CELL,
        INLINE_BLOCK,
        FLEX
    };

    unsigned long int width;
    unsigned long int height;

    unsigned long int max_width;
    unsigned long int max_height;
    unsigned long int min_width;
    unsigned long int min_height;


    /**
     * The X-axis start coordinate inside the parent node.
     */
    unsigned long int coord_x;


    /**
     * The Y-axis start coordinate inside the parent node.
     */
    unsigned long int coord_y;


    short z_index;


    /**
     * @see Display enum class.
     */
    VisualAttributes::Display display_method;

    Colour background_color;
    Colour foreground_color;
    Colour border_color;

    //The string font_family causes segmentation faults
    //when a StylesheetRule is returned in MoeNavigatorEngine::SetNodeStyle.
    std::string font_family;

    uint16_t font_size;
    unsigned char font_style; //italic, normal, ...
    unsigned char font_weight; //normal, bold, extrabold, ...
    uint16_t line_height;

    uint16_t border_left_width;
    uint16_t border_top_width;
    uint16_t border_right_width;
    uint16_t border_bottom_width;

    unsigned short border_radius;


    /**
     * @see The BorderStyle enum class.
     */
    VisualAttributes::BorderStyle border_style;

    uint16_t padding_left;
    uint16_t padding_top;
    uint16_t padding_right;
    uint16_t padding_bottom;
    uint16_t margin_left;
    uint16_t margin_top;
    uint16_t margin_right;
    uint16_t margin_bottom;


    /**
     * The default constructor.
     */
    VisualAttributes();


    /**
     * The copy constructor.
     */
    VisualAttributes(const VisualAttributes& other);


    /**
     * The overloaded assignment operator.
     */
    VisualAttributes& operator=(const VisualAttributes &other);


    /**
     * The toCSS method converts all the attributes into CSS rules.
     * Selectors are not added since the VisualAttributes class does
     * not store that information.
     *
     * @returns std::string CSS rules for the VisualAttributes object.
     */
    std::string toCSS();
};


#endif
