/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "VisualAttributes.h"


VisualAttributes::VisualAttributes()
{
    this->width = 0;
    this->height = 0;
    this->max_width = 0;
    this->max_height = 0;
    this->min_width = 0;
    this->min_height = 0;
    this->coord_x = 0;
    this->coord_y = 0;
    this->z_index = 0;
    this->display_method = VisualAttributes::Display::INLINE;

    this->foreground_color = Colour(0x000000FF);
    this->background_color = Colour(0xFFFFFFFF);
    this->border_color = Colour(0xFFFFFFFF);

    this->font_family = "";
    this->font_size = 0;
    this->font_style = 0;
    this->font_weight = 0;
    this->line_height = 0;

    this->border_left_width = 0;
    this->border_top_width = 0;
    this->border_right_width = 0;
    this->border_bottom_width = 0;

    this->border_radius = 0;
    this->border_style = VisualAttributes::BorderStyle::NONE;

    this->padding_left = 0;
    this->padding_top = 0;
    this->padding_right = 0;
    this->padding_bottom = 0;
    this->margin_left = 0;
    this->margin_top = 0;
    this->margin_right = 0;
    this->margin_bottom = 0;
}


VisualAttributes::VisualAttributes(const VisualAttributes& other)
{
    this->width = other.width;
    this->height = other.height;
    this->coord_x = other.coord_x;
    this->coord_y = other.coord_y;
    this->z_index = other.z_index;
    this->display_method = other.display_method;

    this->foreground_color = other.foreground_color;
    this->background_color = other.background_color;
    this->border_color = other.border_color;

    this->font_family = other.font_family;
    this->font_size = other.font_size;
    this->font_style = other.font_style;
    this->font_weight = other.font_weight;
    this->line_height = other.line_height;

    this->border_left_width = other.border_left_width;
    this->border_top_width = other.border_top_width;
    this->border_right_width = other.border_right_width;
    this->border_bottom_width = other.border_bottom_width;

    this->border_radius = other.border_radius;
    this->border_style = other.border_style;

    this->padding_left = other.padding_left;
    this->padding_top = other.padding_top;
    this->padding_right = other.padding_right;
    this->padding_bottom = other.padding_bottom;
    this->margin_left = other.margin_left;
    this->margin_top = other.margin_top;
    this->margin_right = other.margin_right;
    this->margin_bottom = other.margin_bottom;
}


VisualAttributes& VisualAttributes::operator=(const VisualAttributes &other)
{
    if (this == &other) {
        return *this;
    }

    this->width = other.width;
    this->height = other.height;
    this->coord_x = other.coord_x;
    this->coord_y = other.coord_y;
    this->z_index = other.z_index;
    this->display_method = other.display_method;

    this->foreground_color = other.foreground_color;
    this->background_color = other.background_color;
    this->border_color = other.border_color;

    this->font_family = other.font_family;
    this->font_size = other.font_size;
    this->font_style = other.font_style;
    this->font_weight = other.font_weight;
    this->line_height = other.line_height;

    this->border_left_width = other.border_left_width;
    this->border_top_width = other.border_top_width;
    this->border_right_width = other.border_right_width;
    this->border_bottom_width = other.border_bottom_width;

    this->border_radius = other.border_radius;
    this->border_style = other.border_style;

    this->padding_left = other.padding_left;
    this->padding_top = other.padding_top;
    this->padding_right = other.padding_right;
    this->padding_bottom = other.padding_bottom;
    this->margin_left = other.margin_left;
    this->margin_top = other.margin_top;
    this->margin_right = other.margin_right;
    this->margin_bottom = other.margin_bottom;

    return *this;
}


std::string VisualAttributes::toCSS()
{
    std::stringstream result;
    result << "width: " << this->width << "px;\r\n"
           << "height: " << this->height << "px;\r\n"
           << "z-index: " << this->z_index << ";\r\n";

    result << "background-color: rgba("
           << this->background_color.r << ", "
           << this->background_color.g << ", "
           << this->background_color.b << ", "
           << (this->background_color.alpha/255.f) << ");\r\n";

    result << "color: rgba("
           << this->foreground_color.r << ", "
           << this->foreground_color.g << ", "
           << this->foreground_color.b << ", "
           << (this->foreground_color.alpha/255.f) << ");\r\n";

    result << "border-left-width: " << this->border_left_width << "px;\r\n"
           << "border-top-width: " << this->border_top_width << "px;\r\n"
           << "border-right-width: " << this->border_right_width << "px;\r\n"
           << "border-bottom-width: " << this->border_bottom_width << "px;\r\n";

    result << "padding-left: " << this->padding_left << "px;\r\n"
           << "padding-top: " << this->padding_top << "px;\r\n"
           << "padding-right: " << this->padding_right << "px;\r\n"
           << "padding-bottom: " << this->padding_bottom << "px;\r\n"
           << "margin-left: " << this->margin_left << "px;\r\n"
           << "margin-top: " << this->margin_top << "px;\r\n"
           << "margin-right: " << this->margin_right << "px;\r\n"
           << "margin-bottom: " << this->margin_bottom << "px;\r\n";

    result << "font-size: " << this->font_size << "p;\r\n"
           << "line-height: " << this->line_height << "px;\r\n";

    return result.str();
}
