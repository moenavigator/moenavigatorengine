/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2019  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "URL.h"


using namespace MNE;


URL::URL()
{
    //Nothing at the moment
}


URL::URL(const std::string& url)
{
    //find protocol information:
    size_t protocol_pos = url.find(":");
    this->protocol = url.substr(0, protocol_pos);

    if (this->protocol == "file") {
        size_t path_pos = url.find("/", protocol_pos + 3);
        //The file protocol has no domain but ony a path.
        this->domain = "";
        this->path = url.substr(path_pos, std::string::npos);
    } else {
        size_t user_info_pos = url.find_first_not_of(
            "/",
            protocol_pos + 1
            );
        size_t domain_pos = url.find("@", user_info_pos);
        if (domain_pos == std::string::npos) {
            //No user info in URL.
            domain_pos = user_info_pos;
            user_info_pos = 0;
        }
        size_t path_pos = url.find("/", domain_pos);

        if (user_info_pos > 0) {
            this->user_info = url.substr(user_info_pos, domain_pos - user_info_pos);
            //Increment domain_pos by one to avoid having the "@"-character
            //in the domain.
            domain_pos++;
        }
        this->domain = url.substr(domain_pos, path_pos - domain_pos);

        //Check if a port is in the domain name:
        size_t port_pos = this->domain.find(":");
        if (port_pos != std::string::npos) {
            auto port_str = this->domain.substr(port_pos + 1, std::string::npos);
            if (port_str.length() > 0) {
                uint32_t parsed_port = std::stoul(port_str);
                if (parsed_port > 65535) {
                    throw MNE::Exception(
                        "URL",
                        "The port number is out of range!",
                        1
                        );
                }
                this->port = parsed_port;
            }
            this->domain = this->domain.substr(0, port_pos);
        }

        try {
            this->path = url.substr(path_pos);
        } catch(std::out_of_range& oor) {
            this->path = "/";
        }

        if (this->port == 0) {
            //No port has been provided in the URL string. So we set it
            //using the standard port of the protocol.
            if (this->protocol == "http") {
                this->port = 80;
            } else if (this->protocol == "https") {
                this->port = 443;
            } else if (this->protocol == "gopher") {
                this->port = 70;
            } else if (this->protocol == "qotd") {
                this->port = 17;
            }
        }
    }
}


std::string URL::combine()
{
    return this->protocol + "://" +
        (!this->user_info.empty() ? this->user_info + "@" : "")
        + this->domain + this->path;
}


std::string URL::percentEncode(const std::vector<uint8_t>& data, bool strict)
{
    std::string output;
    for (uint8_t byte: data) {
        if ((byte <= 0x21) || ((byte >= 0x23) && (byte <= 0x2C))
            || (byte == 0x2F) || (byte == 0x3A) || (byte == 0x3B)
            || (byte == 0x3D) || (byte == 0x3F) || (byte == 0x40)
            || (byte == 0x5B) || (byte == 0x5D) || (byte > 0x7E)
            ) {
            //Control characters, reserved characters and other
            //non-printable ASCII characters that need to be encoded.
            if ((strict == false) && byte == 0x20) {
                //Special treatment for the space character:
                //Use a plus character.
                output += "+";
            } else {
                output += "%";
                std::stringstream number;
                number << std::hex << static_cast<int>(byte);
                auto number_str = number.str();
                if (byte < 0x10) {
                    //Add a leading zero to the number.
                    output += '0' + number_str;
                } else {
                    //Just append the number.
                    output += number_str;
                }
            }
        } else {
            //ASCII characters that just can be passed.
            output += byte;
        }
    }
    return output;
}


std::vector<uint8_t> URL::percentDecode(const std::string& data)
{
    std::vector<uint8_t> output;
    std::string percent_chars;
    uint8_t percent_char_pos = 0;
    for (char c: data) {
        if (percent_char_pos > 0) {
            percent_chars += c;
            percent_char_pos++;
        } else {
            if (c == '%') {
                percent_char_pos++;
            } else {
                if (c == '+') {
                    //A space character encoded as plus character.
                    output.push_back(0x20);
                } else {
                    //Just copy the character.
                    output.push_back(c);
                }
            }
        }
        if (percent_char_pos > 2) {
            //We have read a complete percent encoded character.
            //Convert the content in percent_chars to one byte
            //and append it to the output vector.
            try {
                uint32_t decoded_char = std::stoul(percent_chars, nullptr, 16);
                output.push_back(decoded_char & 0xFF);
            } catch (std::invalid_argument& e) {
                //TODO: Decide what to do with invalid percent encoded
                //characters.
            }
            percent_chars = "";
            percent_char_pos = 0;
        }
    }
    return output;
}
