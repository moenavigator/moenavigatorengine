/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "StylesheetCollection.h"


StylesheetCollection::StylesheetCollection()
{

}


StylesheetCollection::~StylesheetCollection()
{
    this->groups.clear();
}


std::shared_ptr<StylesheetCollection> StylesheetCollection::copy()
{
    auto collection_copy = std::make_shared<StylesheetCollection>();

    for (auto group: this->groups) {
        collection_copy->groups.push_back(group->copy());
    }

    return collection_copy;
}


void StylesheetCollection::addGroup(unsigned char group_type, std::string group_name)
{
    this->groups.push_back(
        std::make_shared<StylesheetGroup>(group_type, group_name)
        );
}


std::shared_ptr<StylesheetGroup> StylesheetCollection::getLastGroupRef()
{
    return this->groups.back();
}


std::string StylesheetCollection::toCSS()
{
    std::string result = "";
    for (auto group: this->groups) {
        result += group->toCSS();
    }

    return result;
}


std::vector<std::shared_ptr<StylesheetRule>> StylesheetCollection::findRules(
    const std::string& group_name,
    const std::vector<std::string>& selectors
    )
{
    std::vector<std::shared_ptr<StylesheetRule>> found_rules;
    for (auto group: this->groups) {
        if (group->name == group_name) {
            //if $GroupName and the name of the current group in the $Groups array match
            //search all the rules of that group if their selector matches $Selector
            for (auto rule: group->rules) {
                for (auto rule_selector: rule->getSelectors()) {
                    if (rule_selector == "*") {
                        //A rule with the asterisk selector matches
                        //all rules.
                        //TODO: improve this so that such rules do not
                        //overwrite more specifc rules!
                        found_rules.push_back(rule->copy());
                    } else {
                        for (auto selector: selectors) {
                            if (rule_selector == selector) {
                                //return a copy of the first matching rule

                                found_rules.push_back(rule->copy());
                                //TODO: think about returning the last matching rule
                                //since newer rules overwrite older rules in CSS
                                // ==> general CSS parsing problem?
                            }
                        }
                    }
                }
            }
        }
    }

    return found_rules;
}


std::vector<std::shared_ptr<StylesheetRule>> StylesheetCollection::findRules(
    const std::string& group_name,
    const std::string& selector_str
    )
{
    std::vector<std::string> selectors;
    selectors.push_back(selector_str);
    //This method calls its "twin method" which has
    //a vector of selectors as second parameter.
    return this->findRules(group_name, selectors);
}


void StylesheetCollection::merge(std::shared_ptr<StylesheetCollection> other)
{
    if (other == nullptr) {
        return;
    }

    //to be improved
    if (other->groups.size() == 0) {
        //nothing to do
        return;
    }

    if (this->groups.size() == 0) {
        //We can just use the groups in $other:
        this->groups = other->groups;
        return;
    }

    for (std::shared_ptr<StylesheetGroup> other_group: other->groups) {
        if (other_group == nullptr) {
            continue;
        }

        if (other_group->rules.size() == 0) {
            continue;
        }

        for (std::shared_ptr<StylesheetGroup> group: this->groups) {
            if (group == nullptr) {
                continue;
            }
            if ((group->group_type == other_group->group_type) &&
                (group->name == other_group->name)) {
                //Copy the rules from $other:
                for (std::shared_ptr<StylesheetRule> rule: other_group->rules) {
                    if (rule != nullptr) {
                        group->rules.push_back(rule->copy());
                    }
                }
            }
        }
    }
}
