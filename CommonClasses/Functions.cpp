/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "Functions.h"


using namespace MNE;


std::string Functions::toHexString(const std::vector<uint8_t>& data)
{
    std::stringstream output;
    for (auto byte: data) {
        if (byte > 0x0F) {
            //The byte will have two characters.
            output << std::hex << static_cast<int>(byte);
        } else {
            //The byte will only have one character and therefore
            //has to be prependede by a zero.
            output << '0' << std::hex << static_cast<int>(byte);
        }
    }
    return output.str();
}


std::vector<uint8_t> Functions::base64Encode(const std::vector<uint8_t>& data)
{
    if (data.empty()) {
        return {};
    }
    #ifdef TLS_USE_GNUTLS
    gnutls_datum_t gnutls_data;
    uint8_t* data_ptr = (uint8_t*)gnutls_malloc(data.size());
    if (data_ptr == nullptr) {
        //We cannot use the data.
        return {};
    }
    std::memcpy(data_ptr, data.data(), data.size());
    gnutls_data.data = data_ptr;
    gnutls_data.size = data.size();
    gnutls_datum_t gnutls_output;
    gnutls_base64_encode2(&gnutls_data, &gnutls_output);
    if (gnutls_output.data == nullptr) {
        gnutls_free(data_ptr);
        return {};
    }
    std::vector<uint8_t> output(gnutls_output.data, gnutls_output.data + gnutls_output.size);
    gnutls_free(data_ptr);
    gnutls_free(gnutls_output.data);
    return output;
    #else
    //When there is no implementation, we cannot base64 encode the data.
    return {};
    #endif
}
