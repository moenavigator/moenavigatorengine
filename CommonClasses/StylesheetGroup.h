/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef STYLESHEETGROUP_H
#define STYLESHEETGROUP_H


#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "StylesheetRule.h"


enum
{
  STYLESHEETGROUPTYPE_MEDIA // Group that selects media ("@media print" for example)
};



class StylesheetGroup
{
    public:


    unsigned char group_type;


    std::string name;


    //to become private:
    std::vector<std::shared_ptr<StylesheetRule>> rules;


    StylesheetGroup();


    StylesheetGroup(const unsigned char new_group_type, const std::string& new_name);


    ~StylesheetGroup();


    void addRule(const std::vector<std::string>& selectors, const VisualAttributes& attributes);


    std::string toCSS();


    std::shared_ptr<StylesheetGroup> copy();
};


#endif
