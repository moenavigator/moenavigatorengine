/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2019  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "StylesheetRule.h"



void StylesheetRule::calculateSpecificity()
{
    //TODO
}


std::vector<std::string> StylesheetRule::getSelectors()
{
    return this->selectors;
}


void StylesheetRule::setSelectors(const std::vector<std::string>& selectors)
{
    this->selectors = selectors;
}


void StylesheetRule::addSelector(const std::string& selector)
{
    if (selector != "") {
        this->selectors.push_back(selector);
    }
}


StylesheetRule::StylesheetRule()
{

}


StylesheetRule::StylesheetRule(const StylesheetRule& other)
{
    this->selectors = other.selectors;
    this->attributes = other.attributes;
}


StylesheetRule::StylesheetRule(const std::string& selector)
{
    this->selectors.push_back(selector);
}


StylesheetRule::StylesheetRule(const std::vector<std::string>& selectors)
{
    this->selectors = selectors;
}


StylesheetRule::~StylesheetRule()
{
    this->selectors.clear();
}


StylesheetRule& StylesheetRule::operator=(const StylesheetRule& other)
{
    this->selectors = other.selectors;
    this->attributes = other.attributes;
    return *this;
}


std::shared_ptr<StylesheetRule> StylesheetRule::copy()
{
    auto rule_copy = std::make_shared<StylesheetRule>();
    rule_copy->selectors = this->selectors;
    rule_copy->attributes = this->attributes;
    return rule_copy;
}


StylesheetRule::StylesheetRule(const std::vector<std::string>& selectors, const VisualAttributes& attributes)
{
    this->selectors = selectors;
    this->attributes = attributes;
}


std::string StylesheetRule::toCSS()
{
    std::string css_string = "";
    unsigned short selector_amount = this->selectors.size();
    for (unsigned short i = 0; i < selector_amount; i++) {
        if (i != 0) {
            css_string += ", ";
        }
        css_string += this->selectors[i];
    }

    css_string += "\r\n{\r\n";
    css_string += this->attributes.toCSS();
    css_string += "}\r\n";

    return css_string;
}
