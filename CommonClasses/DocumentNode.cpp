/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "DocumentNode.h"


#include <algorithm>
#include <iostream>
#include <vector>
#include <string>


unsigned long long int DocumentNode::next_id = 0;


unsigned long long int DocumentNode::getNodeID()
{
    unsigned long long int id = DocumentNode::next_id;
    DocumentNode::next_id++;
    return id;
}



void DocumentNode::traverseNext(bool only_tags)
{
    //WARNING: Only tag traversal isn't fully implemented yet!

    if (this->tree_traversal_position.expired()) {
        //There is nothing to be searched.
        return;
    }

    auto node = this->tree_traversal_position.lock();
    if (node == this->shared_from_this()) {
        //We have reached the end of the traversal.
        return;
    }

    if (!node->children.empty()) {
        for (auto c: node->children) {
            if (c != nullptr) {
                if ((only_tags && (c->type == DocumentNode::Type::NODE))
                    || !only_tags) {
                    this->tree_traversal_position = c;
                    return;
                }
            }
        }
        //All children are null pointers or they are text nodes
        //in case we look only for tag nodes.
        //We do not traverse to siblings.
    }
}


DocumentNode::DocumentNode(const std::string& name, const Type type)
    : name(name),
      type(type)
{
    this->node_id = DocumentNode::getNodeID();
}


DocumentNode::DocumentNode(const DocumentNode& other)
{
    this->node_id = DocumentNode::getNodeID();
    this->name = other.name;
    this->type = other.type;

    this->parent_node = other.parent_node;

    //children are not copied: The constructor does only
    //a copy of one node. To copy a node hierarchy, the cloneNode
    //method must be used.

    this->attributes = other.attributes;

    this->style = other.style;
}


DocumentNode::DocumentNode(DocumentNode&& other)
{
    this->node_id = other.node_id;
    this->name = other.name;
    this->type = other.type;
    this->next = other.next;
    this->previous = other.previous;
    if (!this->next.expired()) {
        if (auto this_next = this->next.lock()) {
            this_next->previous = this->shared_from_this();
        }
    }
    if (!this->previous.expired()) {
        if (auto this_previous = this->previous.lock()) {
            this_previous->next = this->shared_from_this();
        }
    }
    this->children = std::move(other.children);
    for (auto child: this->children) {
        if (child != nullptr) {
            child->setParentNode(this->shared_from_this());
        }
    }
    this->attributes = std::move(other.attributes);
    this->style = std::move(other.style);
    this->parent_node = other.parent_node;
}


DocumentNode::~DocumentNode()
{
    //the next and previous pointers have to be corrected:
    auto previous_node = this->getPreviousNode();
    auto next_node = this->getNextNode();
    if (previous_node != nullptr) {
        if (next_node != nullptr) {
            //$this has a predecessor and a successor
            previous_node->setNextNode(next_node);
            next_node->setPreviousNode(previous_node);
        } else {
            //$this has a predecessor but no successor
            previous_node->setNextNode(nullptr);
        }
    } else {
        if (next_node != nullptr) {
            //$this has no predecessor but a successor
            next_node->setPreviousNode(nullptr);
        }
    }
    this->previous.reset();
    this->next.reset();
    this->parent_node.reset();

    //It may seem a bit weird why the removeChild method isn't called in the
    //destructor. The reason behind this is that a DocumentNode is either
    //referenced as a "root node" so that it (and all its child nodes) can
    //just be destroyed or it is a child node of another node. In the latter
    //case, removeChild must be called which already removes the child
    //from the children vector of the parent and thereby destroys the
    //shared_ptr which calls the destructor. Without a shared_ptr referencing
    //the node, the necessary call to shared_from_this will fail with
    //bad_weak_ptr.

    this->children.clear();
    this->attributes.clear();
    this->tree_traversal_position.reset();
}


DocumentNode& DocumentNode::operator=(const DocumentNode& other)
{
    this->name = other.name;
    this->type = other.type;

    for (auto child: other.children) {
        auto cloned_child = child->cloneNode(true);
        this->appendChild(cloned_child);
    }
    for (auto attribute: other.attributes) {
        this->attributes[attribute.first] = attribute.second;
    }
    this->style = other.style;
    return *this;
}


DocumentNode& DocumentNode::operator=(DocumentNode&& other)
{
    this->node_id = other.node_id;
    this->name = other.name;
    this->type = other.type;
    this->next = other.next;
    this->previous = other.previous;
    if (!this->next.expired()) {
        if (auto this_next = this->next.lock()) {
            this_next->previous = this->shared_from_this();
        }
    }
    if (!this->previous.expired()) {
        if (auto this_previous = this->previous.lock()) {
            this_previous->next = this->shared_from_this();
        }
    }
    //this->tree_traversal_position = other.tree_traversal_position;

    this->children = std::move(other.children);
        for (auto child: this->children) {
        if (child != nullptr) {
            child->setParentNode(this->shared_from_this());
        }
    }

    this->attributes = std::move(other.attributes);
    this->style = std::move(other.style);
    this->parent_node = other.parent_node;

    return *this;
}


std::string DocumentNode::toJSON()
{
    std::string json = "{";

    json += "\"name\":\"" + this->name + "\",";
    json += "\"type\":\"";
    switch (this->type) {
        case Type::NODE: {
            json += "NODE";
            break;
        }
        case Type::TEXT: {
            json += "TEXT";
            break;
        }
        case Type::CONTROL: {
            json += "CONTROL";
            break;
        }
        default: {
            json += "NONE";
        }
    }
    json += "\",";

    json += "\"next\":";
    if (!this->next.expired()) {
        if (auto next_node = this->next.lock()) {
            json += next_node->toJSON();
        } else {
            json += "null";
        }
    } else {
        json += "null";
    }
    json += ",";

    json += "\"parent_node\":";
    if (!this->parent_node.expired()) {
        if (auto parent = this->parent_node.lock()) {
            //If toJSON would be called in the parent node
            //all partial trees would be written once again
            //bottom up so that the resulting string could become
            //very large. Therefore only the node name of the parent
            //is included.
            json += "\""+ parent->name + "\"";
        } else {
            json += "null";
        }
    } else {
        json += "null";
    }

    json += ",\"children\":[";

    for (unsigned int i = 0; i<this->children.size(); i++) {
        json += this->children[i]->toJSON();
        if (i != this->children.size()-1) {
             //As long as it isn't the last child
             //add a comma to the list of childen.
            json += ",";
        }
    }

    json += "]}";
    return json;
}


std::string DocumentNode::toTree(unsigned short layer)
{
    std::string tree = "";
    for (unsigned short i = layer; i > 0; i--) {
        tree += " ";
    }
    tree += "@["+this->name + "](";

    for (auto attribute: this->attributes) {
        tree += attribute.first + "=" + attribute.second + "; ";
    }
    tree += ")\n";

    for (auto child: this->children) {
        tree += child->toTree(layer+1);
    }
    return tree;
}


std::string DocumentNode::toXML()
{
    std::string output = "";
    if (this->type == DocumentNode::Type::NODE) {
        output = "<" + this->name;
        for (auto attribute: this->attributes) {
            output += " " + attribute.first + "=\"" + attribute.second + "\"";
        }
        output += ">";
        for (auto child: this->children) {
            output += child->toXML();
        }
        output += "</" + this->name + ">";
    } else if (this->type == DocumentNode::Type::CONTROL) {
        //Control nodes are not printed, but their children:
        for (auto child: this->children) {
            output += child->toXML();
        }
    } else if (this->type == DocumentNode::Type::TEXT) {
        output = this->name;
    }
    return output;
}


///////////////
// DOM methods:
///////////////


std::shared_ptr<DocumentNode> DocumentNode::getElementById(std::string id)
{
    this->tree_traversal_position = this->shared_from_this();
    while (!this->tree_traversal_position.expired()) {
        auto node = this->tree_traversal_position.lock();
        for (auto attribute: node->attributes) {
            if (attribute.first == "id") {
                if (attribute.second == id) {
                    //We have found the right node
                    //and can return it here.
                    this->tree_traversal_position.reset();
                    return node;
                }
            }
        }
        this->traverseNext();
    }

    //No node with the specified ID found.
    //this->tree_traversal_position.reset();
    return nullptr;
}


std::vector<std::shared_ptr<DocumentNode>> DocumentNode::getElementsByTagName(
    std::string name
    )
{
    std::vector<std::shared_ptr<DocumentNode>> results;

    this->tree_traversal_position = this->shared_from_this();
    while (!this->tree_traversal_position.expired()) {
        auto node = this->tree_traversal_position.lock();
        if ((node->name == name)
            && (node->type == DocumentNode::Type::NODE)) {
            //We have found a match.
            results.push_back(node);
        }
        this->traverseNext();
    }
    this->tree_traversal_position.reset();

    return results;
}


std::vector<std::shared_ptr<DocumentNode>> DocumentNode::getElementsByClassName(
    std::string name
    )
{
    std::vector<std::shared_ptr<DocumentNode>> results;

    this->tree_traversal_position = this->shared_from_this();
    while (!this->tree_traversal_position.expired()) {
        auto node = this->tree_traversal_position.lock();
        for (auto attribute: node->attributes) {
            if (attribute.first == "class") {
                //Check if the class attribute contains the exact class
                //we are looking after:
                if (attribute.second.find(" " + name + " ") != std::string::npos) {
                    results.push_back(node);
                    break;
                }
            }
        }
        this->traverseNext();
    }
    this->tree_traversal_position.reset();

    return results;
}


std::shared_ptr<DocumentNode> DocumentNode::querySelector(std::string selector)
{
    //TODO
    return nullptr;
}


std::vector<std::shared_ptr<DocumentNode>> DocumentNode::querySelectorAll(
    std::string selector
    )
{
    std::vector<std::shared_ptr<DocumentNode>> results;

    //TODO

    return results;
}


const unsigned long long int DocumentNode::returnNodeID() const
{
  return this->node_id;
}


std::shared_ptr<DocumentNode> DocumentNode::getParentNode()
{
    if (this->parent_node.expired()) {
        return nullptr;
    }
    if (auto parent = this->parent_node.lock()) {
        return parent;
    }
    return nullptr;
}


void DocumentNode::setParentNode(std::shared_ptr<DocumentNode> new_parent)
{
    //Check if the parent still exists. If so, remove this child
    //from the parent's child list:
    if (!this->parent_node.expired()) {
        if (auto old_parent = this->parent_node.lock()) {
            old_parent->removeChild(this->shared_from_this());
        }
    }
    this->parent_node = new_parent;
}


std::shared_ptr<DocumentNode> DocumentNode::getPreviousNode()
{
    if (this->previous.expired()) {
        return nullptr;
    }
    if (auto prev = this->previous.lock()) {
        return prev;
    }
    return nullptr;
}


void DocumentNode::setPreviousNode(std::shared_ptr<DocumentNode> new_previous)
{
    //Check if the old previous node still exists. If so, remove its link
    //to this node and link the new previous node to this node.
    if (!this->previous.expired()) {
        if (auto old_previous = this->previous.lock()) {
            old_previous->next.reset();
        }
    }
    this->previous = new_previous;
    if (new_previous != nullptr) {
        new_previous->next = this->shared_from_this();
    }
}


std::shared_ptr<DocumentNode> DocumentNode::getNextNode()
{
    if (this->next.expired()) {
        return nullptr;
    }
    if (auto next_node = this->next.lock()) {
        return next_node;
    }
    return nullptr;
}


void DocumentNode::setNextNode(std::shared_ptr<DocumentNode> new_next)
{
    //Check if the next node still exists. If so, remove the link
    //to this node and link to the new following node.
    if (!this->next.expired()) {
        if (auto old_next = this->next.lock()) {
            old_next->previous.reset();
        }
    }
    this->next = new_next;
    if (new_next != nullptr) {
        new_next->previous = this->shared_from_this();
    }
}


void DocumentNode::appendChild(const std::string& child_name, const Type& type)
{
    if (child_name == "") {
        //Empty node: Nothing to do.
        return;
    }
    auto new_child = std::make_shared<DocumentNode>(
        child_name,
        type
        );
    this->appendChild(new_child);
}


void DocumentNode::appendChild(std::shared_ptr<DocumentNode> child)
{
    if (child != nullptr) {
        //Circular reference prevention:
        //Check if the child node is this node or if it is the parent
        //of one of this nodes parents:
        std::weak_ptr<DocumentNode> parent = this->shared_from_this();
        while (!parent.expired()) {
            auto shared_parent = parent.lock();
            if (shared_parent == child) {
                throw MNE::Exception(
                    "DocumentNode",
                    "Circular node hierarchy attempt in appendChild method!",
                    1
                    );
            }
            if (parent.lock() == shared_parent->parent_node.lock()) {
                //We have reached the top node.
                break;
            }
            parent = shared_parent->parent_node;
        }
        child->setParentNode(this->shared_from_this());
        if (this->hasChildNodes()) {
            std::shared_ptr<DocumentNode> last_child = this->children.back();
            child->setPreviousNode(last_child);
            last_child->setNextNode(child);
        } else {
            //first child
            child->setPreviousNode(nullptr);
            child->setNextNode(nullptr);
        }
        this->children.push_back(child);
    }
}


std::shared_ptr<DocumentNode> DocumentNode::getFirstChild()
{
    if (this->children.empty()) {
        return nullptr;
    }

    return this->children.front();
}


//The AddSibling functions will add nodes without adding them to the parent's children vector.
//Because of this the AddSibling functions are disabled.

/*
void DocumentNode::AddSibling(string SiblingName, unsigned char Type)
{
  
  DocumentNode* NewSibling = new DocumentNode(SiblingName, Type);
  NewSibling->Parent = this->Parent;
  
  NewSibling->Next = this->Next;
  NewSibling->Previous = this;
  
  this->Next = NewSibling;
    
}

void DocumentNode::AddSibling(DocumentNode *NewSibling)
{
  if(NewSibling != NULL)
  {
    NewSibling->Parent = this->Parent;
    
    NewSibling->Next = this->Next;
    NewSibling->Previous = this;
    
    this->Next = NewSibling;
  }
}
*/

void DocumentNode::addAttribute(
    const std::string& attribute_name,
    const std::string& attribute_value)
{
    this->setAttribute(attribute_name, attribute_value);
}


void DocumentNode::removeChildsBySelector(const std::string& selector)
{

}



std::shared_ptr<DocumentNode> DocumentNode::getLastChild()
{
  return this->children.back();
}


std::shared_ptr<DocumentNode> DocumentNode::cloneNode(
    bool deep
    )
{
    auto clone = std::make_shared<DocumentNode>(
        this->name,
        this->type
        );

    if (deep) {
        for (auto child: this->children) {
            std::shared_ptr<DocumentNode> cloned_child = child->cloneNode(deep);
            clone->appendChild(cloned_child);
        }
    }

    for (auto attribute: this->attributes) {
        clone->attributes[attribute.first] = attribute.second;
    }

#ifdef DOCUMENTNODE_DEBUG
    std::cerr << "Cloned [" << this->name << "] with "
              << this->children.size() << " child nodes" << std::endl;
    std::cerr << "Clone  [" << clone->name << "] has  "
              << clone->children.size() << " child nodes" << std::endl;
#endif
    return clone;
}


bool DocumentNode::hasChildNodes()
{
    return this->children.size() > 0;
}


bool DocumentNode::hasAttribute(const std::string& name) const
{
    if (name.length() < 1) {
        return false;
    }
    auto it = this->attributes.find(name);
    return it != this->attributes.end();
}


void DocumentNode::setAttribute(const std::string& name, const std::string& value)
{
    if (name.length() > 0) {
        this->attributes[name] = value;
    }
}


std::string DocumentNode::getAttribute(const std::string& name)
{
    if (name.length() < 1) {
        return "";
    }
    auto it = this->attributes.find(name);
    if (it == this->attributes.end()) {
        return "";
    }
    return it->second;
}


bool DocumentNode::isSameNode(std::shared_ptr<DocumentNode> other_node)
{
    return this == other_node.get();
}


void DocumentNode::normalize()
{
    if (this->children.empty()) {
        return;
    }

    //Store the first text node pointer of a sequence of text nodes.
    //The text of the following text nodes is appended to it and the
    //other text nodes are marked for deletion.
    std::shared_ptr<DocumentNode> current_sequence_first = nullptr;
    std::vector<std::shared_ptr<DocumentNode>> nodes_to_be_deleted;

    for (auto child: this->children) {
        if (child->type == Type::TEXT) {
            if (current_sequence_first == nullptr) {
                //The current child is the first text node encountered
                //(possibly after a node with another type). It is therefore
                //the first relevant node for a sequence.
                current_sequence_first = child;
            } else {
                //The current child is not the first text node in a sequence of
                //text nodes. Its content is appended to the first text node of
                //the sequence and the child is marked for deletion.
                current_sequence_first->name += child->name;
                nodes_to_be_deleted.push_back(child);
            }
        } else {
            //The current child is not a text node. In case the
            //current_sequence_first pointer is pointing to a text node,
            //it has to be reset since the sequence of text nodes is ended
            //by the current child.
            if (current_sequence_first != nullptr) {
                current_sequence_first = nullptr;
            }
        }
    }

    //After we went through all child nodes, it is time to delete the text nodes
    //that have been marked for deletion.
    for (auto node: nodes_to_be_deleted) {
        this->removeChild(node);
    }
}


void DocumentNode::normalise()
{
    this->normalize();
}


std::shared_ptr<DocumentNode> DocumentNode::insertBefore(
    std::shared_ptr<DocumentNode> new_node,
    std::shared_ptr<DocumentNode> reference_node
    )
{
    if (new_node == nullptr) {
        return nullptr;
    }

    if (reference_node != nullptr) {
        auto reference_parent_node = reference_node->getParentNode();
        if (reference_parent_node != shared_from_this()) {
            //This node is not the parent of reference_node.
            return nullptr;
        }
    }

    //decouple new_node from its current position, if it already is
    //somewhere inside the DOM:
    auto new_node_parent = new_node->getParentNode();

    if (reference_node == nullptr) {
        if (new_node_parent != nullptr) {
            new_node_parent->removeChild(new_node);
        }
        this->appendChild(new_node);
        return new_node;
    } else {
        auto reference_node_it = std::find(
            this->children.begin(),
            this->children.end(),
            reference_node
            );
        if (reference_node_it == this->children.end()) {
            //reference_node is invalid: It has this node as parent,
            //but this node doesn't know about it.
            return nullptr;
        } else {
            if (new_node_parent != nullptr) {
                new_node_parent->removeChild(new_node);
            }
            if (reference_node_it == this->children.begin()) {
                //reference_node is the first child.
                this->children.insert(reference_node_it, new_node);
                new_node->previous.reset();
                new_node->next = reference_node;
                reference_node->previous = new_node;
            } else {
                //reference_node is either the last child or a child
                //somewhere in the middle.
                auto node_before_reference_it = std::prev(reference_node_it);
                this->children.insert(reference_node_it, new_node);
                new_node->previous = *node_before_reference_it;
                new_node->next = reference_node;
                reference_node->previous = new_node;
                (*node_before_reference_it)->next = new_node;
            }
            return new_node;
        }
    }
}


std::shared_ptr<DocumentNode> DocumentNode::removeChild(
    std::shared_ptr<DocumentNode> child
    )
{
    if (child == nullptr) {
        return nullptr;
    }

    auto child_parent_node = child->getParentNode();
    if (child_parent_node != shared_from_this()) {
        //The node referenced by $child is not a child of this node.
        return nullptr;
    }

    auto child_it = std::find(
        this->children.begin(),
        this->children.end(),
        child
        );
    if (child_it == this->children.end()) {
        //$child is invalid: It has this node as parent node, but this
        //node doesn't know about it.
        return nullptr;
    }

    if (child_it == this->children.begin()) {
        //The first child is to be removed.
        auto new_first_child_it = this->children.begin();
        (*new_first_child_it)->previous.reset();
    } else {
        //A child that is not the first child is to be removed.
        auto child_before = std::prev(child_it);
        auto child_after = std::next(child_it);
        if (child_after == this->children.end()) {
            //The last child has been removed.
            (*child_before)->next.reset();
        } else {
            (*child_before)->next = *child_after;
            (*child_after)->previous = *child_before;
        }
    }
    this->children.erase(child_it);
    child->setParentNode(nullptr);
    child->next.reset();
    child->previous.reset();
    return child;
}


std::shared_ptr<DocumentNode> DocumentNode::replaceChild(
    std::shared_ptr<DocumentNode> new_child,
    std::shared_ptr<DocumentNode> old_child
    )
{
    if (this->insertBefore(new_child, old_child) == nullptr) {
        return nullptr;
    }
    return this->removeChild(old_child);
}
