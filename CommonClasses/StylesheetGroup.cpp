/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "StylesheetGroup.h"


StylesheetGroup::StylesheetGroup()
{
    this->group_type = 0;
    this->name = "";
}


StylesheetGroup::StylesheetGroup(const unsigned char new_group_type, const std::string& new_name)
{
    this->group_type = new_group_type;
    this->name = new_name;
}


StylesheetGroup::~StylesheetGroup()
{
    this->rules.clear();
}


void StylesheetGroup::addRule(const std::vector<std::string>& selectors, const VisualAttributes& attributes)
{
    auto new_rule = std::make_shared<StylesheetRule>(
        selectors,
        attributes
        );
    if (new_rule != nullptr) {
        this->rules.push_back(new_rule);
    }
}


std::string StylesheetGroup::toCSS()
{
    std::string result = "@media " + this->name + "{\n";
    for (auto rule: this->rules) {
        result += rule->toCSS();
    }
    result += "\n}\n";

    return result;
}


std::shared_ptr<StylesheetGroup> StylesheetGroup::copy()
{
    auto group_copy = std::make_shared<StylesheetGroup>(
        this->group_type,
        this->name
        );

    //Copy the stylesheet rules as well:
    for (auto rule: this->rules) {
        group_copy->rules.push_back(rule->copy());
    }
    return group_copy;
}
