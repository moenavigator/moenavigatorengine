/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2017  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <string>



/**
 * The MarkupElement structure helps defining elements of a markup language.
 * For HTML this class holds the definition of an HTML element,
 * including flags which define if the element has an opening
 * and/or a closing tag or if children are allowed.
 */
struct MarkupElement
{
    enum class Property
    {
        OPTIONAL, //The property is optional in the context.
        NECESSARY, //The property is necessary in the context.
        FORBIDDEN //The property is forbidden in the context.
    };

    /**
     * The name of the element.
     */
    std::string name;

    /**
     * The opening tag definition of the element.
     */
    Property opening_tag_prop;

    /**
     * The closing tag definition of the element.
     */
    Property closing_tag_prop;

    /**
     * Defines if child elements are allowed for the element.
     */
    Property children_allowed_prop;

    MarkupElement(
        std::string name,
        Property opening_tag_prop,
        Property closing_tag_prop,
        Property children_allowed_prop
        ) {
        this->name = name;
        this->opening_tag_prop = opening_tag_prop;
        this->closing_tag_prop = closing_tag_prop;
        this->children_allowed_prop = children_allowed_prop;
    }
};
