/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef PLAINTEXTPARSER_H
#define PLAINTEXTPARSER_H


#include "MarkupParser.h"


/**
 * The PlaintextParser class is responsible for "parsing" plain text files.
 * This means reading all the text and putting it into a DOM text node that
 * is inserted into a DOM node named "PlainText".
 */
class PlaintextParser : public MarkupParser
{
    protected:


    /**
     * The text attribute contains all the plain text of the document
     * when the document has been read.
     */
    std::string text;


    public:


    /**
     * @see MarkupParser::reset
     */
    virtual void reset() override;


    /**
     * The data is appended to a string, no real parsing involved.
     *
     * For a general description, @see MarkupParser::parseData
     */
    virtual void parseData(const std::vector<uint8_t>& data) override;


    /**
     * The node tree returned by PlaintextParser is always a DOM node called
     * "PlainText", containing one text node with all the text of the document.
     *
     * For a general description, @see MarkupParser::returnNodeTree
     */
    virtual std::shared_ptr<DocumentNode> returnNodeTree() override;
};


#endif
