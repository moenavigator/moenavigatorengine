/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MARKUPPARSER_H
#define MARKUPPARSER_H


#include "../Exceptions/MarkupParserException.h"
#include "../CommonClasses/DocumentNode.h"


#include <cstdint>
#include <memory>
#include <iostream>
#include <string>
#include <vector>


//Markup-Types:
#define MARKUP_NONODE DOCUMENTNODE_UNDEFINED
#define MARKUP_ELEMENT DOCUMENTNODE_TAG
#define MARKUP_ATTRIBUTE DOCUMENTNODE_ATTRIBUTE
#define MARKUP_TEXT DOCUMENTNODE_TEXT
#define MARKUP_COMMENT DOCUMENTNODE_COMMENT




class MarkupParser
{
    public:


    /**
     * Resets the parser to the state it is upon initialisation.
     */
    virtual void reset() = 0;


    /**
     * parseData is a method for parsing (binary) data. This method
     * should be implemented in a way that it accepts source data
     * splitted into several "data packets".
     */
    virtual void parseData(const std::vector<uint8_t>& data) = 0;


    /**
     * returnNodeTree returns the tree of document nodes
     * that have been parsed by the parser until now.
     */
    virtual std::shared_ptr<DocumentNode> returnNodeTree() = 0;


    /**
     * The default virtual destructor.
     */
    virtual ~MarkupParser() = default;
};


#endif
