/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "GopherMenuParser.h"


GopherMenuParser::GopherMenuParser()
{
    this->reset();
}


GopherMenuParser::~GopherMenuParser()
{
    //Nothing at the moment.
}


void GopherMenuParser::lineToNode()
{
    //We have to split the line by tab characters to construct hyperlinks
    //and informative text lines from it.
    std::vector<std::string> line_parts;
    std::string current_part;
    std::istringstream line(this->current_line);
    while (std::getline(line, current_part, '\t')) {
        line_parts.push_back(current_part);
    }
    //line_parts should contain four parts here.
    if (line_parts.size() < 4) {
        //The line is non-standard. Ignore it.
        return;
    }

    //We made sure in the size check above that four elements
    //are in the line_parts array, so we don't need to catch
    //std::out_of_range when accessing line_parts items below.

    if ((this->current_item_type == 'i') && (this->last_item_type == 'i')
        && (this->current_node != nullptr)) {
        this->current_node->name += line_parts.at(0);
        this->current_node->name += "\r\n";
    } else if ((this->current_item_type == 'i') || (this->current_item_type == '3')) {
        //The current line contains informative text or an error message.
        //Create a new node and append a text node below it.
        std::string node_name;
        node_name += this->current_item_type;
        this->current_node = std::make_shared<DocumentNode>(
            node_name,
            DocumentNode::Type::NODE
            );
        this->document->appendChild(this->current_node);
        auto text_node = std::make_shared<DocumentNode>(
            line_parts.at(0),
            DocumentNode::Type::TEXT
            );
        text_node->name += "\r\n";
        this->current_node->appendChild(text_node);
        this->current_node = text_node;
    } else {
        //The current line contains a link.
        //Construct the url, then construct a new node with an href attribute
        //and the display string as text content.
        std::string url = "gopher://" + line_parts.at(2) + ":" + line_parts.at(3);
        std::string path = line_parts.at(1);
        if (path.length() > 0) {
            if (path[0] != '/') {
                path = "/" + path;
            }
        }
        url += "/";
        url.push_back(this->current_item_type);
        url += path;

        this->current_node = std::make_shared<DocumentNode>("link", DocumentNode::Type::NODE);
        this->current_node->setAttribute("href", url);
        this->document->appendChild(this->current_node);
        auto text_node = std::make_shared<DocumentNode>(line_parts.at(0), DocumentNode::Type::TEXT);
        this->current_node->appendChild(text_node);
    }
}


void GopherMenuParser::reset()
{
    this->last_char = 0;
    this->last_item_type = 0;
    this->current_item_type = 0;
    this->current_line = "";
    this->read_item_type = true;
    this->document = nullptr;
    this->current_node = nullptr;
}


void GopherMenuParser::parseData(const std::vector<uint8_t>& data)
{
    if (data.empty()) {
        //Nothing to do.
        return;
    }
    if (this->document == nullptr) {
        //Initialise the document node.
        this->document = std::make_shared<DocumentNode>("menu", DocumentNode::Type::NODE);
    }

    //Start reading data:
    for (auto c: data) {
        if ((this->last_char == 13) && (c == 10)) {
            //A line break has been read. Put the content of the current line
            //in a node.
            this->lineToNode();
            this->current_line = "";
            this->read_item_type = true;
        } else if (this->read_item_type) {
            //Read the item type.
            this->last_item_type = this->current_item_type;
            this->current_item_type = c;
            this->read_item_type = false;
        } else if (c != 13) {
            //More or less normal text. Add it to the current line string.
            this->current_line += c;
        }

        this->last_char = c;
    }
}


std::shared_ptr<DocumentNode> GopherMenuParser::returnNodeTree()
{
    return this->document;
}
