/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GOPHERMENUPARSER_H
#define GOPHERMENUPARSER_H


#include "MarkupParser.h"


class GopherMenuParser: public MarkupParser
{
    protected:


    /**
     * The last read character. This is needed to detect line breaks.
     */
    char last_char = 0;


    /**
     * The item type of the last line.
     * This is needed to detect "paragraphs" of info lines.
     */
    char last_item_type = 0;


    /**
     * The item type of the current line.
     */
    char current_item_type = 0;


    /**
     * The current line of text that is being read.
     */
    std::string current_line;


    /**
     * Whether the next character to be read is the item type.
     */
    bool read_item_type = true;


    /**
     * The DOM document that has been parsed.
     */
    std::shared_ptr<DocumentNode> document = nullptr;


    /**
     * The current DOM node that is being created / modified
     * while parsing.
     */
    std::shared_ptr<DocumentNode> current_node = nullptr;


    /**
     * This is a helper method to put the content of the current line
     * in a node. Depending on the current item type and maybe the last
     * item type, a new node is created or the current line is appended
     * to the current node.
     *
     * Note that this method does not change the content of current_line,
     * current_item_type and last_item_type.
     */
    virtual void lineToNode();


    public:


    GopherMenuParser();


    ~GopherMenuParser();


    /**
     * @see MarkupParser::reset
     */
    virtual void reset() override;


    /**
     * @see MarkupParser::parseData
     */
    virtual void parseData(const std::vector<uint8_t>& data) override;


    /**
     * @see MarkupParser::returnNodeTree
     */
    virtual std::shared_ptr<DocumentNode> returnNodeTree() override;
};


#endif
