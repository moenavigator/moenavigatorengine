/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2019  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MARKDOWNPARSER_H
#define MARKDOWNPARSER_H

#include "MarkupParser.h"


class MarkdownParser: public MarkupParser
{
    protected:

    enum class State {
        //TODO: complete the state enum!
        NONE,
        //Text:
        CONTENT,
        //Headings:
        H1,
        H2,
        H3,
        H4,
        H5,
        H6,
        //Code blocks:
        MAYBECODE1,
        MAYBECODE2,
        MAYBECODE3,
        CODE,
        PARAGRAPH,
        UNORDERED_LIST,
        ORDERED_LIST,
        LINK_TEXT,
        LINK_URL,
        IMAGE_TEXT,
        IMAGE_URL
    }

    State state;

    std::shared_ptr<DocumentNode> document;


    public:

    MarkdownParser();

    //MarkupParser interface implementation:

    virtual void parseData(std::vector<uint8_t> data);

    virtual shared_ptr<DocumentNode> returnNodeTree();
}


#endif
