/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2019  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "MarkdownParser.h"


MarkdownParser::MarkdownParser()
{
    this->state = State::NONE;
    this->document = nullptr;
}


void MarkdownParser::parseFromString(std::string code)
{
    char* last_chars[6] = "\0\0\0\0\0\0";

    //last_line_state holds the state of the last read line.
    //Knowing the state of the last line helps with reducing the
    //nodes in the DOM.
    State last_line_state = State::NONE;

    for (char current_char: code) {
        switch (this->state) {
            case State::NONE: {
                switch (current_char) {
                    case 0x23: {
                        //Read '#'
                        this->state = State::H1;
                        break;
                    } case 0x20: {
                        //Read ' ' (whitespace)
                        this->state = State::MAYBECODE1;
                        break;
                    } default: {
                        //Do nothing.
                        //Read '#'
                        this->state = State::H2;
                        break;
                    }
                }
                break;
            } case State::H2: {
                switch (current_char) {
                    case 0x23: {
                        //Read '#'
                        this->state = State::H3;
                        break;
                    }
                }
                break;
            } case State::H3: {
                switch (current_char) {
                    case 0x23: {
                        //Read '#'
                        this->state = State::H4;
                        break;
                    }
                }
                break;
            } case State::H4: {
                switch (current_char) {
                    case 0x23: {
                        //Read '#'
                        this->state = State::H5;
                        break;
                    }
                }
                break;
            } case State::H5: {
                switch (current_char) {
                    case 0x23: {
                        //Read '#'
                        this->state = State::H6;
                        break;
                    }
                }
                break;
            } case State::H6: {
                switch (current_char) {
                    case 0x23: {
                        //Read '#'
                        //There is no H7, so we assume it is content.
                        this->state = State::CONTENT;
                        break;
                    }
                }
                break;
            } case State::MAYBECODE1: {
                switch (current_char) {
                    case 0x20: {
                        //Read ' ' (whitespace)
                        this->state = State::MAYBECODE2;
                        break;
                    }
                }
                break;
            } case State::MAYBECODE2: {
                switch (current_char) {
                    case 0x20: {
                        //Read ' ' (whitespace)
                        this->state = State::MAYBECODE3;
                        break;
                    }
                }
                break;
            } case State::MAYBECODE3: {
                switch (current_char) {
                    case 0x20: {
                        //Read ' ' (whitespace)
                        this->state = State::CODE;
                        break;
                    }
                }
            } case state::CODE: {
                switch (current_char) {
                    case 0x10: {
                        //Read newline
                        last_line_state = this->state;
                        this->state = State::NONE;
                        significant_eol = true;
                        break;
                    }
                }
            } case State::CONTENT: {
                break;
            }
        }
        last_char = current_char;
    }
}


void MarkdownParser::parseData(std::vector<uint8_t> data)
{
    //TODO
}
