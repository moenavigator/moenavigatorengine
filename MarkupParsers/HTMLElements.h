/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2017  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <vector>
#include "MarkupElement.h"


//Source: http://www.w3.org/TR/html401/index/elements.html


static const MarkupElement HTML_401_ELEMENTS[] =
{
    //element name, start tag property, closing tag property, children property
    {"a", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"abbr", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"acronym", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"address", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    //DEPRECATED {"applet", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"area", MarkupElement::Property::NECESSARY, MarkupElement::Property::FORBIDDEN, MarkupElement::Property::FORBIDDEN},
    {"b", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"base", MarkupElement::Property::NECESSARY, MarkupElement::Property::FORBIDDEN, MarkupElement::Property::FORBIDDEN},
    //DEPRECATED {"basefont", MarkupElement::Property::NECESSARY, MarkupElement::Property::FORBIDDEN, MarkupElement::Property::FORBIDDEN},
    {"bdo", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"big", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"blockquote", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"body", MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"br", MarkupElement::Property::NECESSARY, MarkupElement::Property::FORBIDDEN, MarkupElement::Property::FORBIDDEN},
    {"button", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"caption", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    //DEPRECATED {"center", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"cite", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"code", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"col", MarkupElement::Property::NECESSARY, MarkupElement::Property::FORBIDDEN, MarkupElement::Property::FORBIDDEN},
    {"colgroup", MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"dd", MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"del", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"dfn", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    //DEPRECATED {"dir", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"div", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"dl", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"dt", MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"em", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"fieldset", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    //DEPRECATED {"font", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"form", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"frame", MarkupElement::Property::NECESSARY, MarkupElement::Property::FORBIDDEN, MarkupElement::Property::FORBIDDEN},
    {"frameset", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"h1", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"h2", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"h3", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"h4", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"h5", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"h6", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"head", MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"hr", MarkupElement::Property::NECESSARY, MarkupElement::Property::FORBIDDEN, MarkupElement::Property::FORBIDDEN},
    {"html", MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"i", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"iframe", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"img", MarkupElement::Property::NECESSARY, MarkupElement::Property::FORBIDDEN, MarkupElement::Property::FORBIDDEN},
    {"input", MarkupElement::Property::NECESSARY, MarkupElement::Property::FORBIDDEN, MarkupElement::Property::FORBIDDEN},
    {"ins", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    //DEPRECATED {"isindex", MarkupElement::Property::NECESSARY, MarkupElement::Property::FORBIDDEN, MarkupElement::Property::FORBIDDEN},
    {"kbd", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"label", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"legend", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"li", MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"link", MarkupElement::Property::NECESSARY, MarkupElement::Property::FORBIDDEN, MarkupElement::Property::FORBIDDEN},
    {"map", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    //DEPRECATED {"menu", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"meta", MarkupElement::Property::NECESSARY, MarkupElement::Property::FORBIDDEN, MarkupElement::Property::FORBIDDEN},
    {"noframes", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"noscript", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"object", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"ol", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"optgroup", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"option", MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"p", MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"param", MarkupElement::Property::NECESSARY, MarkupElement::Property::FORBIDDEN, MarkupElement::Property::FORBIDDEN},
    {"pre", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"q", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    //DEPRECATED {"s", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"samp", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"script", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"select", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"small", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"span", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    //DEPRECATED {"strike", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY},
    {"strong", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"style", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"sub", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"sup", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"table", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"tbody", MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"td", MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"textarea", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"tfoot", MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"th", MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"thead", MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"title", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"tr", MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL, MarkupElement::Property::OPTIONAL},
    {"tt", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    //DEPRECATED {"u", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"ul", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL},
    {"var", MarkupElement::Property::NECESSARY, MarkupElement::Property::NECESSARY, MarkupElement::Property::OPTIONAL}
};
