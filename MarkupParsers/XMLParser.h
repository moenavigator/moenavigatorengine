/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2019  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef XMLPARSER_H
#define XMLPARSER_H


#include <iostream>
#include <memory>

#include "MarkupParser.h"
#include "../CommonClasses/DocumentNode.h"




class XMLParser: public MarkupParser
{
    public:


    /**
     * An enum for parser states.
     */
    enum class State
    {
        NOTAG,  //Not inside a tag: read char
        TAGNAME, //Inside a tag name: read char
        CLOSINGTAG, //Inside a closing tag: read char
        ATTRIBUTE, //Inside an attribute: read char
        ATTRASSIGN, //"=" character between attribute name and value:
                    //skip char
        ATTRVALUEBAD, //Badly written attribute value: read char
        ATTRVALUE1, //Attribute value in single quotes: read char
        ATTRVALUE2, //Attribute value in double quotes: read char
        SGMLTAG, //Inside an SGML tag: read char

        PROLOG, //Inside a prolog: skip char

        COMMENT, //Inside a comment: skip char
        CLOSINGCOMMENT, //The end of a comment: skip char
        TAGSTART, //The start of a tag: skip char
        TAGCONTENT, //Whitespace inside a tag: skip char
        DIRECTCLOSEDTAG, //Inside the closing part of a directly closed tag:
                         //skip char
        CLOSINGDIRECTCLOSEDTAG //The end of a directly closed tag: skip char
    };

    /**
     * enum to distinguish the types of the node
     * to be created or the different kinds of content of
     * an existing node.
     */
    enum class NewNodeType
    {
        NONE, //Do not create anything new.
        NODE, //Create a new node.
        ATTR, //Create a new attribute in the current node.
        ATTRVAL, //Set the value for the current attribute.
        TEXT, //Create a new text node.
        SGML, //Create a new SGML control node.
        CLOSING //"Create" a closing node.
                //This means moving one layer up in the node tree.
    };


    XMLParser();


    ~XMLParser();


    /**
     * @see MarkupParser::reset
     */
    virtual void reset() override;


    /**
     * @see MarkupParser::parseData
     */
    virtual void parseData(const std::vector<uint8_t>& data) override;


    /**
     * @see MarkupParser::returnNodeTree
     */
    virtual std::shared_ptr<DocumentNode> returnNodeTree() override;


    protected:


    std::shared_ptr<DocumentNode> node_tree;


    std::shared_ptr<DocumentNode> first_node_ptr;


    std::shared_ptr<DocumentNode> current_node_ptr;


    State current_state;


    State last_state;


    /**
     * The last three read characters in the order:
     * [2] [1] [0] [current character]
     */
    uint8_t last_chars[3];


    /**
     * If this attribute is set to false
     * the current character is skipped (not parsed).
     */
    bool read_current_char;


    /**
     * Represents the type of the read node.
     */
    NewNodeType new_node_type;


    /**
     * Holds the current line number of the input.
     */
    uint32_t input_row_pos;


    /**
     * Holds the current column number of the input.
     */
    uint32_t input_column_pos;


    /**
     * Holds the name or content of a new node which has just
     * been parsed.
     */
    std::string new_node_name;


    /**
     * The name of the attribute that is currently read.
     * The attribute name must be saved temporarily to be able to use
     * the setAttribute method of the DocumentNode class which requires
     * the name and the value of an attribute at the same time.
     */
    std::string current_attribute_name;


    /**
     * Pauses reading the next character for one cycle if
     * set to true so that the current character is read twice.
     */
    bool pause_reading;
};


#endif
