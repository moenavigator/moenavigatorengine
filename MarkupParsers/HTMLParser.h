/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2019  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HTMLPARSER_H
#define HTMLPARSER_H

#include <iostream>
#include <bits/stdc++.h>
#include <cstdint>
#include <vector>

#include "MarkupParser.h"
#include "../CommonClasses/DocumentNode.h"
#include "HTMLElements.h"




class HTMLParser: public MarkupParser
{
    public:


    enum class State
    {
        NOTAG,  //Not inside a tag: read char
        TAGNAME, //Inside a tag name: read char
        CLOSINGTAG, //Inside a closing tag: read char
        ATTRIBUTE, //Inside an attribute: read char
        ATTRASSIGN, //"=" character between attribute name and value:
                    //skip char
        ATTRVALUEBAD, //Badly written attribute value: read char
        ATTRVALUE1, //Attribute value in single quotes: read char
        ATTRVALUE2, //Attribute value in double quotes: read char
        SGMLTAG, //Inside an SGML tag: read char

        COMMENT, //Inside a comment: skip char
        CLOSINGCOMMENT, //The end of a comment: skip char

        TAGSTART, //The start of a tag: skip char
        TAGCONTENT, //Whitespace inside a tag: skip char
        DIRECTCLOSEDTAG, //Inside the closing part of a directly closed tag:
                         //skip char
        CLOSINGDIRECTCLOSEDTAG //The end of a directly closed tag: skip char
    };

    enum class NewNodeType
    {
        NONE, //Do not create anythin new.
        NODE, //Create a new node.
        ATTR, //Create a new attribute in the current node.
        ATTRVAL, //Set the value for the current attribute.
        TEXT, //Create a new text node.
        SGML, //Create a new SGML control node.
        CLOSING //"Create" a closing node.
                //This means moving one layer up in the node tree.
    };


    /**
     * @see MarkupParser::reset
     */
    virtual void reset() override;


    virtual void parseData(const std::vector<uint8_t>& data) override;

    virtual std::shared_ptr<DocumentNode> returnNodeTree() override;


    HTMLParser();

    ~HTMLParser();


    protected:


    /**
     * The pointer to the first parsed node (the root node).
     */
    std::shared_ptr<DocumentNode> first_node_ptr;


    /**
     * The pointer to the currently parsed node.
     */
    std::shared_ptr<DocumentNode> current_node_ptr;


    State current_state;


    State last_state;


    /**
     * last_chars holds the last three read chars in the following order:
     * [2] [1] [0] [current character]
     */
    unsigned char last_chars[3];

    /**
     * read_current_char indicates if the current char is to be read
     * (true) or not to be read (false).
     */
    bool read_current_char;

    /**
     * new_node_type represents the type of the read node.
     */
    NewNodeType new_node_type;

    /**
     * The name or content of the new node.
     */
    std::string new_node_name;


    /**
     * The name of the attribute that is currently read.
     * The attribute name must be saved temporarily to be able to use
     * the setAttribute method of the DocumentNode class which requires
     * the name and the value of an attribute at the same time.
     */
    std::string current_attribute_name;

    /**
     * If last_tag_has_closing_tag is set to false
     * the parser jumps one layer up in the node tree
     * when reading a new node.
     */
    bool last_tag_has_closing_tag;

    /**
     * Holds the current line number of the input.
     */
    uint32_t input_row_pos;

    /**
     * Holds the current column number of the input.
     */
    uint32_t input_column_pos;

    /**
     * If set to true reading the next character is paused
     * for one cycle, reading the current character twice.
     */
    bool pause_reading;
};


#endif
