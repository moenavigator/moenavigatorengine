/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "HTMLParser.h"
#include "../CommonClasses/DocumentNode.h"

#include <iostream>
#include <string>
#include <stack>


HTMLParser::HTMLParser()
{
    this->reset();

    this->input_row_pos = 1;
    this->input_column_pos = 1;
}


HTMLParser::~HTMLParser()
{

}


void HTMLParser::reset()
{
    //initialize state-relevant variables:
    this->current_state = State::NOTAG;
    this->last_state = State::NOTAG;
    this->new_node_name = "";
    this->last_chars[0]='\0';
    this->last_chars[1]='\0';
    this->last_chars[2]='\0';

    this->read_current_char = true; //wird false, wenn keine relevanten Zeichen gelesen werden
    this->new_node_type = NewNodeType::NONE; //da beim Start kein Text gelesen wurde

    this->last_tag_has_closing_tag = true; //wenn auf false gesetzt ist, so wird beim Lesen eines neuen Knotens zuerst eine Ebene nach oben gesprungen.

    this->pause_reading = false; //pausiert einen Zyklus lang das Lesen des nächsten Zeichens.
                                //Bei true wird so ein Zeichen zweimal gelesen
}


void HTMLParser::parseData(const std::vector<uint8_t>& data)
{
    //Humans start counting at 1, so we start reading at column 1 and row 1
    //from their perspective:
    this->input_column_pos = 1;
    this->input_row_pos = 1;

    for (auto current_byte: data) {
        this->input_column_pos++;
        if (this->pause_reading == true) {
            this->pause_reading = false;
            continue;
        }

        //Phase 0: check for line breaks:
        if (!(current_byte != 0x0A && this->last_chars[0] != 0x0D)) {
            this->input_row_pos++;
            this->input_column_pos = 1;
        }

        //Phase 1: Scanner

        //First, we replace new line characters
        //(LF + VT + FF + CR + CRLF + Unicode NEL) with blank characters:

        if (((current_byte >= 0x0A) && (current_byte <= 0x0D)) ||
            (current_byte == 0x85) /* ||
           (this->last_chars[0] == 0x20 && (current_byte == 0x28 || current_byte == 0x29))*/ //this can not be detected properly since it is 16-Bit Unicode. Only 8-Bit unicode is supported at the moment!
            ) {
            current_byte = 0x20;
        }

        //Handle the current char differently according to the parser state:

        if (this->current_state == State::NOTAG) {
            if (current_byte == 0x3c) {
                //read '<'
                this->last_state = this->current_state;
                this->current_state = State::TAGSTART;

                //Avoid creating empty text nodes:
                if (this->new_node_name.length() != 0) {
                    this->new_node_type = NewNodeType::TEXT;
                }

                this->read_current_char = false;
            } else {
                //remove multiple blank spaces by not adding them
                //to the string of read characters:
                if (last_chars[0] == 0x20 && current_byte == 0x20) {
                    read_current_char = false;
                } else {
                    this->read_current_char = true;
                }
            }
        } else if (this->current_state == State::TAGSTART) {
            if ((current_byte > 0x40 && current_byte < 0x5b) ||
                (current_byte > 0x60 && current_byte < 0x7b) ||
                (current_byte > 0x2f && current_byte < 0x3a)
                ) {
                //read A-Z or a-z or 0-9
                this->last_state = this->current_state;
                this->current_state = State::TAGNAME;
                this->read_current_char = true;
            } else if (current_byte == 0x2f) {
                // read '/'
                this->last_state = this->current_state;
                this->current_state = State::CLOSINGTAG;
                this->read_current_char = false;
            } else if (current_byte == 0x21) {
                // read '!'
                this->last_state = this->current_state;
                this->current_state = State::SGMLTAG;
                this->read_current_char = false;
            }
        } else if (this->current_state == State::SGMLTAG) {
            if (current_byte == 0x2d) {
                // read '-'
                if (this->last_chars[0] == 0x2d) {
                    //previous character was '-': new comment
                    this->last_state = this->current_state;
                    this->current_state = State::COMMENT;
                    this->read_current_char = false;
                }
            } else if (current_byte == 0x3e) {
                // read '>'
                this->last_state = this->current_state;
                this->current_state = State::NOTAG;
                this->new_node_type = NewNodeType::TEXT;
                this->read_current_char = false;
            }
        } else if (this->current_state == State::TAGNAME) {
            if (current_byte == 0x2f) {
                // read '/'
                this->last_state = this->current_state;
                this->current_state = State::DIRECTCLOSEDTAG;
                this->read_current_char = false;
                this->new_node_type = NewNodeType::NODE;
            } else if (current_byte == 0x3e) {
                // read '>'
                this->last_state = this->current_state;
                this->current_state = State::NOTAG;
                this->new_node_type = NewNodeType::NODE;
                this->read_current_char = false;
            } else if (current_byte == 0x20) {
                // read blank character
                this->last_state = this->current_state;
                this->current_state = State::TAGCONTENT;
                this->read_current_char = false;
                this->new_node_type = NewNodeType::NODE;
            } else {
                this->read_current_char = true;
            }
        } else if (this->current_state == State::TAGCONTENT) {
            if (current_byte == 0x3e) {
                // read '>' gelesen
                this->last_state = this->current_state;
                this->current_state = State::NOTAG;
            } else if ((current_byte > 0x40 && current_byte < 0x5b) ||
                       (current_byte > 0x60 && current_byte < 0x7b) ||
                       (current_byte > 0x2f && current_byte < 0x3a)
                ) {
                //A-Z or a-z or 0-9
                this->last_state = this->current_state;
                this->current_state = State::ATTRIBUTE;
                this->read_current_char = true;
            }
        } else if (this->current_state == State::CLOSINGTAG) {
            if (current_byte == 0x3e) {
                // read '>'
                this->last_state = this->current_state;
                this->read_current_char = false;
                this->current_state = State::NOTAG;
                this->new_node_type = NewNodeType::CLOSING;
            } else {
                this->read_current_char = true;
            }
        } else if (this->current_state == State::DIRECTCLOSEDTAG) {
            if (current_byte == 0x3e) {
                // read '>'
                this->last_state = this->current_state;
                this->current_state = State::NOTAG;
                this->read_current_char = false;
                this->new_node_type = NewNodeType::CLOSING;
            }
        } else if (this->current_state == State::COMMENT) {
            if (current_byte == 0x3e &&
                this->last_chars[0] == 0x2d &&
                this->last_chars[1] == 0x2d) {
                // read sequence '-', '-', '>': end of comment
                this->last_state = this->current_state;
                this->current_state = State::CLOSINGCOMMENT;
                this->read_current_char = false;
                //Comments are not processed:
                this->new_node_name = "";
                this->new_node_type = NewNodeType::NONE;
            } else {
                this->read_current_char = true;
            }
        } else if (this->current_state == State::CLOSINGCOMMENT) {
            this->pause_reading = true;
            this->read_current_char = false;
            this->current_state = State::NOTAG;
            this->new_node_type = NewNodeType::NONE;
        } else if (this->current_state == State::ATTRIBUTE) {
            if (current_byte == 0x20) {
                //read blank character
                this->last_state = this->current_state;
                this->current_state = State::TAGCONTENT;
                this->read_current_char = false;
                this->new_node_type = NewNodeType::ATTR;
            } else if (current_byte == 0x3d) {
                //read '='
                this->last_state = this->current_state;
                this->current_state = State::ATTRASSIGN;
                this->read_current_char = false;
                this->new_node_type = NewNodeType::ATTR;
            } else if (current_byte == 0x3e) {
                // read '>'
                this->last_state = this->current_state;
                this->current_state = State::NOTAG;
                this->read_current_char = false;
                this->new_node_type = NewNodeType::ATTR;
            } else {
                this->read_current_char = true;
            }
        } else if (this->current_state == State::ATTRASSIGN) {
            if (current_byte == 0x22) {
                //read '"'
                this->last_state = this->current_state;
                this->read_current_char = false;
                this->current_state = State::ATTRVALUE2;
            } else if (current_byte == 0x27) {
                // read '
                this->last_state = this->current_state;
                this->read_current_char = false;
                this->current_state = State::ATTRVALUE1;
            } else if ((current_byte > 0x40 && current_byte < 0x5b) ||
                       (current_byte > 0x60 && current_byte < 0x7b) ||
                       (current_byte > 0x2f && current_byte < 0x3a)
                ) {
                //read A-Z or a-z or 0-9
                this->last_state = this->current_state;
                this->current_state = State::ATTRVALUEBAD;
                this->read_current_char = true;
            } else if (current_byte == 0x3e) {
                //read '>'
                this->last_state = this->current_state;
                this->current_state = State::NOTAG;
                this->new_node_type = NewNodeType::ATTR;
                this->read_current_char = false;
            }
        } else if (this->current_state == State::ATTRVALUE1) {
            if (current_byte == 0x27 &&
                this->last_chars[0] != 0x5c) {
                //read ', without a preceeding backslash
                this->last_state = this->current_state;
                this->current_state = State::TAGCONTENT;
                this->read_current_char = false;
                this->new_node_type = NewNodeType::ATTRVAL;
            } else {
                this->read_current_char = true;
            }
        } else if (this->current_state == State::ATTRVALUE2) {
            if (current_byte == 0x22 &&
                this->last_chars[0] != 0x5c) {
                //read '"', without a preceeding backslash
                this->last_state = this->current_state;
                this->current_state = State::TAGCONTENT;
                this->read_current_char = false;
                this->new_node_type = NewNodeType::ATTRVAL;
            } else {
                this->read_current_char = true;
            }
        } else if (this->current_state == State::ATTRVALUEBAD) {
            if (current_byte == 0x20) {
                //read whitespace
                this->last_state = this->current_state;
                this->current_state = State::TAGCONTENT;
                this->read_current_char = false;
                this->new_node_type = NewNodeType::ATTRVAL;
            } else if (current_byte == 0x3e) {
                //read '>'
                this->last_state = this->current_state;
                this->current_state = State::NOTAG;
                this->read_current_char = false;
                this->new_node_type = NewNodeType::ATTRVAL;
            } else {
                this->read_current_char = true;
            }
        }

        if (this->read_current_char == true) {
            this->new_node_name.push_back(current_byte);
        }

        //Phase 2: Parser

        if (this->new_node_type != NewNodeType::NONE) {
            if (this->last_tag_has_closing_tag == false &&
                (this->new_node_type == NewNodeType::NODE)) {
                //Move one layer up to avoid child elements
                //of a tag that has no closing tag.
                if (this->current_node_ptr != nullptr) {
                    auto current_parent = this->current_node_ptr->getParentNode();
                    if (current_parent != nullptr) {
                        this->current_node_ptr = current_parent;
                    }
                }
                this->last_tag_has_closing_tag = true;
            }

            if (this->new_node_type == NewNodeType::NODE) {
                //Convert the node name to lower case:
                std::transform(
                    this->new_node_name.begin(),
                    this->new_node_name.end(),
                    this->new_node_name.begin(),
                    ::tolower
                    );

                //Check if the tag is known to the parser:
                this->last_tag_has_closing_tag = true;
                for (MarkupElement el: HTML_401_ELEMENTS) {
                    if (el.name == this->new_node_name) {
                        //The element's name is known to the parser.
                        if (el.closing_tag_prop == MarkupElement::Property::FORBIDDEN) {
                            this->last_tag_has_closing_tag = false;
                        }

                        //Add a new child element and since we might
                        //process the child element further, we set it
                        //as the current node pointer.
                        if (this->first_node_ptr == nullptr) {
                            this->first_node_ptr = std::make_shared<DocumentNode>(
                                this->new_node_name,
                                DocumentNode::Type::NODE
                                );
                            this->first_node_ptr->origin.column = this->input_column_pos;
                            this->first_node_ptr->origin.line = this->input_row_pos;
                            this->current_node_ptr = this->first_node_ptr;
                        } else {
                            auto new_node = std::make_shared<DocumentNode>(
                                this->new_node_name,
                                DocumentNode::Type::NODE
                                );
                            this->current_node_ptr->appendChild(new_node);
                            this->current_node_ptr = new_node;
                            this->current_node_ptr->origin.column = this->input_column_pos;
                            this->current_node_ptr->origin.line = this->input_row_pos;
                        }

                        //No need to loop through the other elements
                        //if we have already found the element.
                        break;
                    }
                }
            } else if (this->new_node_type == NewNodeType::TEXT) {
                //Check if the text node contains anything else than
                //whitespace or control characters. Only create a new
                //text node when it contains more than just whitespace.
                bool only_whitespace = true;
                for (auto character: this->new_node_name) {
                    if (character > 0x20) {
                        only_whitespace = false;
                        break;
                        }
                }
                if (!only_whitespace) {
                    if (this->current_node_ptr != nullptr) {
                        auto new_child = std::make_shared<DocumentNode>(
                            this->new_node_name,
                            DocumentNode::Type::TEXT
                            );
                        this->current_node_ptr->appendChild(new_child);
                    }
                    //No else here: If a document starts with text instead
                    //of a doctype or a tag, it isn't valid and its text
                    //can be thrown away.
                }
            } else if (this->new_node_type == NewNodeType::ATTR) {
                this->current_attribute_name = this->new_node_name;
            } else if (this->new_node_type == NewNodeType::ATTRVAL) {
                if (this->current_node_ptr != nullptr && !this->current_attribute_name.empty()) {
                    this->current_node_ptr->setAttribute(this->current_attribute_name, this->new_node_name);
                    this->current_attribute_name = "";
                }
            } else if (this->new_node_type == NewNodeType::CLOSING) {
                if (this->current_node_ptr != nullptr) {
                    if (this->new_node_name == "") {
                        //Empty name: It is a direct closing tag (e.g. <br/>).
                        this->current_node_ptr = this->current_node_ptr->getParentNode();
                    } else {
                        if (this->new_node_name == this->current_node_ptr->name) {
                            //If the closing tag has the same name as the
                            //current node, we move one layer up.
                            //But before moving up, we must first check,
                            //if the current node is the root node
                            //or a nullptr:
                            if (this->current_node_ptr != nullptr) {
                                auto current_parent = this->current_node_ptr->getParentNode();
                                if (current_parent != nullptr) {
                                    this->current_node_ptr = current_parent;
                                }
                            }
                        } else {
                            //If the name of the closing tag differs
                            //from the name of the current node,
                            //we traverse the node tree upwards until we
                            //find a node that matches the name of the
                            //closing tag. If such a node is found, we make
                            //the layer of that node the current layer.
                            //If no such node is found, the current
                            //closing tag is discarded.
                            std::shared_ptr<DocumentNode> search_ptr = this->current_node_ptr;
                            unsigned short steps = 0;

                            //First we traverse the node hierarchy upwards.
                            std::shared_ptr<DocumentNode> old_search_ptr = nullptr;
                            while ((search_ptr != nullptr) && (old_search_ptr != search_ptr)) {
                                if (this->new_node_name == search_ptr->name) {
                                    break;
                                }
                                old_search_ptr = search_ptr;
                                search_ptr = search_ptr->getParentNode();
                                steps++;
                            }

                            if (search_ptr != nullptr) {
                                //A node has been found that has the same name
                                //as the current node. current_node_ptr has to
                                //point to the node that is $steps layers above
                                //its current position.
                                for (auto i = steps; i > 0; i--) {
                                    if ((this->current_node_ptr != this->first_node_ptr)
                                        && (this->current_node_ptr != nullptr)) {
                                        this->current_node_ptr = this->current_node_ptr->getParentNode();
                                    }
                                }
                            }
                            //No else part is placed here since at the end
                            //of the surrounding if-else-if-statement,
                            //$new_node_name is cleared.
                        }
                    }
                }
            }

            this->new_node_type = NewNodeType::NONE;
            this->new_node_name = "";
        }

        this->last_chars[2] = this->last_chars[1];
        this->last_chars[1] = this->last_chars[0];
        this->last_chars[0] = current_byte;
    }
}


std::shared_ptr<DocumentNode> HTMLParser::returnNodeTree()
{
    if (this->first_node_ptr != nullptr) {
        //return the node tree:
        return this->first_node_ptr->cloneNode();
    } else {
        //otherwise return null:
        return nullptr;
    }
}
