/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "PlaintextParser.h"


void PlaintextParser::reset()
{
    this->text = "";
}


void PlaintextParser::parseData(const std::vector<uint8_t>& data)
{
    if (data.empty()) {
        //Nothing to do.
        return;
    }
    this->text.append(data.begin(), data.end());
}


std::shared_ptr<DocumentNode> PlaintextParser::returnNodeTree()
{
    auto node = std::make_shared<DocumentNode>("PlainText", DocumentNode::Type::NODE);
    if (node == nullptr) {
        return nullptr;
    }
    node->appendChild(this->text, DocumentNode::Type::TEXT);
    return node;
}
