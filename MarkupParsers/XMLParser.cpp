/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2019  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "XMLParser.h"




XMLParser::XMLParser()
{
    this->reset();

    this->input_row_pos = 1;
    this->input_column_pos = 1;
}


XMLParser::~XMLParser()
{

}


void XMLParser::reset()
{
    //Initialise state-relevant variables:
    this->current_state = State::NOTAG;
    this->last_state = State::NOTAG;
    this->new_node_name = "";
    this->last_chars[0]='\0';
    this->last_chars[1]='\0';
    this->last_chars[2]='\0';

    this->read_current_char = true; //wird false, wenn keine relevanten Zeichen gelesen werden
    this->new_node_type = NewNodeType::NONE; //da beim Start kein Text gelesen wurde

    this->pause_reading = false; //pausiert einen Zyklus lang das Lesen des nächsten Zeichens.
                                //Bei true wird so ein Zeichen zweimal gelesen
}


void XMLParser::parseData(const std::vector<uint8_t>& data)
{
    //Humans start counting at 1, so we start reading at column 1 and row 1
    //from their perspective:
    this->input_column_pos = 1;
    this->input_row_pos = 1;

    for (auto current_char: data) {
        //Phase 0: count line number and character number:
        if (!(current_char != 0x0A && this->last_chars[0] != 0x0D)) {
            this->input_row_pos++;
            this->input_column_pos = 1;
        }

        //Turn control characters to whitespace:
        if (current_char < 0x20) {
            current_char = 0x20;
        }

        //Phase 1: Scanner
        //Set states according to the last state and the current characters.

        switch (this->current_state) {
            case State::NOTAG: {
                switch (current_char) {
                    case 0x3c: { // < gelesen
                        this->last_state = this->current_state;
                        this->current_state = State::TAGSTART;

                        //Vermeidung von Textknoten mit dem leeren Wort:
                        if (this->new_node_name.length() != 0) {
                            this->new_node_type = NewNodeType::TEXT;
                        }

                        this->read_current_char = false;
#ifdef SHOW_STATE
                        cerr << "State: NOTAG>TAGSTART" << endl;
#endif
                        break;
                    }

                    default: {
                        this->read_current_char = true;
                        break;
                    }
                }
                break;
            }

            case State::TAGSTART: {
                if ((current_char > 0x40 && current_char < 0x5b) ||
                    (current_char > 0x60 && current_char < 0x7b) ||
                    (current_char > 0x2f && current_char < 0x3a)
                ) {
                    //A-Z or a-z or 0-9
                    this->last_state = this->current_state;
                    this->current_state = State::TAGNAME;
                    this->read_current_char = true;
#ifdef SHOW_STATE
                    cerr << "State: TAGSTART>TAGNAME" << endl;
#endif
                } else if (current_char == 0x2f) {
                    // read /
                    this->last_state = this->current_state;
                    this->current_state = State::CLOSINGTAG;
                    this->read_current_char = false;
#ifdef SHOW_STATE
                    cerr << "State: TAGSTART>CLOSINGTAG" << endl;
#endif
                } else if (current_char == 0x21) {
                    // read !
                    this->last_state = this->current_state;
                    this->current_state = State::SGMLTAG;
                    this->read_current_char = false;
#ifdef SHOW_STATE
                    cerr << "State: TAGSTART>SGMLTAG" << endl;
#endif
                } else if (current_char == 0x3f) {
                    // read ?
                    this->last_state = this->current_state;
                    this->current_state = State::PROLOG;
                    this->read_current_char = false;
#ifdef SHOW_STATE
                    cerr << "State: TAGSTART>PROLOG" << endl;
#endif
                }
                break;
            }

            case State::PROLOG: {
                this->read_current_char = false;
                //TODO: prolog handling!
                if (current_char == 0x3e) {
                    //read >
                    if (this->last_chars[0] == 0x3f) {
                        //previously read ?: End of prolog.
                        this->last_state = this->current_state;
                        this->current_state = State::NOTAG;
#ifdef SHOW_STATE
                    cerr << "State: PROLOG>NOTAG" << endl;
#endif
                    }
                }
                break;
            }

            case State::SGMLTAG: {
                if (current_char == 0x2d) { // - gelesen
                    if (this->last_chars[0]==0x2d) { //- vorher gelesen: neuer Kommentar
                        this->last_state = this->current_state;
                        this->current_state = State::COMMENT;
                        this->read_current_char = false;
#ifdef SHOW_STATE
                        cerr << "State: SGMLTAG>COMMENT" << endl;
#endif
                    }
                }

                if (current_char == 0x3e) { // > gelesen
                    this->last_state = this->current_state;
                    this->current_state = State::NOTAG;
                    this->new_node_type = NewNodeType::TEXT;
                    this->read_current_char = false;
#ifdef SHOW_STATE
                    cerr << "State: SGMLTAG>NOTAG" << endl;
#endif
                }
                break;
            }

            case State::TAGNAME: {
                switch(current_char) {
                    case 0x2f: { // / gelesen
                        this->last_state = this->current_state;
                        this->current_state = State::DIRECTCLOSEDTAG;
                        this->read_current_char = false;
                        this->new_node_type = NewNodeType::NODE;
#ifdef SHOW_STATE
                        cerr << "State: TAGNAME>DIRECTCLOSEDTAG" << endl;
#endif
                        break;
                    }

                    case 0x3e: { // > gelesen
                        this->last_state = this->current_state;
                        this->current_state = State::NOTAG;
                        this->new_node_type = NewNodeType::NODE;
                        this->read_current_char = false;
#ifdef SHOW_STATE
                        cerr << "State: TAGNAME>NOTAG" << endl;
#endif
                        break;
                    }

                    case 0x20: { // blank gelesen
                        this->last_state = this->current_state;
                        this->current_state = State::TAGCONTENT;
                        this->read_current_char = false;
                        this->new_node_type = NewNodeType::NODE;
#ifdef SHOW_STATE
                        cerr << "State: TAGNAME>TAGCONTENT" << endl;
#endif
                        break;
                    }

                    default: {
                        this->read_current_char = true;
                        break;
                    }
                }
                break;
            }

            case State::TAGCONTENT: {
                if (current_char == 0x3e) { // > gelesen
                    this->last_state = this->current_state;
                    this->current_state = State::NOTAG;
#ifdef SHOW_STATE
                    cerr << "State: TAGCONTENT>NOTAG" << endl;
#endif
                }

                if ((current_char > 0x40 && current_char < 0x5b) ||
                    (current_char > 0x60 && current_char < 0x7b) ||
                    (current_char > 0x2f && current_char < 0x3a)
                    ) { //A-Z oder a-z oder 0-9
                    this->last_state = this->current_state;
                    this->current_state = State::ATTRIBUTE;
                    this->read_current_char = true;
#ifdef SHOW_STATE
                    cerr << "State: TAGCONTENT>ATTRIBUTE" << endl;
#endif
                }
                break;
            }

            case State::CLOSINGTAG: {
                if (current_char == 0x3e) { // > gelesen
                    this->last_state = this->current_state;
                    this->read_current_char = false;
                    this->current_state = State::NOTAG;
                    this->new_node_type = NewNodeType::CLOSING;
#ifdef SHOW_STATE
                    cerr << "State: CLOSINGTAG>NOTAG" << endl;
#endif
                } else {
                    this->read_current_char = true;
                }
                break;
            }

            case State::DIRECTCLOSEDTAG: {
                if (current_char == 0x3e) { // > gelesen
                    this->last_state = this->current_state;
                    this->current_state = State::NOTAG;
                    this->read_current_char = false;
                    this->new_node_type = NewNodeType::CLOSING;
#ifdef SHOW_STATE
                    cerr << "State: DIRECTCLOSEDTAG>NOTAG" << endl;
#endif
                }
                break;
            }

            case State::COMMENT: {
                if (current_char == 0x3e && this->last_chars[0] == 0x2d &&
                    this->last_chars[1] == 0x2d) { // Reihenfolge -, -, > gelesen
                    this->last_state = this->current_state;
                    this->current_state = State::CLOSINGCOMMENT;
                    this->read_current_char = false;
                    this->new_node_name = this->new_node_name.erase(
                        this->new_node_name.length() - 2
                        ); //Sonderbehandlung, um die letzten zwei - zu entfernen
#ifdef SHOW_STATE
                    cerr << "State: COMMENT>NOTAG" << endl;
#endif
                } else {
                    this->read_current_char = true;
                }
                break;
            }

            case State::CLOSINGCOMMENT: {
                this->pause_reading = true;
                this->read_current_char = false;
                this->current_state = State::NOTAG;
                this->new_node_type = NewNodeType::CLOSING;
                break;
            }

            case State::ATTRIBUTE: {
                switch(current_char) {
                    case 0x20: { // blank gelesen
                        this->last_state = this->current_state;
                        this->current_state = State::TAGCONTENT;
                        this->read_current_char = false;
                        this->new_node_type = NewNodeType::ATTR;
#ifdef SHOW_STATE
                        cerr << "State: ATTRIBUTE>TAGCONTENT" << endl;
#endif
                        break;
                    }

                    case 0x3d: { // = gelesen
                        this->last_state = this->current_state;
                        this->current_state = State::ATTRASSIGN;
                        this->read_current_char = false;
                        this->new_node_type = NewNodeType::ATTR;
#ifdef SHOW_STATE
                        cerr << "State: ATTRIBUTE>ATTRASSIGN" << endl;
#endif
                        break;
                    }

                    case 0x3e: { // > gelesen
                        this->last_state = this->current_state;
                        this->current_state = State::NOTAG;
                        this->read_current_char = false;
                        this->new_node_type = NewNodeType::ATTR;
#ifdef SHOW_STATE
                        cerr << "State: ATTRIBUTE>NOTAG" << endl;
#endif
                        break;
                    }

                    default: {
                        this->read_current_char = true;
                        break;
                    }
                }
                break;
            }

            case State::ATTRASSIGN: {
                if (current_char == 0x22) { // " gelesen
                    this->last_state = this->current_state;
                    this->read_current_char = false;
                    this->current_state = State::ATTRVALUE2;
#ifdef SHOW_STATE
                    cerr << "State: ATTRASSIGN>ATTRVALUE2" << endl;
#endif
                }
                if(current_char == 0x27) { // ' gelesen
                    this->last_state = this->current_state;
                    this->read_current_char = false;
                    this->current_state = State::ATTRVALUE1;
#ifdef SHOW_STATE
                    cerr << "State: ATTRASSIGN>ATTRVALUE1" << endl;
#endif
                }
                if ((current_char > 0x40 && current_char < 0x5b) ||
                    (current_char > 0x60 && current_char < 0x7b) ||
                    (current_char > 0x2f && current_char < 0x3a)
                    ) { //A-Z oder a-z oder 0-9
                    this->last_state = this->current_state;
                    this->current_state = State::ATTRVALUEBAD;
                    this->read_current_char = true;
#ifdef SHOW_STATE
                    cerr << "State: ATTRASSIGN>ATTRVALUEBAD" << endl;
#endif
                }
                if (current_char == 0x3e) { // > gelesen
                    this->last_state = this->current_state;
                    this->current_state = State::NOTAG;
                    this->new_node_type = NewNodeType::ATTR;
                    this->read_current_char = false;
#ifdef SHOW_STATE
                    cerr << "State: ATTRASSIGN>NOTAG" << endl;
#endif
                }
                break;
            }

            case State::ATTRVALUE1: {
                if (current_char == 0x3c) { // < gelesen
                    //(see W3C: XML 1.0 Chapter 3.1)
                    //since '<' is not allowed here throw an exception

                    throw MNE::MarkupParserException(
                        "XMLParser",
                        "Character < not allowed at this position!",
                        0,
                        this->input_row_pos,
                        this->input_column_pos
                        );
                } else if (current_char == 0x27 && this->last_chars[0] != 0x5c) { // ' gelesen, ohne vorhergehenden Backslash
                    this->last_state = this->current_state;
                    this->current_state = State::TAGCONTENT;
                    this->read_current_char = false;
                    this->new_node_type = NewNodeType::ATTRVAL;
#ifdef SHOW_STATE
                    cerr << "State: ATTRVALUE1>TAGCONTENT" << endl;
#endif
                } else {
                    this->read_current_char = true;
                }
                break;
            }

            case State::ATTRVALUE2: {
                if (current_char == 0x3c) { // < gelesen
                    //(see W3C: XML 1.0 Chapter 3.1)
                    //since '<' is not allowed here throw an exception

                    throw MNE::MarkupParserException(
                        "XMLParser",
                        "Character < not allowed at this position!",
                        0,
                        this->input_row_pos,
                        this->input_column_pos
                        );
                } else if(current_char == 0x22 && this->last_chars[0] != 0x5c) { // " gelesen, ohne vorhergehenden Backslash
                    this->last_state = this->current_state;
                    this->current_state = State::TAGCONTENT;
                    this->read_current_char = false;
                    this->new_node_type = NewNodeType::ATTRVAL;
#ifdef SHOW_STATE
                    cerr << "State: ATTRVALUE2>TAGCONTENT" << endl;
#endif
                } else {
                    this->read_current_char = true;
                }
                break;
            }

            case State::ATTRVALUEBAD: { //WARNING: is this allowed in XML?
                switch (current_char) {
                    case 0x20: { // blank gelesen
                        this->last_state = this->current_state;
                        this->current_state = State::TAGCONTENT;
                        this->read_current_char = false;
                        this->new_node_type = NewNodeType::ATTRVAL;
#ifdef SHOW_STATE
                        cerr << "State: ATTRVALUEBAD>TAGCONTENT" << endl;
#endif
                        break;
                    }

                    case 0x3e: { // > gelesen
                        this->last_state = this->current_state;
                        this->current_state = State::NOTAG;
                        this->read_current_char = false;
                        this->new_node_type = NewNodeType::ATTRVAL;
#ifdef SHOW_STATE
                        cerr << "State: ATTRVALUEBAD>NOTAG" << endl;
#endif
                        break;
                    }

                    default: {
                        this->read_current_char = true;
                        break;
                    }
                }
                break;
            }
        }

        if (this->read_current_char == true) {
            this->new_node_name += current_char;
        }

        //Phase 2: Parser

        switch (this->new_node_type) {
            case NewNodeType::NODE: {
                if (this->first_node_ptr == nullptr) {
                    node_tree = std::make_shared<DocumentNode>(
                        this->new_node_name,
                        DocumentNode::Type::NODE
                        );
                    node_tree->origin.column = this->input_column_pos;
                    node_tree->origin.line = this->input_row_pos;
                    first_node_ptr = node_tree;
                    current_node_ptr = node_tree;
                } else {
                    current_node_ptr->appendChild(
                        this->new_node_name,
                        DocumentNode::Type::NODE
                        );
                    current_node_ptr = current_node_ptr->getLastChild();
                    current_node_ptr->origin.column = this->input_column_pos;
                    current_node_ptr->origin.line = this->input_row_pos;
                }
                this->new_node_type = NewNodeType::NONE;
                this->new_node_name = "";

                break;
            }

            case NewNodeType::TEXT: {
                //Check if the text node contains anything else than
                //whitespace or control characters. Only create a new
                //text node when it contains more than just whitespace.
                bool only_whitespace = true;
                for (size_t i = 0; i < this->new_node_name.size(); i++) {
                    if (this->new_node_name[i] > 0x20) {
                        only_whitespace = false;
                        break;
                    }
                }
                if (!only_whitespace) {
                    if (current_node_ptr == nullptr) {
                        //We haven't read any node yet and the document
                        //starts with text? That's an error!
                        throw MNE::MarkupParserException(
                            "XMLParser",
                            "Text found before the first node!",
                            0,
                            this->input_row_pos,
                            this->input_column_pos
                            );
                    }

                    current_node_ptr->appendChild(
                        this->new_node_name,
                        DocumentNode::Type::TEXT
                        );
                }
                this->new_node_type = NewNodeType::NONE;
                this->new_node_name = "";

                break;
            }

            case NewNodeType::ATTR: {
                //Check if attribute name already exists and throw exception
                //if this occurs (see W3C: XML 1.0 Chapter 3.1):
                if (current_node_ptr != nullptr) {
                    if (current_node_ptr->hasAttribute(this->new_node_name)) {
                        throw MNE::MarkupParserException(
                            "XMLParser",
                            "Duplicate attribute for a node",
                            100,
                            this->input_row_pos,
                            this->input_column_pos
                            );
                    }
                    this->current_attribute_name = this->new_node_name;
                }
                this->new_node_type = NewNodeType::NONE;
                this->new_node_name = "";

                break;
            }

            case NewNodeType::ATTRVAL: {
                if (current_node_ptr != nullptr && !this->current_attribute_name.empty()) {
                    current_node_ptr->setAttribute(this->current_attribute_name, this->new_node_name);
                    this->current_attribute_name = "";
                }
                this->new_node_type = NewNodeType::NONE;
                this->new_node_name = "";

                break;
            }

            case NewNodeType::CLOSING: {
                if (this->new_node_name == "") {
                    //leerer Name: es handelt sich um einen direkt schliessenden Knoten (z.B. <br/>)
                    current_node_ptr = current_node_ptr->getParentNode();
                } else {
                    if (this->new_node_name == current_node_ptr->name) {
                        this->new_node_type = NewNodeType::NONE;
                        this->new_node_name = "";
                        //wenn der schliessende Knoten denselben Namen hat wie der aktuelle Knoten,
                        //so wird eine Ebene nach oben gesprungen.
                        //Allerdings muss erst geprüft werden, ob CurrentNode gleich dem ersten Knoten ist oder NULL ist:
                        //if((current_node_ptr != first_node_ptr) && (current_node_ptr->parent != NULL))
                        auto current_parent = current_node_ptr->getParentNode();
                        if (current_parent != nullptr) {
                            //19.07.2012: nur noch prüfen, ob Vaterknoten NULL ist
                            current_node_ptr = current_parent;
                        } else {
                            //vielleicht sollte etwas getan werden, wenn der oberste Knoten erreicht wurde
                        }
                    } else {
                        //Wenn der Name des schliessenden Knoten anders lautet als der des aktuellen Knotens,
                        //so bricht der Parser ab und wirft eine ParserException aus, da der Quellcode nicht mehr XML-konform ist
                        throw MNE::MarkupParserException(
                            "XMLParser",
                            "Closing tag doesn't match opening tag!",
                            0,
                            this->input_row_pos,
                            this->input_column_pos
                            );
                    }
                }
                break;
            }
        }

        this->last_chars[2]=this->last_chars[1];
        this->last_chars[1]=this->last_chars[0];
        this->last_chars[0]=current_char;

        if (this->pause_reading == false) {
            this->input_column_pos++;
        } else {
            this->pause_reading = false;
        }
    }
}


std::shared_ptr<DocumentNode> XMLParser::returnNodeTree()
{
    //klont rekursiv alle Knoten, damit nicht auf ungültigen Speicher verwiesen wird
    return this->first_node_ptr->cloneNode();
}
