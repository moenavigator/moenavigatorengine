/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MNE__COOKIEJAR_H
#define MNE__COOKIEJAR_H


#include <map>
#include <string>
#include <stdexcept>
#include <vector>


#include "Cookie.h"


namespace MNE
{
    /**
     * The CookieJar class manages cookies often used in HTTP requests
     * and responses.
     */
    class CookieJar
    {
        protected:


        /**
         * A map containing all cookies, grouped by domain.
         */
        std::map<std::string, std::vector<Cookie>> cookies;


        public:


        /**
         * Retrieves all cookies for a domain, including subdomain,
         * if requested.
         *
         * @param const std::string& domain The domain for which to return
         *     all cookies.
         *
         * @param bool include_subdomain Whether to include cookies from
         *     subdomains (true) or not (false). Defaults to false.
         *
         * @returns std::map<std::string, std::vector<Cookie>> All cookies that
         *     are stored in the cookie jar and that belong to the domain and,
         *     if requested, the subdomains of that domain.
         */
        virtual std::map<std::string, std::vector<Cookie>> getDomainCookies(
            const std::string& domain,
            bool include_subdomains = false
            );


        /**
         * Sets a single cookie for a domain. If a cookie with that name
         * already exists, it will be overwritten.
         *
         * @param const std::string& domain The domain for which the cookie
         *     shall be set.
         *
         * @param Cookie& cookie The cookie to be set.
         */
        virtual void setCookie(const std::string& domain, Cookie& cookie);


        /**
         * Deletes all cookies for a domain from the cookie jar.
         *
         * @param const std::string& domain The domain whose cookies
         *     shall be deleted from the cookie jar.
         */
        virtual void deleteCookies(const std::string& domain);


        /**
         * Returns all cookies that are set in this cookie jar.
         *
         * @returns std::map<std::string, std::vector<Cookie>> A map containing
         *     all cookies, grouped by domain name.
         */
        virtual std::map<std::string, std::vector<Cookie>> getAllCookies();


        /**
         * Sets all cookies for this cookie jar.
         * Existing cookies are overwritten
         *
         * @param std::map<std::string, std::vector<Cookie>> A map containing
         *     the cookies to be set.
         */
        virtual void setAllCookies(std::map<std::string, std::vector<Cookie>> cookies);


        /**
         * Deletes all (really all!) cookies from the cookie jar.
         */
        virtual void deleteAllCookies();
    };
}


#endif
