/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "CookieJar.h"


using namespace MNE;


std::map<std::string, std::vector<Cookie>> CookieJar::getDomainCookies(
    const std::string& domain,
    bool include_subdomains
    )
{
    if (domain.empty()) {
        return {};
    }

    if (include_subdomains) {
        //Iterate over all cookie domains (keys) and check if the
        //requested domain is at the end of those keys.
        std::map<std::string, std::vector<Cookie>> relevant_cookies;
        auto domain_length = domain.length();
        for (auto it = this->cookies.begin(); it != this->cookies.end(); it++) {
            if (it->first == domain) {
                //The requested domain is found.
                relevant_cookies[it->first] = it->second;
                continue;
            }
            auto current_length = it->first.length();
            if (current_length < domain_length) {
                //The requested domain cannot be part of the domain that is
                //currently iterated.
                continue;
            }
            //Check if the requested domain name occurs in the part from
            //(current_length - domain_length) to the end.
            auto search_start = current_length - domain_length;
            if (it->first.find(domain, search_start) != std::string::npos) {
                //We found a subdomain.
                relevant_cookies[it->first] = it->second;
            }
        }
        return relevant_cookies;
    } else {
        try {
            return {
                std::pair<std::string, std::vector<Cookie>>(domain, this->cookies.at(domain))
            };
        } catch (std::out_of_range) {
            return {};
        }
    }
}


void CookieJar::setCookie(const std::string& domain, Cookie& cookie)
{
    if (domain.empty()) {
        return;
    }
    auto domain_cookies = this->cookies[domain];
    //Check if the cookie already exist:
    for (auto it = domain_cookies.begin(); it != domain_cookies.end(); it++) {
        if (it->name == cookie.name) {
            *it = cookie;
            return;
        }
    }
    //The cookie is new for the domain if this point is reached.
    domain_cookies.push_back(cookie);
}


void CookieJar::deleteCookies(const std::string& domain)
{
    if (domain.empty()) {
        return;
    }

    auto domain_cookies = this->cookies.find(domain);
    if (domain_cookies != this->cookies.end()) {
        //Empty the vector:
        domain_cookies->second.clear();
    }
}


std::map<std::string, std::vector<Cookie>> CookieJar::getAllCookies()
{
    return this->cookies;
}


void CookieJar::setAllCookies(std::map<std::string, std::vector<Cookie>> cookies)
{
    this->cookies = cookies;
}


void CookieJar::deleteAllCookies()
{
    this->cookies.clear();
}
