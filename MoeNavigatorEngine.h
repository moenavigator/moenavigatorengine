/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MOENAVIGATORENGINE_H
#define MOENAVIGATORENGINE_H


#include <algorithm>
#include <cstdint>
#include <memory>
#include <stack>
#include <iostream>
#include <vector>


#include "./MarkupParsers/GopherMenuParser.h"
#include "./MarkupParsers/HTMLParser.h"
#include "./MarkupParsers/PlaintextParser.h"
#include "./MarkupParsers/XMLParser.h"

#include "./StylesheetParsers/DefaultStyle.h"
#include "./StylesheetParsers/CSSParser.h"

#include "./Exceptions/Exception.h"

#include "./CommonClasses/DocumentNode.h"
#include "./CommonClasses/URL.h"
#include "./CommonClasses/StylesheetRule.h"
#include "./CommonClasses/VisualAttributes.h"

#include "./LocalStorage/CookieJar.h"

#include "./Settings/CookiePolicy.h"

#include "./Network/NetworkHandler.h"
#include "./MNERenderer/MNERenderer.h"
#include "./MNECommon/MNECommon.h"
#include "./Network/HTTPRequest.h"
#include "./Network/GopherRequest.h"
#include "./Network/QOTDRequest.h"
#include "./Network/ResponseHandler.h"


#include "./Network/NetworkHandler_POSIX.h"
#include "./Network/NetworkHandler_GnuTLS.h"


//STUB:
#define SYSTEM_LITTLEENDIAN


enum
{
  STYLESHEETMEDIA_SCREEN,
  STYLESHEETMEDIA_PRINT,
  STYLESHEETMEDIA_SIZE //to determine the size for an array with all these enums
};


/**
 * This class represents MoeNavigator's Engine.
 * It calls the parsers, analyses a node tree of a parsed document
 * and draws content on the graphical output.
 **/
class MoeNavigatorEngine: public MNEEventHandler, public MNE::Network::ResponseHandler
{
    protected:


    /**
     * A pointer to the document's root node (usually named $DOCUMENT).
     */
    std::shared_ptr<DocumentNode> document;


    /**
     * A pointer to the node which is currently used/processed.
     */
    std::shared_ptr<DocumentNode> current_node;


    /**
     * A pointer to the cookie jar the engine uses to manage cookies
     * sent via HTTP(S).
     */
    std::shared_ptr<MNE::CookieJar> cookie_jar;


    /**
     * The general cookie policy to be used unless a specific policy
     * is set for a specified domain. Defaults to BLOCK to not save
     * any cookies (feel free to think of the cookie monster here).
     *
     * @see CookiePolicy class in the Settings subdirectory.
     */
    CookiePolicy general_cookie_policy = CookiePolicy::BLOCK;


    /**
     * Traverses the node tree and analyses the content of the tree.
     */
    void processDocument();


    /**
     * A function to set a node's style by its style-attribute
     */
    void setNodeStyleAttribute(
        const std::shared_ptr<DocumentNode> node,
        const std::string attribute_name,
        const std::string attribute_value
        );


    /**
     * Searches the stylesheet rules for a matching rule
     * and applies it to the node.
     */
    void setNodeStyle(std::shared_ptr<DocumentNode> node);


    /**
     * Apply a specific stylesheet rule to a node.
     */
    void setNodeStyle(
        std::shared_ptr<DocumentNode> node,
        std::shared_ptr<StylesheetRule> rule
        );


    bool isEmptyTextNode(std::shared_ptr<DocumentNode> node);


    /**
     * A stub variable at this moment since it is used to workaround text nodes
     * that are children of the $DOCUMENT node having the document as child nodes.
     * This is a bug in the HTML parser that has to be fixed before this workaround
     * can be removed!
     */
    bool document_head_found;


    /**
     * If this attribute is set to false it indicates that analysing
     * the document isn't possible yet.
     */
    bool document_ready;


    /**
     * The MIME type of the document.
     */
    std::string document_mime_type = "";


    /**
     * The encoding of the document.
     */
    std::string DocumentEncoding;


    /**
     * A stack for the current child nodes on all layers.
     * TODO: move into ProcessDocument method!
     */
    std::stack<unsigned long> current_child_stack;


    /**
     * A vector containing all renderers which shall receive the processed
     * document.
     */
    std::vector<std::shared_ptr<MNERenderer>> renderers;


    //Stylesheet related attributes and methods:


    /**
     * Searches a selector name in the list of selectors.
     * NOT IMPLEMENTED YET.
     */
    unsigned long int* searchSelector(std::string selector);


    std::shared_ptr<StylesheetCollection> stylesheet_collection;


    std::shared_ptr<StylesheetRule> searchStylesheetsFromNode(
        const std::shared_ptr<DocumentNode> Node
        ) const;


    /**
     * This method sets a node's style according to
     * the style defined for the node and its parents and children.
     */
    void determineVisualAttributes(std::shared_ptr<DocumentNode> Node);


    /**
     * A function to decode HTML color values and to store them in a
     * 32-bit unsigned int value. The RGBA values will be stored in the
     * following order from high to low bits: RGBA. So the R value is stored
     * the highest 8 bits, the alpha value is stored in the lowest 8 bits.
     */
    unsigned long int decodeColourValues(const std::string colour);

    /**
     * A stylesheet parser instance.
     */
    std::unique_ptr<CSSParser> stylesheet_parser;

    /**
     * A markup parser instance.
     */
    std::unique_ptr<MarkupParser> markup_parser;

    /**
     * A network handler instance.
     */
    std::shared_ptr<MNE::Network::NetworkHandler> network;


    /**
     * A network handler instance for TLS encrypted network traffic.
     */
    std::shared_ptr<MNE::Network::NetworkHandler> tls_network;


    /**
     * The user agent string the engine will send on HTTP requests.
     */
    std::string user_agent;


    /**
     * SetDocument_Internal sets a document node tree to be the tree
     * the engine processes. The previous document will be deleted.
     * After this the new document will be analysed.
     * SetDocument_Internal doesn't clone the new document
     * with all its children like setDocument does.
     */
    void setDocument_Internal(
        std::shared_ptr<DocumentNode> new_document,
        const std::string& mime_type = ""
        );


    unsigned long document_width;

    unsigned long document_height;


    public:


    /**
     * This constructor does all the initialisation like setting pointers
     * to nullptr, creating class instances for parsers, network handlers
     * and the graphical output.
     */
    MoeNavigatorEngine();


    /**
     * Returns a pointer to the internal cookie jar that can be used
     * to manipulate cookies inside the cookie jar. If no cookie jar
     * is set up, a nullptr is returned.
     *
     * @returns std::shared_ptr<CookieJar> The pointer to the internal
     *     cookie jar of the engine. If no cookie jar is setup,
     *     a nullptr is returned.
     */
    std::shared_ptr<MNE::CookieJar> getCookieJar();


    /**
     * Sets another cookie jar as the cookie jar for the engine.
     * The cookie jar must exist, this means it must not be a nullptr.
     *
     * @param std::shared_ptr<CookieJar> cookie_jar The new cookie jar
     *     to be used.
     */
    void setCookieJar(std::shared_ptr<MNE::CookieJar> cookie_jar);


    /**
     * Returns the current general cookie policy that is to be used unless an
     * explicit policy is set for a specific domain.
     *
     * @returns CookiePolicy The general cookie policy.
     */
    CookiePolicy getGeneralCookiePolicy();


    /**
     * Sets the current general cookie policy that is to be used unless an
     * explicit policy is set for a specific domain.
     *
     * @param CookiePolicy The general cookie policy to be used.
     */
    void setGeneralCookiePolicy(CookiePolicy policy);


    //Node and DOM Tree methods:

    /**
     * ApplyAttribute checks if a node has special attributes
     * that have to be treated different from normal nodes.
     * At the moment this function just checks for style attributes.
     */
    void applyAttribute(
        std::shared_ptr<DocumentNode> node,
        std::string name,
        std::string value
        );

    /**
     * SetDocument sets a document node tree to be the tree
     * the engine processes. The previous document will be deleted.
     * After this the new document will be analysed.
     */
    void setDocument(
        std::shared_ptr<DocumentNode> new_document,
        const std::string& mime_type = ""
        );


    //Renderer methods:


    /**
     * Attaches a renderer to the list of renderers which shall render
     * a processed document.
     *
     * @param std::shared_ptr<MNERenderer> renderer The render to be attached.
     */
    void addRenderer(std::shared_ptr<MNERenderer> renderer);


    /**
     * Returns the renderers that are currently attached to the engine.
     *
     * @returns std::vector<std::shared_ptr<MNERenderer>> A list with the
     *     renderer instances.
     */
    std::vector<std::shared_ptr<MNERenderer>> getRenderers();


    /**
     * Detaches a specific renderer from the engine. In case the renderer
     * referenced by the pointer hasn't been attached, nothing is done.
     *
     * @param std::shared_ptr<MNERenderer> renderer The renderer to be detached.
     */
    void removeRenderer(std::shared_ptr<MNERenderer> renderer);


    //Graphics methods:

    /**
     * Sets the output width and height of the document.
     */
    void setDocumentDimensions(
        unsigned long int width,
        unsigned long int height
        );

    /**
     * drawPage starts the drawing process of the document node tree.
     */
    void drawPage();

    //network methods:

    /**
     * When called, OpenURL first splits the URL in protocol, domain name
     * and path. After that the appropriate network protocol handler
     * is called and if the resource could be opened successfully the HTML
     * parser is called for every data packet.
     */
    void openURL(std::string url);

    /**
     * Sets the UserAgent string the engine uses in HTTP requests.
     * SetUserAgent sets the UserAgent string the engine uses
     * in HTTP requests.
     */
    void setUserAgent(std::string user_agent);


    //ResponseHandler interface implementation:


    virtual void processResponseData(
        std::shared_ptr<std::vector<uint8_t>> data,
        bool last_chunk = false
        ) override;


    //event handling methods:

    void handleEvent(MNEEvent* e) override;
};

#endif
