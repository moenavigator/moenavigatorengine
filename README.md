# MoeNavigatorEngine

MoeNavigatorEngine is a web browser engine written from scratch in C++.

The goal of this project is to create a modular,
fast and flexible web browser engine.


## Dependencies

To compile MoeNavigatorEngine you will need the following software:

- CMake (at least version 2.8)
- A C++ compiler (for example g++) and all standard C++ library headers
- C POSIX socket headers
- GnuTLS library development headers
- libfmt (C++20 text formatting library)


## Compiling

First you will need to create a "build" directory below the base directory
of this repository, where all the temporary files will be placed.
Then, open a terminal in the build directory and run the following command:

    cmake -DBUILD_SHARED_LIBS=1 ..

This will make the project configuration and will create all files necessary
for invoking make. When cmake has finished you invoke make simply by typing:

    make

If compilation is successful you should have the compiled version of
MoeNavigatorEngine lying in your build directory.

### Compiling for development

To compile MoeNavigatorEngine for development and debugging purposes,
you should make a debug build. From the "build" directory from above,
you can create a debug build like this:

    cmake -DCMAKE_BUILD_TYPE=Debug -DBUILD_SHARED_LIBS=1 ..

When cmake has finished, you can invoke make like before to build
a debug version of MoeNavigatorEngine.


## Documentation

Besides the source code documentation there is the project's wiki
for additional documentation regarding the internals of the engine
and the project in general. The wiki is available at codeberg.org:

https://codeberg.org/moenavigator/moenavigatorengine/wiki/index

Alternatively, you may download the whole wiki source code (markdown)
for offline reading. After cloning the source code repository, simply
init and update the submodules:

    git submodule init
    git submodule update

After that, you will find the whole wiki source code in the "doc" directory.
