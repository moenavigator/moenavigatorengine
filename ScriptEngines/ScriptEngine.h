/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef SCRIPTENGINE_H
#define SCRIPTENGINE_H


#include <string>
#include <vector>


class ScriptEngine
{
    public:


    /**
     * Parses script source code.
     */
    virtual void parseCode(const std::string& code) = 0;


    /**
     * Callings functions from the engine.
     */
    virtual void callFunction(
        const std::string& function_name,
        const std::vector<std::string>& function_arguments
        ) = 0;


    /**
     * Raises ("fires") an event.
     */
    virtual void raiseEvent(const std::string& event_name) = 0;


    /**
     * The default virtual destructor.
     */
    virtual ~ScriptEngine() = default;
};


#endif
