/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "JavaScriptEngine.h"




void JavaScriptEngine::parseCode(const std::string& code)
{
    //Scan first, then parse.
    std::cerr << "DEBUG: scan: [" << code << "]" << std::endl;

    //Scan:
    for (auto c: code) {
        if (this->current_token == TOK_NOTOKEN) {
            //No token being read at the moment.
            if ((c > 0x40 && c < 0x5B) || (c > 0x40 && c < 0x5B)) {
                //read a-z or A-Z: identifier
                this->current_token = TOK_IDENTIFIER;
            }
            if (c == 0x22) {
                //read ": string opening
                this->current_token = TOK_STRING_OPEN;
            }
        } else if (this->current_token == TOK_IDENTIFIER) {
            //Currently reading an identifier
            if (!((c > 0x40 && c < 0x5B) || (c > 0x40 && c < 0x5B))) {
                std::cerr << "DEBUG: read identifier! " << std::endl;
                this->current_token = TOK_NOTOKEN;
            }
        } else if (this->current_token == TOK_STRING_OPEN) {
            if (c == 0x22) {
                //read ": string closing
                std::cerr << "DEBUG: read string! " << std::endl;
                this->current_token = TOK_STRING_CLOSE;
            }
            break;
        }
    }
}


std::vector<JavascriptToken> JavaScriptEngine::getTokens()
{
    return {};
}


JavaScriptEngine::JavaScriptEngine()
{
  this->current_token = TOK_NOTOKEN;
  this->token_list = {};
}


void JavaScriptEngine::callFunction(
    const std::string& function_name,
    const std::vector<std::string>& function_arguments
    )
{
    
}


void JavaScriptEngine::raiseEvent(const std::string& event_name)
{
    
}
