/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef JAVASCRIPTENGINE_H
#define JAVASCRIPTENGINE_H


#include "ScriptEngine.h"

#include <iostream>
#include <string>
#include <vector>


//Token-Typen:
#define TOK_NOTOKEN 0
#define TOK_IDENTIFIER 1
#define TOK_NUMBER 2
#define TOK_PARENTHESE_OPEN 3 //(
#define TOK_PARENTHESE_CLOSE 4 //)
#define TOK_BRACKETS_OPEN 5 //[
#define TOK_BRACKETS_CLOSE 6 //]
#define TOK_BRACES_OPEN 7 //{
#define TOK_BRACES_CLOSE 8 //}
#define TOK_SIGN_PLUS 9 //+
#define TOK_SIGN_MINUS 10 //-
#define TOK_SIGN_EQUAL 11 //=
#define TOK_SIGN_ASTERISK 12  //*
#define TOK_SIGN_SLASH 13 // /
#define TOK_SIGN_BACKSLASH 14
#define TOK_SIGN_AMPERSAND 15 //&
#define TOK_STRING_OPEN 16 //"
#define TOK_STRING_CLOSE 17 //"
#define TOK_SEMICOLON 18 //;
#define TOK_COLON 19 //:
#define TOK_LESSTHAN 20 //<
#define TOK_GREATERTHAN 21 //>
#define TOK_QUESTION 22 //?




#define BIT_IDENTIFIER 0x0
#define BIT_NUMBER 0x1
#define BIT_PARENTHESE 0x2
#define BIT_BRACKETS 0x4
#define BIT_BRACES 0x10


struct JavascriptToken
{
    unsigned int type;
    std::string name;
};


class JavaScriptEngine: public ScriptEngine
{
    public:


    JavaScriptEngine();


    void parseCode(const std::string& code);


    void callFunction(
        const std::string& function_name,
        const std::vector<std::string>& function_arguments
        );


    void raiseEvent(const std::string& event_name);


    std::vector<JavascriptToken> getTokens();


    protected:

    /**
     * The token that is being read at the moment.
     */
    unsigned char current_token = TOK_NOTOKEN;


    /**
     * The list of tokens already read.
     */
    std::vector<JavascriptToken> token_list = {};
};


#endif
