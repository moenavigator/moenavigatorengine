/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CSSSELECTORPARSER_H
#define CSSSELECTORPARSER_H


#include <algorithm>
#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include <stdexcept>
#include <vector>


#include "../MNECommon/ParsingException.h"



/**
 * The CSSSelector class represents a CSS selector.
 */
class CSSSelector
{
    public:


    /**
     * The selected ID.
     */
    std::string id = "";


    /**
     * The selected element name.
     */
    std::string element = "";


    /**
     * The selected class names.
     */
    std::vector<std::string> classes;


    /**
     * The selected element attributes.
     */
    std::map<std::string, std::string> attributes;


    /**
     * The selected sibling.
     */
    std::shared_ptr<CSSSelector> sibling;


    /**
     * Whether the selected sibling is the next sibling (true)
     * or one of the following siblings (false).
     */
    bool adjacent_sibling = false;


    /**
     * The selected child.
     */
    std::shared_ptr<CSSSelector> child;


    /**
     * Whether the selected child must be a direct child of
     * this selector (true) or else can be a grandchild or
     * even further down in the node tree (false).
     */
    bool direct_child = false;


    std::string toString()
    {
        std::string result = "";
        if (!this->element.empty()) {
            result += this->element;
        }
        if (!this->id.empty()) {
            result += "#" + this->id;
        }
        for (auto class_name: this->classes) {
            result += "." + class_name;
        }
        for (auto attribute: this->attributes) {
            if (attribute.second.empty()) {
                result += "[" + attribute.first + "]";
            } else {
                result += "[" + attribute.first + "=\"" + attribute.second + "\"]";
            }
        }
        if (this->sibling != nullptr) {
            if (this->adjacent_sibling) {
                result += " + " + this->sibling->toString();
            } else {
                result += " ~ " + this->sibling->toString();
            }
        }
        if (this->child != nullptr) {
            result += " ";
            if (this->direct_child) {
                result += " > ";
            }
            result += this->child->toString();
        }
        return result;
    }


    /**
     * The overloaded comparison operator.
     */
    bool operator==(const CSSSelector& other)
    {
        if (this->id != other.id) {
            return false;
        }
        if (this->element != other.element) {
            return false;
        }
        if (this->classes.size() != other.classes.size()) {
            return false;
        }
        for (auto class_name: this->classes) {
            auto it = std::find(
                other.classes.begin(),
                other.classes.end(),
                class_name
                );
            if (it == other.classes.end()) {
                //The class is not in the other selector.
                return false;
            }
        }
        if (this->attributes.size() != other.attributes.size()) {
            return false;
        }
        for (auto attribute: this->attributes) {
            try {
                auto value = other.attributes.at(attribute.first);
                if (value != attribute.second) {
                    return false;
                }
            } catch (std::out_of_range& e) {
                //The attribute name is not in the other selector.
                return false;
            }
        }
        if (this->sibling == nullptr) {
            if (other.sibling != nullptr) {
                return false;
            }
        } else {
            if (other.sibling == nullptr) {
                return false;
            } else {
                if (this->adjacent_sibling != other.adjacent_sibling) {
                    return false;
                }
                if (*(this->sibling) != *(other.sibling)) {
                    return false;
                }
            }
        }
        if (this->child == nullptr) {
            if (other.child != nullptr) {
                return false;
            }
        } else {
            if (other.child == nullptr) {
                return false;
            } else {
                if (this->direct_child != other.direct_child) {
                    return false;
                }
                if (*(this->child) != *(other.child)) {
                    return false;
                }
            }
        }
        return true;
    }


    bool operator!=(const CSSSelector& other)
    {
        return !(*this == other);
    }
};


/**
 * The CSSSelectorParser is a parser for CSS selector strings.
 */
class CSSSelectorParser
{
    public:


    /**
     * The states of the CSSSelectorParser.
     */
    enum class State
    {
        /**
         * NONE: The parser is parsing irrelevant whitespace.
         */
        NONE,

        /**
         * ID: The parser is parsing an element ID.
         */
        ID,

        /**
         * NAME: The parser is parsing an element name.
         */
        NAME,

        /**
         * CLASS: The parser is parsing a class name.
         */
        CLASS,

        /**
         * ATTRIBUTE_NAME: The parser is parsing an attribute name.
         */
        ATTRIBUTE_NAME,

        /**
         * ATTRIBUTE_VALUE: The parser is parsing an attribute value.
         */
        ATTRIBUTE_VALUE,


        /**
         * AFTER_ATTRIBUTE_VALUE: The parser has read the whole attribute
         * and awaits the closing bracket or "value flags" before it.
         */
        AFTER_ATTRIBUTE_VALUE,


        /**
         * AFTER_ATTRIBUTE: The parser has read the closing bracket,
         * but no character after it.
         */
        AFTER_ATTRIBUTE
    };


    protected:


    /**
     * The string that is currently read.
     */
    std::string current_string = "";


    /**
     * The name of the attribute that is currently read.
     */
    std::string current_attribute_name = "";


    /**
     * The character which encapsulates an attribute that is currently read.
     */
    char attribute_encapsulation_char = 0;


    /**
     * The character last read from the input.
     */
    char last_char = 0;


    /**
     * Whether the parser should create a new current selector part
     * when it finds the next selector in the input string (true)
     * or not (false).
     */
    bool create_new_part = false;


    /**
     * The selector that resulted from parsing.xs
     */
    std::shared_ptr<CSSSelector> selector = nullptr;


    /**
     * The current selector part that is being parsed.
     */
    std::shared_ptr<CSSSelector> current_part = nullptr;


    /**
     * The current state of the parser.
     */
    State state = State::NONE;


    /**
     * Checks whether the character is a whitespace character or not.
     *
     * @returns bool True, if it is a whitespace character, false otherwise.
     */
    bool isWhitespace(const char c);


    /**
     * Checks whether the character is a text character or not.
     *
     * @returns bool True, if it is a text character, false otherwise.
     */
    bool isText(const char c);


    /**
     * Sets up the current selector part and the selector pointer,
     * if necessary.
     */
    void setupCurrentPart();


    /**
     * Stores the current string. The parser state determines
     * where in the current selector part the current string is stored.
     */
    void storeCurrentString();


    public:


    /**
     * Resets the parser to the state it is upon initialisation.
     */
    void reset();


    /**
     * Parses a selector or parts of it using a string as input.
     *
     * @param const std::string& input The selector as string.
     *
     * @param bool last_chunk Whether the input string is the
     *     last segment of the selector string (true) or if there
     *     are other strings following with the rest of the selector string.
     *     Defaults to true.
     */
    void parseString(const std::string& input, bool last_chunk = true);


    /**
     * @returns std::shared_ptr<CSSSelector> The parsed selector.
     *     If no selector could be parsed, a nullptr is returned.
     */
    std::shared_ptr<CSSSelector> getSelector();


};


#endif
