/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "CSSSelectorParser.h"



bool CSSSelectorParser::isWhitespace(const char c)
{
    //To be extended.
    if (c == 0x20) {
        return true;
    }
    return false;
}



bool CSSSelectorParser::isText(const char c)
{
    if ((c > 0x40 && c < 0x5B) || (c > 0x60 && c < 0x7B)) {
        return true;
    }
    return false;
}


void CSSSelectorParser::setupCurrentPart()
{
    if (this->current_part == nullptr) {
        this->current_part = std::make_shared<CSSSelector>();
        if (this->current_part == nullptr) {
            throw MNE::Exception("CSSSelectorParser", "Cannot allocate memory!", 1);
        }
        if (this->selector == nullptr) {
            this->selector = this->current_part;
        } else if (this->selector != this->current_part) {
            this->selector->child = this->current_part;
        }
    } else if (this->create_new_part) {
        auto new_part = std::make_shared<CSSSelector>();
        if (new_part == nullptr) {
            throw MNE::Exception("CSSSelectorParser", "Cannot allocate memory!", 1);
        }
        this->current_part->child = new_part;
        this->current_part = this->current_part->child;
        this->create_new_part = false;
    }
}


void CSSSelectorParser::storeCurrentString()
{
    this->setupCurrentPart();
    if (this->state == State::NAME) {
        this->current_part->element = this->current_string;
    } else if (this->state == State::ID) {
        this->current_part->id = this->current_string;
    } else if (this->state == State::CLASS) {
        if (this->current_string == "") {
            return;
        }
        this->current_part->classes.push_back(this->current_string);
    } else if (this->state == State::ATTRIBUTE_NAME) {
        if (this->current_string == "") {
            return;
        }
        this->current_part->attributes.insert(
            std::pair<std::string, std::string>(this->current_string, "")
            );
    } else if (this->state == State::ATTRIBUTE_VALUE) {
        this->current_part->attributes.insert(
            std::pair<std::string, std::string>(
                this->current_attribute_name,
                this->current_string
                )
            );
        this->current_attribute_name = "";
    }
    this->current_string = "";
}



void CSSSelectorParser::reset()
{
    this->current_string = "";
    this->current_attribute_name = "";
    this->attribute_encapsulation_char = 0;
    this->last_char = 0;
    this->create_new_part = false;
    this->selector = nullptr;
    this->current_part = nullptr;
    this->state = State::NONE;
}


void CSSSelectorParser::parseString(const std::string& input, bool last_chunk)
{
    bool store_current_char = false;

    for (auto c: input) {
        if (this->state == State::NONE) {
            if (this->isWhitespace(c)) {
                continue;
            } else if (this->isText(c)) {
                this->state = State::NAME;
                store_current_char = true;
            } else if (c == '#') {
                this->state = State::ID;
            } else if (c == '.') {
                this->state = State::CLASS;
            }
            //TODO: child and sibling selectors
        } else if (this->state == State::NAME) {
            if (this->isWhitespace(c)) {
                //End of element name and selector reached.
                this->storeCurrentString();
                this->create_new_part = true;
                this->state = State::NONE;
            } else if (c == '#') {
                //End of element name reached, ID follows.
                this->storeCurrentString();
                this->state = State::ID;
            } else if (c == '.') {
                //End of element name reached, class name follows.
                this->storeCurrentString();
                this->state = State::CLASS;
            } else {
                store_current_char = true;
            }
        } else if (this->state == State::ID) {
            if (this->isWhitespace(c) || (c == '.') || (c == '[')) {
                //End of ID reached.
                if (this->current_string.empty()) {
                    //ERROR: only a hash without any text after it!
                    throw MNE::ParsingException(0, 0, "CSSSelectorParser", "Read an ID selector without ID name!", 1);
                } else {
                    this->storeCurrentString();
                }
                if (c == '.') {
                    this->storeCurrentString();
                    this->state = State::CLASS;
                } else if (c == '[') {
                    this->storeCurrentString();
                    this->state = State::ATTRIBUTE_NAME;
                } else if (this->isWhitespace(c)) {
                    this->storeCurrentString();
                    this->state = State::NONE;
                    this->create_new_part = true;
                }
            } else {
                store_current_char = true;
            }
        } else if (this->state == State::CLASS) {
            if (this->isWhitespace(c) || (c == '.') || (c == '[') || (c == '#')) {
                //End of a class name reached.
                if (this->current_string.empty()) {
                    //ERROR: only a hash without any text after it!
                    throw MNE::ParsingException(0, 0, "CSSSelectorParser", "Read a class selector without a class name!", 2);
                } else {
                    this->storeCurrentString();
                }
                if (c == '#') {
                    this->storeCurrentString();
                    this->state = State::ID;
                } else if (c == '[') {
                    this->storeCurrentString();
                    this->state = State::ATTRIBUTE_NAME;
                } else if (this->isWhitespace(c)) {
                    this->storeCurrentString();
                    this->state = State::NONE;
                    this->create_new_part = true;
                }
            } else {
                store_current_char = true;
            }
        } else if (this->state == State::ATTRIBUTE_NAME) {
            if (this->isWhitespace(c)) {
                //Whitespace inside brackets? That's an error, I guess.
                throw new MNE::ParsingException(0, 0, "CSSSelectorParser", "Read an attribute name with whitespace in it!", 3);
            } else if (c == '=') {
                //The attribute value begins.
                this->state = State::ATTRIBUTE_VALUE;
                this->current_attribute_name = this->current_string;
                this->current_string = "";
            } else if (c == ']') {
                //End of attribute name, no attribute value specified.
                this->storeCurrentString();
                this->state = State::AFTER_ATTRIBUTE;
            } else {
                store_current_char = true;
            }
        } else if (this->state == State::ATTRIBUTE_VALUE) {
            //TODO: Check for encapsulation in single or double quotes.
            if ((c == ']') && (this->attribute_encapsulation_char != 0)) {
                //End of an unencapsulated value.
                this->storeCurrentString();
                this->state = State::AFTER_ATTRIBUTE;
            } else if (this->current_string.empty()) {
                //No character from the value has been read yet.
                if ((c == '\"') || (c == '\'')) {
                    //The attribute value is encapsulated by a
                    //single or double quote character.
                    this->attribute_encapsulation_char = c;
                } else {
                    //The attribute value isn't encapsulated.
                    store_current_char = true;
                }
            } else if ((c == this->attribute_encapsulation_char)
                       && (this->last_char != '\\')) {
                //The end of an encapsulated attribute value is reached.
                this->storeCurrentString();
                this->state = State::AFTER_ATTRIBUTE_VALUE;
            } else {
                store_current_char = true;
            }
        } else if (this->state == State::AFTER_ATTRIBUTE_VALUE) {
            //In this state, there is not much to do:
            //Either a closing bracket is the next character
            //or the parser encounters whitespace and a "value flag".
            //(the latter is unsupported at the moment).
            if (c == ']') {
                this->state = State::AFTER_ATTRIBUTE;
            }
        } else if (this->state == State::AFTER_ATTRIBUTE) {
            if (c == '[') {
                this->state = State::ATTRIBUTE_NAME;
            } else if (c == '.') {
                this->state = State::CLASS;
            } else if (c == '#') {
                this->state = State::ID;
            } else if (this->isWhitespace(c)) {
                this->state = State::NONE;
                this->create_new_part = true;
            } else if (this->isText(c)) {
                this->state = State::NAME;
            } else {
                //Error: Invalid character sequence!
                throw MNE::ParsingException(0, 0, "CSSSelectorParser", "Invalid character sequence after the end of an attribute!", 10);
            }
        }
        if (store_current_char) {
            current_string += c;
        }
        store_current_char = false;
        this->last_char = c;
    }
    if (last_chunk) {
        //End of input reached.
        if (!this->current_string.empty()) {
            this->storeCurrentString();
        }
    }
}


std::shared_ptr<CSSSelector> CSSSelectorParser::getSelector()
{
    return this->selector;
}
