/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2019  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CSSPARSER_H
#define CSSPARSER_H


#include <cinttypes>
#include <climits>
#include <iostream>
#include <string>
#include <cstdio> //for sscanf
#include <cstdlib> //for atoi etc...
#include <stack>
#include <stdexcept>
#include <vector>


#include "../CommonClasses/Colour.h"
#include "../CommonClasses/VisualAttributes.h"
#include "../CommonClasses/StylesheetAttribute.h"
#include "../CommonClasses/StylesheetRule.h"

#include "../MNECommon/ParsingException.h"

#include "StylesheetParser.h"




class CSSParser: public StylesheetParser
{
    public:


    CSSParser();


    ~CSSParser();


    virtual void parseData(
        std::vector<uint8_t> data
        );


    virtual void parseData(
        std::vector<uint8_t> data,
        StylesheetParser::State state
        );


    virtual std::shared_ptr<StylesheetCollection> returnStylesheetRules();


    /**
     * ConvertAttributes converts a list (vector) of stylesheet attributes into an instance
     * of VisualAttributes. That instance has to be created before this function is called.
     */
    void convertAttributes(
        std::vector<std::shared_ptr<StylesheetAttribute>> source,
        std::shared_ptr<StylesheetRule> rule
        );


    /**
     * This function converts a CSS attribute from an StylesheetAttribute
     * instance into an attribute stored in a VisualAttributes instance.
     * This instance has to be created before this function is called.
     */
    void convertAttribute(
        std::shared_ptr<StylesheetAttribute> source,
        std::shared_ptr<StylesheetRule> rule
        );


    virtual void flushStylesheetRules();


    protected:


    std::shared_ptr<StylesheetCollection> parsed_rules; //a collection of all CSS groups and rules that have been parsed


    std::shared_ptr<StylesheetGroup> current_group;     //the group that is parsed at the moment


    std::shared_ptr<StylesheetRule> current_rule;       //the rule that is parsed at the moment


    std::shared_ptr<StylesheetAttribute> current_attribute; //the attribute (inside a rule) that is parsed at the moment


    /**
     * The current line number of the parsed tata.
     */
    uint32_t line = 0;


    /**
     * The current column number of the parsed data.
     */
    uint32_t column = 0;


    /**
     * Decodes colour values from a hexadecimal string representation
     * (#RRGGBB or #RGB) to an object of the Colour class.
     */
    Colour decodeColourValues(const std::string colour);
};

#endif
