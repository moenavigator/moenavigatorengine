/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "CSSParser.h"


#ifdef CSSPARSER_DEBUG_STATES
/**
 * This is a debug function that shows
 * the names of the parser states.
 **/
std::string CSSParser_DecodeState(StylesheetParser::State state)
{
    switch (state) {
        case StylesheetParser::State::NOSTYLE: {
          return "NOSTYLE";
          break;
        }

        case StylesheetParser::State::COMMENT: {
            return "COMMENT";
            break;
        }

        case StylesheetParser::State::SELECTOR: {
            return "SELECTOR";
            break;
        }

        case StylesheetParser::State::RULECONTENT: {
            return "RULECONTENT";
            break;
        }

        case StylesheetParser::State::PROPERTYNAME: {
            return "PROPERTYNAME";
            break;
        }

        case StylesheetParser::State::PROPERTYVALUE: {
            return "PROPERTYVALUE";
            break;
        }

        case StylesheetParser::State::ATEXPR: {
            return "ATEXPR";
            break;
        }

        case StylesheetParser::State::ATNAME: {
            return "ATNAME";
            break;
        }

        case StylesheetParser::State::ATNAMEWHITE: {
            return "ATNAMEWHITE";
            break;
        }

        case StylesheetParser::State::ATPARAM: {
            return "ATPARAM";
            break;
        }

        case StylesheetParser::State::ATPARAMWHITE: {
            return "ATPARAMWHITE";
            break;
        }

        case StylesheetParser::State::GROUPSTYLES: {
            return "GROUPSTYLES";
            break;
        }

        case StylesheetParser::State::ATCLOSING: {
            return "ATCLOSING";
            break;
        }

        case StylesheetParser::State::SELECTOR: {
            return "SELECTOR";
            break;
        }

        case StylesheetParser::State::TAGATTRIBUTE: {
            return "TAGATTRIBUTE";
            break;
        }

        case StylesheetParser::State::TAGATTRIBUTENAME: {
            return "TAGATTRIBUTENAME";
            break;
        }

        case StylesheetParser::State::TAGATTRIBUTEVALUE: {
            return "TAGATTRIBUTEVALUE";
            break;
        }

        case StylesheetParser::State::CLASSSELECTOR: {
            return "CLASSSELECTOR";
            break;
        }

        case StylesheetParser::State::IDSELECTOR: {
            return "IDSELECTOR";
            break;
        }

        case StylesheetParser::State::NEXTSELECTOR: {
            return "NEXTSELECTOR";
            break;
        }

        case StylesheetParser::State::SELECTORWHITE: {
            return "SELECTORWHITE";
            break;
        }

        case StylesheetParser::State::STYLEWHITE: {
            return "STYLEWHITE";
            break;
        }

        case StylesheetParser::State::ATTRIBUTENAME: {
            return "ATTRIBUTENAME";
            break;
        }

        case StylesheetParser::State::ATTRIBUTENAMEWHITE: {
            return "ATTRIBUTENAMEWHITE";
            break;
        }

        case StylesheetParser::State::ATTRIBUTEVALUEWHITEBEFORE: {
            return "ATTRIBUTEVALUEWHITEBEFORE";
            break;
        }

        case StylesheetParser::State::ATTRIBUTEVALUE: {
            return "ATTRIBUTEVALUE";
            break;
        }

        case StylesheetParser::State::ATTRIBUTEVALUEWHITEAFTER: {
            return "ATTRIBUTEVALUEWHITEAFTER";
            break;
        }

        case StylesheetParser::State::STYLECLOSING: {
            return "STYLECLOSING";
            break;
        }
    }
}
#endif


CSSParser::CSSParser()
{
    this->parsed_rules = nullptr; //to prevent a bug when delete on an empty stylesheet group is called!

    this->current_group = nullptr;
    this->current_rule = nullptr;
    this->current_attribute = nullptr;
    this->line = 0;
    this->column = 0;
}


CSSParser::~CSSParser()
{

}


void CSSParser::parseData(
    std::vector<uint8_t> data
    )
{
    this->parseData(data, StylesheetParser::State::NOSTYLE);
}


void CSSParser::parseData(
    std::vector<uint8_t> data,
    StylesheetParser::State state
    )
{
    StylesheetParser::State current_state = state;
    StylesheetParser::State last_state = state;

    std::stack<StylesheetParser::State> state_stack;

    bool read_next_char = true; //nächstes Zeichen in $stylesheet_code lesen, wenn TRUE
    bool read_current_char = false; //$CurrentChar in $ReadString einlesen bei TRUE
    StylesheetParser::NewElementType new_element = StylesheetParser::NewElementType::NONE;
    std::string read_string = "";

    /**
     * The start position of whitespace that is possibly trailing.
     */
    size_t possibly_trailing_whitespace_start = std::string::npos;

    std::string AtExprName = ""; //for the At-Expression Name (@media, @import, both without @)
    std::string AtExprParam = ""; //for the string following the At-Expression Name (for example "screen" in "@media screen")

    uint8_t last_chars[2];
    last_chars[0] = '\0';
    last_chars[1] = '\0';


    for (auto current_char: data) {
        if (read_next_char == false) {
            read_next_char = true;
            continue;
            //damit einen Durchlauf lang kein neues Zeichen gelesen wird
            //(ist nötig, wenn das leere Wort gelesen werden soll)
        }
        if (current_char == '\n') {
            this->column = 1;
            this->line++;
        } else {
            this->column++;
        }

        if (current_state == StylesheetParser::State::NOSTYLE) {
            if (current_char == '*' && last_chars[0] == '/') {
                //read '/*'
                current_state = StylesheetParser::State::COMMENT;
                read_current_char = false;
            }

            if ((current_char > 0x40 && current_char < 0x5b) ||
                (current_char > 0x60 && current_char < 0x7b) ||
                (current_char == '.') ||
                (current_char == '#') ||
                (current_char == '*' && last_chars[0] != '/')
                ) {
                //A-Z or a-z or a dot or a hash or asterisk character:
                //The start of a selector has been read.
                current_state = StylesheetParser::State::SELECTOR;
                read_current_char = true;
            } else if ((current_char > 0x2f && current_char < 0x3a)) {
                //0-9 directly after whitespace: invalid sequence.
                throw MNE::ParsingException(
                    this->line,
                    this->column,
                    "CSSParser",
                    "Invalid character sequence!",
                    1
                    );
            }

            if (current_char == '@') {
                current_state = StylesheetParser::State::ATBLOCKTYPE;
                read_current_char = false;
            }
        } else if (current_state == StylesheetParser::State::COMMENT) {
            if (current_char == '/') {
                if (last_chars[0] == '*') {
                    //read '*/'
                    //if there's at least one state in $state_stack
                    //that state has to be popped out of $state_stack.
                    //Otherwise the state is set to NOSTYLE

                    if(state_stack.size() > 0) {
                        current_state = state_stack.top();
                        state_stack.pop();
                    } else {
                        current_state = StylesheetParser::State::NOSTYLE;
                    }
                    read_current_char = false;
                    new_element = StylesheetParser::NewElementType::COMMENT;
                }
            }
        } else if (current_state == StylesheetParser::State::SELECTOR) {
            if (current_char == '{') {
                //We have reached the end of the selector.
                current_state = StylesheetParser::State::RULECONTENT;
                new_element = StylesheetParser::NewElementType::SELECTOR;
                read_current_char = false;
                //Remove trailing whitespace from the read string, if any:
                if ((read_string.length() > 0) &&
                    (possibly_trailing_whitespace_start != std::string::npos)) {
                    read_string = read_string.substr(0, possibly_trailing_whitespace_start);
                }
                possibly_trailing_whitespace_start = std::string::npos;
            } else if (current_char == '/') {
                //This may introduce a comment.
                read_current_char = false;
            } else if (last_chars[0] == '/') {
                if (current_char == '*') {
                    //The start of a comment.
                    //Push the selector state on the state stack.
                    state_stack.push(current_state);
                    current_state = StylesheetParser::State::COMMENT;
                    read_current_char = false;
                } else {
                    //Invalid character sequence.
                    throw MNE::ParsingException(
                        this->line,
                        this->column,
                        "CSSParser",
                        "Invalid character sequence!",
                        1
                        );
                }
            } else if (current_char == ' ') {
                if (possibly_trailing_whitespace_start == std::string::npos) {
                    possibly_trailing_whitespace_start = read_string.length();
                }
            } else {
                possibly_trailing_whitespace_start = std::string::npos;
            }
        } else if (current_state == StylesheetParser::State::RULECONTENT) {
            if ((current_char > 0x40 && current_char < 0x5b) ||
                (current_char > 0x60 && current_char < 0x7b) ||
                (current_char == '-')
                ) {
                //A-Z or a-z or -
                current_state = StylesheetParser::State::PROPERTY_NAME;
                read_current_char = true;
            } else if ((current_char > 0x2f && current_char < 0x3a)) {
                //0-9 directly after whitespace: invalid character sequence.
                throw MNE::ParsingException(
                    this->line,
                    this->column,
                    "CSSParser",
                    "Invalid character sequence!",
                    1
                    );
            } else if (current_char == ' ') {
                //Whitespace
                read_current_char = false;
            } else if (current_char == '}') {
                //End of rule
                read_current_char = false;
                current_state = StylesheetParser::State::NOSTYLE;
                new_element = StylesheetParser::NewElementType::CLOSINGRULE;
            }
        } else if (current_state == StylesheetParser::State::PROPERTY_NAME) {
            if (current_char == ' ') {
                //Whitespace after the property name.
                current_state = StylesheetParser::State::PROPERTY_NAME_WHITESPACE_AFTER;
                read_current_char = false;
                new_element = StylesheetParser::NewElementType::ATTRIBUTENAME;
            } else if (current_char == ':') {
                //Beginning of the value.
                current_state = StylesheetParser::State::PROPERTY_VALUE_WHITESPACE_BEFORE;
                read_current_char = false;
                new_element = StylesheetParser::NewElementType::ATTRIBUTENAME;
            }
        } else if (current_state == StylesheetParser::State::PROPERTY_NAME_WHITESPACE_AFTER) {
            read_current_char = false;
            if (current_char == ':') {
                //Beginning of the value.
                current_state = StylesheetParser::State::PROPERTY_VALUE_WHITESPACE_BEFORE;
            } else if (current_char != ' ') {
                //Invalid character sequence.
                throw MNE::ParsingException(
                    this->line,
                    this->column,
                    "CSSParser",
                    "Invalid character sequence!",
                    1
                    );
            }
        } else if (current_state == StylesheetParser::State::PROPERTY_VALUE_WHITESPACE_BEFORE) {
            if ((current_char > 0x40 && current_char < 0x5b) ||
                (current_char > 0x60 && current_char < 0x7b) ||
                (current_char > 0x2f && current_char < 0x3a) ||
                (current_char == '#') ||
                (current_char == '\"')
                ) {
                //The value starts.
                current_state = StylesheetParser::State::PROPERTY_VALUE;
                read_current_char = true;
            } else if (current_char == ';') {
                //End of property without a value. Invalid character sequence.
                throw MNE::ParsingException(
                    this->line,
                    this->column,
                    "CSSParser",
                    "Invalid character sequence!",
                    1
                    );
            } else {
                read_current_char = false;
            }
        } else if (current_state == StylesheetParser::State::PROPERTY_VALUE) {
            if (current_char == ' ') {
                //Whitespace after the end of the value.
                current_state = StylesheetParser::State::PROPERTY_VALUE_WHITESPACE_AFTER;
                read_current_char = false;
                new_element = StylesheetParser::NewElementType::ATTRIBUTEVALUE;
            } else if (current_char == ';') {
                //End of value and end of property.
                current_state = StylesheetParser::State::RULECONTENT;
                read_current_char = false;
                new_element = StylesheetParser::NewElementType::ATTRIBUTEVALUE;
            }
            //TODO: more complex property value handling, e.g.
            //- properties encapsulated in double quotes.
            //- property values containing whitespace (margin, padding, border).
        } else if (current_state == StylesheetParser::State::PROPERTY_VALUE_WHITESPACE_AFTER) {
            read_current_char = false;
            if (current_char == ';') {
                //End of property.
                current_state = StylesheetParser::State::RULECONTENT;
            } else if (current_char != ' ') {
                //Invalid character sequence (for now).
                throw MNE::ParsingException(
                    this->line,
                    this->column,
                    "CSSParser",
                    "Invalid character sequence!",
                    1
                    );
            }


        // Old states:


        } else if (current_state == StylesheetParser::State::ATBLOCKTYPE) {
            if ((current_char > 0x40 && current_char < 0x5b) ||
                (current_char > 0x60 && current_char < 0x7b)
                ) {
                //A-Z or a-z
                current_state = StylesheetParser::State::ATNAME;
                read_current_char = true;
            }
        } else if (current_state == StylesheetParser::State::ATNAME) {
            if (current_char == 0x20) {
                AtExprName = read_string;
                read_string = "";
                possibly_trailing_whitespace_start = std::string::npos;
                current_state = StylesheetParser::State::ATNAMEWHITE;
                read_current_char = false;
            }
        } else if (current_state == StylesheetParser::State::ATNAMEWHITE) {
            if ((current_char > 0x40 && current_char < 0x5b) ||
                (current_char > 0x60 && current_char < 0x7b) ||
                (current_char > 0x2f && current_char < 0x3a) ||
                (current_char == 0x23)
                ) {
                //A-Z or a-z or 0-9 or #
                current_state = StylesheetParser::State::ATPARAM;
                read_current_char = true;
            }
        } else if (current_state == StylesheetParser::State::ATPARAM) {
            if (current_char == 0x20) {
                AtExprParam = read_string;
                read_string = "";
                possibly_trailing_whitespace_start = std::string::npos;
                current_state = StylesheetParser::State::ATPARAMWHITE;
                read_current_char = false;
            }
            if (current_char == '{') {
                AtExprParam = read_string;
                read_string = "";
                possibly_trailing_whitespace_start = std::string::npos;
                new_element = StylesheetParser::NewElementType::MEDIAGROUP;
                current_state = StylesheetParser::State::GROUPSTYLES;
                read_current_char = false;
            }
        } else if (current_state == StylesheetParser::State::GROUPSTYLES) {
            //this behaves like StylesheetParser::State::NOSTYLE except that the state
            //has to be pushed to the state stack
            //if the state changes

            if (current_char == '*' && last_chars[0] == '/') {
                //read '/*'
                state_stack.push(current_state);
                current_state = StylesheetParser::State::COMMENT;
                read_current_char = false;
            }

            if ((current_char > 0x40 && current_char < 0x5b) ||
                (current_char > 0x60 && current_char < 0x7b)
                ) {
                //A-Z or a-z or 0-9
                state_stack.push(current_state);
                current_state = StylesheetParser::State::SELECTOR;
                read_current_char = true;
            } else if (current_char > 0x2f && current_char < 0x3a) {
                //Number after whitespace: Invalid character sequence.
                throw MNE::ParsingException(
                    this->line,
                    this->column,
                    "CSSParser",
                    "Invalid character sequence!",
                    1
                    );
            }
            //are @-Statements allowed in @media blocks?
        } else if (current_state == StylesheetParser::State::ATCLOSING) {
            read_next_char = false;
            current_state = StylesheetParser::State::NOSTYLE;
            read_current_char = false;
        }

        //New element type handling:

        if (new_element == StylesheetParser::NewElementType::COMMENT) {
            //Nothing for now.
        } else if (new_element == StylesheetParser::NewElementType::SELECTOR) {
            if (this->current_rule == nullptr) {
                this->current_rule = std::make_shared<StylesheetRule>();
            }
            this->current_rule->addSelector(read_string);
        } else if (new_element == StylesheetParser::NewElementType::ATTRIBUTENAME) {
            if (this->current_rule == nullptr) {
                this->current_rule = std::make_shared<StylesheetRule>();
            }
            if(this->current_attribute == nullptr) {
                this->current_attribute = std::make_shared<StylesheetAttribute>(read_string);
            } else {
                this->current_attribute->name = read_string;
            }
        } else if (new_element == StylesheetParser::NewElementType::ATTRIBUTEVALUE) {
            if (this->current_rule != nullptr) {
                if(this->current_attribute != nullptr) {
                    this->current_attribute->value = read_string;
                    this->convertAttribute(this->current_attribute, this->current_rule);
                }
                this->current_attribute.reset();
            }
        } else if (new_element == StylesheetParser::NewElementType::RULE) {
            this->current_rule = std::make_shared<StylesheetRule>();
        } else if (new_element == StylesheetParser::NewElementType::CLOSINGRULE) {
            //WARNING: It is assumed that the rule that is being created
            //belongs to the last created group!
            //This has to be corrected in the future!

            if (this->parsed_rules == nullptr) {
                //if the stylesheet collection wasn't yet created,
                //it now has to be created!
                this->parsed_rules = std::make_shared<StylesheetCollection>();
                //TODO: check if creation was successful
            }

            if (this->parsed_rules->groups.size() == 0) {
                //If no groups are present then a group has to be
                //created with the default media type.

                this->current_group = std::make_shared<StylesheetGroup>(
                    STYLESHEETGROUPTYPE_MEDIA,
                    "screen"
                    );

                this->parsed_rules->groups.push_back(this->current_group);
                //TODO: check if creation was successful
            }

            if (this->current_group != nullptr) {
                this->current_group->rules.push_back(this->current_rule);
            }

            //since the address on which $this->current_rule points contains valid content
            //delete mustn't be called here. Instead only $this->current_rule has to be set to nullptr.
            this->current_rule = nullptr;

        } else if (new_element == StylesheetParser::NewElementType::MEDIAGROUP) {
            if (this->parsed_rules == nullptr) {
                //If the stylesheet collection wasn't yet created,
                //it now has to be created!
                this->parsed_rules = std::make_shared<StylesheetCollection>();
                //TODO: check if creation was successful
            }

            this->parsed_rules->groups.push_back(
                std::make_shared<StylesheetGroup>(
                    STYLESHEETGROUPTYPE_MEDIA,
                    AtExprParam
                    )
                );
            AtExprParam = "";
            AtExprName = "";
        }

        if (new_element != StylesheetParser::NewElementType::NONE) {
            //Reset the new element type, the read string and the
            //index position for possibly trailing whitespace:
            new_element = StylesheetParser::NewElementType::NONE;
            read_string = "";
            possibly_trailing_whitespace_start = std::string::npos;
        }

        if ((read_current_char == true) && (read_next_char == true)) {
            //damit nur dann ein Zeichen in read_string eingelesen wird
            //wenn $read_current_char und $read_next_char auf TRUE gesetzt sind.
            //Wenn read_next_char auf FALSE gesetzt ist und trotzdem ein neues Zeichen
            //an $read_string eingelesen würde, so würde dieses Zeichen zweimal in $read_string auftauchen
            read_string += current_char;
        }

        last_chars[1] = last_chars[0];
        last_chars[0] = current_char;
        last_state = current_state;
    }
}


Colour CSSParser::decodeColourValues(const std::string colour)
{
    //Determine the length of the string:
    int length = colour.length();
    char *r = new char[3];
    char *g = new char[3];
    char *b = new char[3];
    if(length == 7) //Form: #000000
    {
        r[0] = colour[1];
        r[1] = colour[2];
        r[2] = '\0';
        g[0] = colour[3];
        g[1] = colour[4];
        g[2] = '\0';
        b[0] = colour[5];
        b[1] = colour[6];
        b[2] = '\0';
    }
    if(length == 4) //Form #000
    {
        r[0] = colour[1];
        r[1] = colour[1];
        r[2] = '\0';
        g[0] = colour[2];
        g[1] = colour[2];
        g[2] = '\0';
        b[0] = colour[3];
        b[1] = colour[3];
        b[2] = '\0';
    }

    Colour result;
    unsigned int ir = 0;
    unsigned int ig = 0;
    unsigned int ib = 0;
    sscanf(r,"%x",&ir);
    sscanf(g,"%x",&ig);
    sscanf(b,"%x",&ib);

    //We're only interested in the lowest 8 bits, unless
    //we add support for 64-bit colours ;-)
    result.r = (unsigned char) (ir & 0x000000FF);
    result.g = (unsigned char) (ig & 0x000000FF);
    result.b = (unsigned char) (ib & 0x000000FF);
    result.alpha = 255;

    delete[] r;
    delete[] g;
    delete[] b;

    return result;
}


void CSSParser::convertAttribute(
    std::shared_ptr<StylesheetAttribute> source,
    std::shared_ptr<StylesheetRule> rule
    )
{
    if ((source == nullptr) || (rule == nullptr)) {
        return;
    }
    if (source->name.substr(0, 7) == "padding") {
        uint32_t padding = 0;
        try {
            padding = strtoul(source->value.c_str(), nullptr, 10);
        } catch (std::out_of_range) {
            //Do nothing
            return;
        } catch (std::invalid_argument) {
            //Do nothing
            return;
        }
        if (padding > USHRT_MAX) {
            //We cannot set such a value.
            return;
        }
        if (source->name == "padding") {
            rule->attributes.padding_left = padding;
            rule->attributes.padding_top = padding;
            rule->attributes.padding_right = padding;
            rule->attributes.padding_bottom = padding;
        } else if (source->name == "padding-left") {
            rule->attributes.padding_left = padding;
        } else if (source->name == "padding-right") {
            rule->attributes.padding_right = padding;
        } else if (source->name == "padding-top") {
            rule->attributes.padding_top = padding;
        } else if (source->name == "padding-bottom") {
            rule->attributes.padding_bottom = padding;
        }
    } else if (source->name.substr(0, 6) == "margin") {
        uint32_t margin = 0;
        try {
            margin = strtoul(source->value.c_str(), nullptr, 10);
        } catch (std::out_of_range) {
            //Do nothing
            return;
        } catch (std::invalid_argument) {
            //Do nothing
            return;
        }
        if (margin > USHRT_MAX) {
            //We cannot set such a value.
            return;
        }
        if (source->name == "margin") {
            rule->attributes.margin_left = margin;
            rule->attributes.margin_top = margin;
            rule->attributes.margin_right = margin;
            rule->attributes.margin_bottom = margin;
        } else if (source->name == "margin-left") {
            rule->attributes.margin_left = margin;
        } else if (source->name == "margin-right") {
            rule->attributes.margin_right = margin;
        } else if (source->name == "margin-top") {
            rule->attributes.margin_top = margin;
        } else if (source->name == "margin-bottom") {
            rule->attributes.margin_bottom = margin;
        }
    } else if (source->name == "background-color") {
        rule->attributes.background_color = this->decodeColourValues(source->value);
    } else if (source->name == "color") {
        rule->attributes.foreground_color = this->decodeColourValues(source->value);
    } else if (source->name == "z-index") {
        rule->attributes.z_index = atoi(source->value.c_str());
    } else if (source->name == "display") {
        if (source->value == "block") {
            rule->attributes.display_method = VisualAttributes::Display::BLOCK;
        } else if (source->value == "inline") {
            rule->attributes.display_method = VisualAttributes::Display::INLINE;
        } else if (source->value == "none") {
            rule->attributes.display_method = VisualAttributes::Display::NONE;
        }
        //more to be added
    } else if (source->name == "border-width") {
        uint32_t border_width = 0;
        try {
            uint32_t border_width = strtoul(source->value.c_str(), nullptr, 10);
        } catch (std::out_of_range) {
            //Do nothing
            return;
        } catch (std::invalid_argument) {
            //Do nothing
            return;
        }
        if (border_width > USHRT_MAX) {
            //We cannot set such a value for the border.
            return;
        }
        rule->attributes.border_left_width = border_width;
        rule->attributes.border_top_width = border_width;
        rule->attributes.border_right_width = border_width;
        rule->attributes.border_bottom_width = border_width;
        if (rule->attributes.border_style == VisualAttributes::BorderStyle::NONE) {
            rule->attributes.border_style = VisualAttributes::BorderStyle::SOLID;
        }
    } else if ((source->name == "font-size") || (source->name == "line-height")) {
        try {
            uint32_t parsed_value = std::stoul(source->value.c_str(), nullptr);
            if (parsed_value <= USHRT_MAX) {
                if (source->name == "font-size") {
                    rule->attributes.font_size = (uint16_t) parsed_value;
                } else if (source->name == "line-height") {
                    rule->attributes.line_height = (uint16_t) parsed_value;
                }
            }
        } catch (std::invalid_argument) {
            //Do nothing.
        } catch (std::out_of_range) {
            //Do nothing.
        }
    }
}


void CSSParser::convertAttributes(
    std::vector<std::shared_ptr<StylesheetAttribute>> source,
    std::shared_ptr<StylesheetRule> rule
    )
{
    for (std::shared_ptr<StylesheetAttribute> attr: source) {
        this->convertAttribute(attr, rule);
    }
}


void CSSParser::flushStylesheetRules()
{
    this->parsed_rules = nullptr;
    this->current_group = nullptr;
    this->current_rule = nullptr;
    this->current_attribute = nullptr;
    this->line = 0;
    this->column = 0;
}


std::shared_ptr<StylesheetCollection> CSSParser::returnStylesheetRules()
{
    if (this->parsed_rules != nullptr)
    {
        //If a collection of parsed rules and groups is present
        //a copy of that collection is returned.
        return this->parsed_rules->copy();
    } else if(this->current_rule != nullptr) {
        //If the parser was reading a CSS rule that hasn't ended yet in the CSS code
        //this rule isn't part of $this->parsed_rules yet.
        //However if you parse a style attribute in HTML you will in most cases have rules that have no end (no closing curly braces).
        //So if someone has parsed CSS attributes from a style attribute in HTML
        //these attributes are stored in a rule.
        //At this point the rule has to be connected to a stylesheet group
        //and that group has to be connected to the stylesheet collection with all parsed rules ($this->parsed_rules)

        if (this->current_group == nullptr) {
            this->current_group = std::make_shared<StylesheetGroup>(
                STYLESHEETGROUPTYPE_MEDIA,
                "screen"
                );
            if (this->current_group == nullptr) {
                throw (char) -1; //throw errorcode -1: StylesheetParser: object construction failed
            }
        }
        this->current_group->rules.push_back(this->current_rule); //append $this->current_rule to the current parsed group

        if (this->parsed_rules == nullptr) {
            //It might be that there is no stylesheet collection yet
            //so it has to be created now.
            this->parsed_rules = std::make_shared<StylesheetCollection>();
            if (this->parsed_rules == nullptr) {
                throw (char) -1; //throw errorcode -1: StylesheetParser: object construction failed
            }
            this->parsed_rules->groups.push_back(this->current_group);
        }

        return this->parsed_rules->copy();
    }

    return nullptr; //shouldn't happen
}
