/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2019  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DEFAULTSTYLE_H
#define DEFAULTSTYLE_H


const std::string DEFAULT_STYLE =
    //"@media screen {"
    "* { display: inline; font-size: 12; line-height: 16; } "
    "head { display: none; } "
    "script { display: none; } "
    "style { display: none; } "
    "link { display: none; } "
    "body { margin: 8; } /* to be changed */ "
    "a { color: #0000FF; text-decoration: underline; font-size: 12;"
    " line-height: 16; } "
    "div { display: block; font-size: 12; line-height: 16; } "
    "img { display: block; font-size: 12; line-height: 16; } "
    "h1 { margin: 5; font-size: 20; line-height: 24; } "
    "p { padding: 15; font-size: 12; line-height: 16; } "
    //"}"
    ;


#endif
