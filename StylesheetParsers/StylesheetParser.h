/*
    This file is part of MoeNavigatorEngine
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef STYLESHEETPARSER_H
#define STYLESHEETPARSER_H


#include <iostream>
#include "../CommonClasses/StylesheetRule.h"
#include "../CommonClasses/StylesheetGroup.h"
#include "../CommonClasses/StylesheetCollection.h"




enum
{
    STYLESHEET_ELEMENT_NONE,
    STYLESHEET_ELEMENT_MEDIAGROUP, //inside a @media group
    STYLESHEET_ELEMENT_SELECTOR,
    STYLESHEET_ELEMENT_RULE,
    STYLESHEET_ELEMENT_CLOSINGRULE,
    STYLESHEET_ELEMENT_ATTRIBUTENAME,
    STYLESHEET_ELEMENT_ATTRIBUTEVALUE,
    STYLESHEET_ELEMENT_COMMENT
};



class StylesheetParser
{
    public:


    enum class State
    {
        NOSTYLE, ///parser is inside irrelevant code sequences
        COMMENT, ///parser is inside a comment

        CONTROL,
        CONTROLNAME,
        CONTROLNAMEWHITE,
        CONTROLPARAM,
        CONTROLPARAMWHITE,
        RULEGROUP,
        CONTROLCLOSING,

        SELECTOR, ///The parser is reading a selector.

        RULECONTENT, ///The parser is reading the content of a rule.

        PROPERTY_NAME, ///The parser is reading the name of a property.

        PROPERTY_NAME_WHITESPACE_AFTER,

        PROPERTY_VALUE_WHITESPACE_BEFORE,

        PROPERTY_VALUE, ///The parser is reading the value of a property.

        PROPERTY_VALUE_WHITESPACE_AFTER,

        TAGATTRIBUTE,
        TAGATTRIBUTE_NAME,
        TAGATTRIBUTE_VALUE,
        CLASSSELECTOR,
        IDSELECTOR,
        NEXTSELECTOR,
        SELECTORWHITE,

        RULEWHITE, ///parser is inside a rule (wrapped in curly braces in CSS)
        ATTRIBUTENAME, ///parser is inside an attribute name
        ATTRIBUTENAMEWHITE, ///parser is in whitespace after an attribute name and before an attribute value
        ATTRIBUTEVALUEWHITEBEFORE,
        ATTRIBUTEVALUE, ///parser is inside an attribute value
        ATTRIBUTEVALUEWHITEAFTER,
        RULECLOSING,

        ATBLOCKTYPE,
        ATNAME,
        ATNAMEWHITE,
        ATPARAM,
        ATPARAMWHITE,
        ATCLOSING,
        STYLEWHITE,
        STYLECLOSING,
        GROUPSTYLES
    };


    enum class NewElementType
    {
        NONE,
        MEDIAGROUP, //inside a @media group
        SELECTOR,
        RULE,
        CLOSINGRULE,
        ATTRIBUTENAME,
        ATTRIBUTEVALUE,
        COMMENT
    };


    /**
     * This variant of the parseData method resets the parsing state
     * before parsing the data block. It is meant for reading a
     * whole set of CSS code (e.g. a CSS file) at once.
     *
     * @param std::vector<uint8_t> data The CSS code to be read.
     */
    virtual void parseData(
        std::vector<uint8_t> data
        ) = 0;


    /**
     * This variant of the parseData method resets the parsing state to
     * a specified state. It is useful when the content of a style attribute
     * of a HTML tag shall be read.
     *
     * @param std::vector<uint8_t> data The CSS code to be read.
     *
     * @param StylesheetParser::State state The parser state to set before
     *     parsing the data.
     */
    virtual void parseData(
        std::vector<uint8_t> data,
        State state
        ) = 0;


    virtual std::shared_ptr<StylesheetCollection> returnStylesheetRules() = 0;


    virtual void flushStylesheetRules() = 0;


    /**
     * The default virtual destructor.
     */
    virtual ~StylesheetParser() = default;

};


#endif
